\documentclass[12pt]{article}
\usepackage{fouriernc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[bookmarks=true,colorlinks=true,linkcolor=blue]{hyperref}
\usepackage{listings}
\usepackage{mdframed}
\addtolength{\hoffset}{-1cm}
\addtolength{\textwidth}{2cm}
\addtolength{\voffset}{-1cm}
\addtolength{\textheight}{2cm}
\definecolor{lbcolor}{rgb}{0.92,0.92,0.92}
\begin{document}
\lstset{language=C}
\vspace{-1ex}
\noindent{}The \textsc{Acrobatic} Sampler Collection\\
\noindent{}Most code: Copyright \textcopyright{}2011-2019 Peter Behroozi\\
\noindent{}License: MIT/Expat\\
\noindent{}Mersenne Twister Code: Copyright \textcopyright{}2004 Makoto Matsumoto and Takuji Nishimura\\
\noindent{}Science/Documentation Papers: TBD\\

\tableofcontents
\newcommand{\ttt}[1]{\texttt{#1}}
\newcommand{\codestyle}{\small}
\definecolor{varcolor}{rgb}{0.127,0.526,0.541}

\lstset{
	tabsize=4,
	language=c,
        basicstyle=\codestyle,
        upquote=true,
        columns=fixed,
        extendedchars=true,
%        breaklines=true,
%        prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
        numberstyle=\ttfamily,
        identifierstyle=\ttfamily,
        keywordstyle={\ttfamily\color[rgb]{0,0,1}},
        commentstyle={\ttfamily\color[rgb]{0.133,0.545,0.133}},
        stringstyle={\ttfamily\color[rgb]{0.627,0.126,0.941}},
}
\lstset{emph={Sampler,QueueSample},emphstyle={\ttfamily\color[rgb]{0,0.7,0.233}}}
\lstset{emph={[2]NULL,ANGULAR_METROPOLIS_SAMPLER,STANDARD_METROPOLIS_SAMPLER,ALEXANDER_SAMPLER,SEARCHLIGHT_SAMPLER,SAMPLER_INVALID_LN_PROB,SAMPLER_INVALID_CHI2},emphstyle={[2]\ttfamily\color[rgb]{0.627,0.126,0.941}}}
\lstset{emph={[3]e,walkers,params,mode,q,dims,max_steps,stdout,dim,stderr,chi2,T,step_scale,success_ratio,radial_scale,theta_scale,N,dt,theta_rotate,delta_prob,tmax_scale,tmax_scatter,vel_scale,tmax_scaling,dt,walker,step_completed,ln_prob,stddevs},emphstyle={[3]\ttfamily\color{varcolor}}}
\lstset{emph={[4]FILE,int64_t},emphstyle={[4]\ttfamily\color[rgb]{0,0,1}}}

\surroundwithmdframed[linewidth=0pt,backgroundcolor=lbcolor,innertopmargin=0pt,innerbottommargin=0pt,innerleftmargin=4pt,innerrightmargin=4pt]{lstlisting}

\section{Overview}
\label{overview}

The \textsc{Acrobatic} Sampler Collection includes four parallel samplers:
\begin{itemize}
\item Standard Metropolis-Hastings, with adaptive step sizes; for uniform distributions or where other samplers fail (e.g., non-differentiable likelihood functions).\\[-4ex]
\item \textsc{Angular Metropolis}, with adaptive step sizes; for Gaussian-like distributions.\\[-4ex]
\item \textsc{Alexander} Hamiltonian; for continuously differentiable likelihood functions.\\[-4ex]
\item \textsc{Adaptive Searchlight}; for continuously twice differentiable likelihood functions.\\[-4ex]
\end{itemize}

\section{Getting Started with the C Language Version}

After cloning the source code (\url{https://bitbucket.org/pbehroozi/acrobatic/}), move the \texttt{src\_c} directory into your project.  Optionally, you can rename this directory into something more informative, like \texttt{acrobatic}.  Test that everything works by running \texttt{make} in the \textsc{Acrobatic} directory.  If this works, then you can add the C files in the \textsc{Acrobatic} directory (\texttt{sampler.c}, \texttt{mt\_rand.c}, \texttt{jacobi\_dim.c}, and \texttt{check\_syscalls.c}) into your project or Makefile so that they are compiled together.

The \texttt{example.c} source provides a full example of how to use the sampler.  The main parts are paraphrased here:
\begin{lstlisting}
#include "/path/to/sampler.h"

int main(void) {
  struct Sampler *e = new_sampler(walkers, dims, max_steps);
  sampler_set_type(ANGULAR_METROPOLIS_SAMPLER);
  sampler_estimate_covariance_matrix(e, params, NULL);  
  sampler_set_output_file(e, stdout);

  while (sampler_total_steps(e) < max_steps) {
    struct QueueSample *q = sampler_get_sample(e);
    sampler_assign_chi2(e,q,my_chi2(q->params));
  }
  return 0;
}
\end{lstlisting}
We'll go over each line in detail below.

\subsection{Sampler Initialization}
\label{c:init}

\begin{lstlisting}
struct Sampler *e = new_sampler(walkers, dims, max_steps);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Here, \lstinline{walkers} is the number of parallel samplers to initialize, \lstinline{dims} is the number of dimensions for your model parameter space, and \lstinline{max_steps} is the number of samples you're planning to generate.  For all samplers, the first 30\% of the steps will be adaptive and therefore non-reversible; these steps should be \textbf{discarded} from the output chains.  If you don't want any adaptation, set \lstinline{max_steps} to 0; there is no rule against generating more samples than you initially planned!

\subsection{Sampler Type}
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_type(e, ANGULAR_METROPOLIS_SAMPLER);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Options for the type include:
\begin{itemize}
\item \lstinline{STANDARD_METROPOLIS_SAMPLER}\\[-5ex]
\item \lstinline{ANGULAR_METROPOLIS_SAMPLER}\\[-5ex]
\item \lstinline{ALEXANDER_SAMPLER}\\[-5ex]
\item \lstinline{SEARCHLIGHT_SAMPLER}
\end{itemize}
All of the above are described in Section \ref{overview}.

\subsection{Parameter Space Validity}
\label{c:validity}

If your parameter space has boundaries, many of the available samplers can use this information to make more intelligent steps.  Hence, if you have a function that can check if a point in parameter space is valid (without computing the full likelihood), then you can pass that to the sampler as follows:

\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_valid_function(e, &valid);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
where \lstinline{valid} is a function with prototype \lstinline{int valid(const double *params)}.  This function should return \lstinline{1} if \lstinline{params} represents a valid position and \lstinline{0} if it does not.

If your parameter space has \textit{static} dimensions, i.e., dimensions that you do not want to be varied, then you need call the following function once for each static dimension:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_static_dim(e, dim);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Here, \lstinline{dim} is zero-indexed, so \lstinline{sampler_set_static_dim(e, 0);} would make the first dimension static.

\subsection{Initializing Walker Positions}
All sampling algorithms except for Standard Metropolis \textbf{benefit greatly} when you have already found the maximum likelihood position.  If you haven't already found this, there are many existing packages that can help, including \href{https://docs.scipy.org/doc/scipy/reference/optimize.html}{\texttt{scipy.optimize}} and \href{https://www.gnu.org/software/gsl/doc/html/multimin.html}{\texttt{GSL}}.

\textbf{You must use exactly one of the following options} for initializing walker positions, or your program will fail.  In addition, the sampler must be informed about \textbf{parameter space validity} (Section \ref{c:validity}) before you use any of the automatic walker setup options.

If your distribution is Gaussian-like, it can be very effective to estimate the covariance matrix from the \href{https://en.wikipedia.org/wiki/Hessian_matrix}{Hessian matrix} (i.e., the matrix of all second derivatives) at the maximum likelihood position:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_estimate_covariance_matrix(e, params, stddevs);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Here, \lstinline{params} is a 1-D array of type \lstinline{double} and length \lstinline{dims}; it should contain the maximum likelihood position.  The parameter \lstinline{stddevs} should be your estimate/guess/prior for the standard deviation of the parameter space along each dimension (again as a 1-D array of \lstinline{double}s with length \lstinline{dims}).  This helps to set a reasonable size for the numerical derivative steps in each dimension.  If you have no way of guessing, then you can pass \lstinline{NULL} for the third argument, and step sizes will be of order unity in each dimension.  

If your distribution is not Gaussian, it can still be effective to tell the walkers to search for the typical set (i.e., region where most probability is concentrated).  This can be done by:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_initial_exploration(e, params, stddevs);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
As above, \lstinline{params} should contain the maximum likelihood position, and the third argument should contain an estimate of the standard deviation of the parameter space along each dimension (or \lstinline{NULL} if you have absolutely no priors).  The search algorithm uses the second derivative of your likelihood function to inform its steps.

Finally, you could supply your own initial positions (e.g., drawing randomly from the prior distribution) by making repeated calls to the following function at least once for each of your \lstinline{walkers}:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_init_position(e, params);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
where \lstinline{params} is again a 1-D array of type \lstinline{double} and length \lstinline{dims}.  If you call this function more times than the number of \lstinline{walkers}, it will use this information to update the sample covariance matrix; this can be helpful when resuming a previous analysis.

In case you missed it above: \textbf{use only one of these methods to initialize your walkers}!

\subsection{Saving Accepted Samples}

Accepted samples are printed to standard output (\lstinline{stdout}) by default.  Each output line contains a single sample, with space-separated columns for each dimension.  The final two columns contain the natural log of the likelihood function ($\ln(\mathcal{L})$) and the equivalent $\chi^2$ ($\equiv  -2 \ln(\mathcal{L})$):
\begin{lstlisting}
dim0 dim1 dim2 dim3 ... dimN ln(L) chi^2 //Sample 1
dim0 dim1 dim2 dim3 ... dimN ln(L) chi^2 //Sample 2
dim0 dim1 dim2 dim3 ... dimN ln(L) chi^2 //etc.
\end{lstlisting}

You can change this to any \lstinline{FILE *} handle desired, including \lstinline{NULL} to disable output:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_output_file(e, stdout);
\end{lstlisting}
\lstset{basicstyle=\normalsize}

If you want information about step size adaptations, you can also ask for that to be saved, again to any  \lstinline{FILE *} handle:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_log_file(e, stderr);
\end{lstlisting}
\lstset{basicstyle=\normalsize}

If you would prefer for your own code to handle output, you can supply a callback function:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_step_completed_function(e, &step_completed);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Here, \lstinline{step_completed} is a function with the following prototype:
\lstset{basicstyle=\footnotesize}
\begin{lstlisting}
void step_completed(int64_t walker, const double *params, double ln_prob);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
This \textbf{automatically turns off writing samples to a file}, but you can re-enable this behavior (without disabling your callback) by calling \lstinline{sampler_set_output_file()} again after setting the step completion callback.  The function \lstinline{step_completed()} will be called \lstinline{walkers} times when all walkers have updated their positions.  Here, \lstinline{walker} is the index of the walker being returned (from \lstinline{0} to \lstinline{walkers-1}), \lstinline{params} is the walker position (length \lstinline{dims}) which \textbf{cannot be modified}, and \lstinline{ln_prob} is the natural logarithm of the likelihood for the accepted point.

\subsection{Evaluating Likelihoods}

All \textsc{Acrobatic} samplers generate a queue of likelihoods to evaluate.  You can pull a sample off this queue by calling:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
struct QueueSample *q = sampler_get_sample(e);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
The position to evaluate is \lstinline{q->params}, a \lstinline{double *} pointer to a \lstinline{dims}-length array.  There is no limit to how many times you do this; \lstinline{max_steps} is only used as a guide to plan how many steps should be adaptive.  You can calculate the likelihood however you wish and return the result as, e.g., a $\chi^2$ value:

\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_assign_chi2(e,q,my_chi2(q->params));
\end{lstlisting}
\lstset{basicstyle=\normalsize}
For invalid positions in parameter space, you should assign \lstinline{SAMPLER_INVALID_CHI2}.  

If you prefer (natural) log-likelihoods instead of $\chi^2$, you can instead use:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_assign_ln_prob(e,q,my_ln_like(q->params));
\end{lstlisting}
\lstset{basicstyle=\normalsize}
The associated invalid log-likelihood is \lstinline{SAMPLER_INVALID_LN_PROB}.

You can change the \textit{temperature} \lstinline{T} of the exploration (e.g., during burn-in) at any time:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_temp(e,T);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
This is equivalent to exploring parameter space with the modified likelihood function $(\mathcal{L})^{1/\texttt{\footnotesize{}\color{varcolor}T}}$; higher temperatures will explore broader regions of parameter space.

\subsection{Parallelism}

Each \textsc{Acrobatic} walker will add positions to the queue.  The number of positions per walker will depend on the sampler type:
\begin{itemize}
\item Standard Metropolis-Hastings: 1; sometimes 0 if not all parameter space is valid.\\[-4.5ex]
\item \textsc{Angular Metropolis}: 1; sometimes 0 if not all parameter space is valid.\\[-4.5ex]
\item \textsc{Alexander} Hamiltonian: 1+\lstinline{N}, where \lstinline{N} is the subspace dimensionality.\\[-4.5ex]
\item \textsc{Searchlight}: 3.\\[-4ex]
\end{itemize}
You can pull multiple positions off the queue and return them in any order, e.g.:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
struct QueueSample *q;
double chi2;
while (q=sampler_get_sample(e))
  my_start_evaluation_thread(q);  //Start evaluating all positions
while (q=my_evaluation_ready(&chi2))
  sampler_assign_chi2(e,q,chi2);  //Return positions in any order
\end{lstlisting}
\lstset{basicstyle=\normalsize}
where \lstinline{my_start_evaluation_thread} and \lstinline{my_evaluation_ready} are calls to your code.  When no more positions are available on the queue, \lstinline{sampler_get_sample(e)} will return \lstinline{NULL}.  Once all positions on the queue have been assigned likelihoods, then you are guaranteed to get a non-\lstinline{NULL} sample from your next call to \lstinline{sampler_get_sample(e)}.

The queue structure allows you maximum flexibility in how you choose to do parallel evaluation (e.g., via MPI calls to remote machines, or locally via OpenMP).  \textbf{Caution}: \lstinline{sampler_get_sample()}, \lstinline{sampler_assign_chi2()}, and \lstinline{sampler_assign_ln_prob()} \textbf{are not generally thread-safe}.  They are safe for threaded use with OpenMP (as they are wrapped with \lstinline{#pragma}\texttt{ omp critical}), but those using other thread libraries (e.g., \lstinline{boost::thread}) should make sure that these functions are executed by a single thread at a time.  As in the pseudocode above, you may wish to use a single thread to read queue elements, start worker threads, and then assign likelihoods as each worker thread finishes.

\subsection{Sampler Statistics}

You can query for the total number of steps taken by:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_total_steps(e)
\end{lstlisting}
\lstset{basicstyle=\normalsize}
This can be useful for deciding when to stop sampling, since  \lstinline{sampler_get_sample(e)} will otherwise continue to generate new samples.

The average acceptance fraction is:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_acceptance_fraction(e)
\end{lstlisting}
\lstset{basicstyle=\normalsize}

The total number of likelihoods evaluated is:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_total_likelihoods(e)
\end{lstlisting}
\lstset{basicstyle=\normalsize}

You can also estimate \textbf{integrated autocorrelation times} from the saved samples file.  If you ran \texttt{make} in the \textsc{Acrobatic} directory, it should have generated an executable called \texttt{acor\_estimate}.  You can run this in your terminal as follows:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
[shell]$ /path/to/acor_estimate samples_file dims walkers
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Where \lstinline{dims} and \lstinline{walkers} should match the values supplied to \lstinline{new_sampler()}.

\section{Sampler-Specific Options}

\subsection{Standard Metropolis-Hastings}

Standard Metropolis-Hastings step sizes start out as $2.38 / \sqrt{\texttt{\color{varcolor}dims}}$, after scaling out the sample covariance matrix.  You can scale this initial value by a factor \lstinline{step_scale} by calling:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_step_scaling(e,step_scale);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Note that this will be automatically adapted as the sampler evaluates new likelihoods unless you turned off adaptation entirely (Section \ref{c:init}).

The default success target ratio (i.e., fraction of accepted to total samples) is 0.234.  To change this, call:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_target_success_ratio(e,success_ratio);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
The code will try to adapt the step size to achieve the desired \lstinline{success_ratio} until it has sampled \lstinline{0.3*max_steps} steps.

\subsection{\textsc{Angular Metropolis}}

\textsc{Angular Metropolis} steps start out with $r_\mathrm{scale} = \theta_\mathrm{scale} = 1$.  To change these initial values, you can call:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_radial_scaling(e,radial_scale);
sampler_set_theta_scaling(e,theta_scale); //theta_scale in radians
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Note that these will be automatically adapted as the sampler evaluates new likelihoods unless you turned off adaptation entirely (Section \ref{c:init}).  The maximum value of \lstinline{theta_scale} is $\frac{\pi}{2}$.

The default success target ratio (i.e., fraction of accepted to total samples) is 0.234.  To change this, call:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_target_success_ratio(e,success_ratio);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
The code will try to adapt the step size to achieve the desired \lstinline{success_ratio} until it has sampled \lstinline{0.3*max_steps} steps.

\subsection{\textsc{Alexander} Hamiltonian}

To change the subspace dimensionality \lstinline{N} (i.e., the number of dimensions for the Hamiltonian transition), call:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_basis_dims(e,N);
\end{lstlisting}
\lstset{basicstyle=\normalsize}
By default, \lstinline{N=3}.  If you set \lstinline{N=dims}, you will get a full Hamiltonian transition.

You can also control many of the integration parameters:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_dt(e,dt);                      //timestep size
sampler_set_tmax_scaling(e,tmax_scale);    //tmax scaling
sampler_set_tmax_scatter(e,tmax_scatter);  //scatter (dex) in tmax
sampler_set_velocity_scaling(e,vel_scale); //velocity scaling
sampler_set_theta_rotate(e,theta_rotate);  //boundary deflection angle
\end{lstlisting}
\lstset{basicstyle=\normalsize}
Defaults are \lstinline{dt=1}, \lstinline{tmax_scale=1}, \lstinline{tmax_scatter=0.05}, \lstinline{vel_scale=1}, and \lstinline{theta_rotate=}$\frac{\pi}{6}$.

You can also set the threshold for summary rejection:
\lstset{basicstyle=\codestyle}
\begin{lstlisting}
sampler_set_ln_prob_reject_threshold(e,delta_prob); //units of ln(L)
\end{lstlisting}
\lstset{basicstyle=\normalsize}
If the (absolute) natural log probability difference between the initial and current position exceeds \lstinline{delta_prob}, then the integration path will be terminated.

\subsection{\textsc{Adaptive Searchlight}}
 TBC.

\end{document}
