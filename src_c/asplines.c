#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <assert.h>
#include "asplines.h"
#include "check_syscalls.h"

struct aspline *aspline_new(void) {
  struct aspline *a = NULL;
  check_calloc_s(a, sizeof(struct aspline), 1);
  a->uncertainty_scaling = ASPLINE_DEFAULT_UNCERTAINTY_SCALING;
  return a;
}

void aspline_reset(struct aspline *a) {
  if (a->v) check_free(a->v);
  memset(a, 0, sizeof(struct aspline));
  a->uncertainty_scaling = ASPLINE_DEFAULT_UNCERTAINTY_SCALING;
}

#define SWP(a,b) { ASPLINE_TYPE tmp = a; a=b; b=tmp; } 
//Solves ax^2 + bx + c = 0; stores solutions in x1 and x2, with *x1 <= *x2
int aspline_quadratic_solution(ASPLINE_TYPE a, ASPLINE_TYPE b, ASPLINE_TYPE c, ASPLINE_TYPE *x1, ASPLINE_TYPE *x2) {
  ASPLINE_TYPE b2 = b*b;
  ASPLINE_TYPE ac4 = 4.0l*a*c;
  if (a==0) { //Linear equation
    if (b==0) {
      if (c==0) {
	*x1 = *x2 = 0;
	return 1;
      }
      return 0; //No real roots
    }
    *x1 = *x2 = (-c/b);
    return 1;
  }
  if (b2 < ac4) return 0; //No real roots
  if (b==0 && c==0) {
    *x1 = *x2 = 0;
    return 2;
  }
  ASPLINE_TYPE sq_disc = sqrtl(b2 - ac4);
  ASPLINE_TYPE q = -(b + copysign(1l, b)*sq_disc)/2.0l;
  *x1 = q/a;
  *x2 = c/q;

  if (*x1 > *x2) SWP(*x1, *x2);
  return 2;
}


void aspline_improve_quad_roots(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots, int64_t nroots) {
  ASPLINE_TYPE new_roots[2];
  for (int64_t i=0; i<nroots; i++) {
    ASPLINE_TYPE nx = roots[i];
    ASPLINE_TYPE cv = d + nx*c + nx*nx*b + nx*nx*nx*a;
    for (int64_t j=0; j<10; j++) {
      ASPLINE_TYPE rd, rc, rb=0;
      rd = d + nx*c + nx*nx*b + nx*nx*nx*a;
      rc = c + 2.0*nx*b + 3.0*nx*nx*a;
      //rb = b + 3.0*nx*a;
      aspline_quadratic_solution(rb, rc, rd, new_roots, new_roots+1);
      if (fabsl(new_roots[1])<fabsl(new_roots[0])) new_roots[0] = new_roots[1];
      nx = nx+new_roots[0];
      ASPLINE_TYPE nv = d + nx*c + nx*nx*b + nx*nx*nx*a;
      if (fabsl(nv)<=fabsl(cv)) {
	roots[i]=nx;
	cv = nv;
      }
    }
  }
}

int _aspline_cubic_solution_blinn(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots) {
  roots[0] = roots[1] = roots[2] = 0;
  if (a==0) {
    return aspline_quadratic_solution(b, c, d, roots, roots+1);
  }
  ASPLINE_TYPE A=a, B=b/3.0l, C=c/3.0l, D=d;
  ASPLINE_TYPE delta_1 = (3.0*a*c-b*b);
  ASPLINE_TYPE delta_2 = (a*d*9.0l-b*c);
  ASPLINE_TYPE delta_3 = (b*d*3.0-c*c);
  ASPLINE_TYPE disc = (4.0*delta_1*delta_3 - delta_2*delta_2)/(81.0l);
  delta_1/=9.0l;
  delta_2/=9.0l;
  delta_3/=9.0l;
  
  if (disc <0) { //One real root
    ASPLINE_TYPE A_tilde = D, C_bar = delta_3, D_bar = -D*delta_2+2.0l*C*delta_3;
    if (B*B*B*D >= A*C*C*C) {
      A_tilde = A; C_bar = delta_1; D_bar = -2.0*B*delta_1+A*delta_2;
    }
    ASPLINE_TYPE T0 = -copysign(fabsl(A_tilde)* sqrtl(-disc), D_bar);
    ASPLINE_TYPE T1 = T0 - D_bar;
    ASPLINE_TYPE p = cbrtl(T1/2.0l);
    ASPLINE_TYPE q = -p;
    if (T1!=T0) q = -C_bar / p;
    ASPLINE_TYPE x1_tilde = p+q;
    if (C_bar>0) x1_tilde = -D_bar / (p*p + q*q + C_bar);
    if (B*B*B*D >=  A*C*C*C) //Optimized away
      roots[0] = (x1_tilde - B)/A;
    else
      roots[0] = -D / (x1_tilde + C);
    roots[1] = roots[2] = roots[0];
    return 1;
  }
  // disc >= 0
  ASPLINE_TYPE C_bar_A = delta_1, D_bar_A = -2.0*B*delta_1+A*delta_2;
  ASPLINE_TYPE C_bar_D = delta_3, D_bar_D = -D*delta_2+2.0l*C*delta_3;

  
  ASPLINE_TYPE theta_A = fabsl(atan2l(A*sqrtl(disc), -D_bar_A))/3.0l;
  ASPLINE_TYPE x_tilde_1A = 2.0l*sqrtl(-C_bar_A)*cosl(theta_A);
  ASPLINE_TYPE x_tilde_3A = 2.0l*sqrtl(-C_bar_A)*(-0.5l*cosl(theta_A)-sqrtl(3.0l/4.0l)*sinl(theta_A));
  ASPLINE_TYPE x_tilde_L = x_tilde_3A;
  if (x_tilde_1A +  x_tilde_3A > 2.0l*B)
    x_tilde_L = x_tilde_1A;
  ASPLINE_TYPE x_L = x_tilde_L-B;
  ASPLINE_TYPE w_L = A;
  
  ASPLINE_TYPE theta_D = fabsl(atan2l(D*sqrtl(disc), -D_bar_D))/3.0l;
  ASPLINE_TYPE x_tilde_1D = 2.0l*sqrtl(-C_bar_D)*cosl(theta_D);
  ASPLINE_TYPE x_tilde_3D = 2.0l*sqrtl(-C_bar_D)*(-0.5l*cosl(theta_D)-sqrtl(3.0l/4.0l)*sinl(theta_D));
  ASPLINE_TYPE x_tilde_S = x_tilde_3D;
  if (x_tilde_1D +  x_tilde_3D < 2.0l*C)
    x_tilde_S = x_tilde_1D;
  ASPLINE_TYPE x_S = -D;
  ASPLINE_TYPE w_S = x_tilde_S+C;

  ASPLINE_TYPE E = w_L*w_S;
  ASPLINE_TYPE F = -x_L*w_S-w_L*x_S;
  ASPLINE_TYPE G = x_L*x_S;
  if ((C*F-B*G)==0) roots[1] = 0;
  else roots[1] = (C*F-B*G)/(-B*F+C*E);
  roots[2] = x_L/w_L;
  if (D==0) roots[0] = 0;
  else roots[0] = x_S/w_S;
  
  ASPLINE_TYPE p = (3.0l*a*c - b*b)/(3.0l*a*a);
  if (p==0) { return 1; } //Triple root
  else if (disc==0) { //Double root
    if (roots[0]==roots[1]) SWP(roots[1], roots[2]);
    if (roots[0]>roots[1]) SWP(roots[0], roots[1]);
    return 2;
  }
  if (roots[0]>roots[2]) SWP(roots[0],roots[2]);
  if (roots[0]>roots[1]) SWP(roots[0],roots[1]);
  if (roots[1]>roots[2]) SWP(roots[1],roots[2]);
  return 3;
}

int aspline_cubic_solution_blinn(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots) {
  int nroots = _aspline_cubic_solution_blinn(d, c, b, a, roots);
  aspline_improve_quad_roots(d, c, b, a, roots, nroots);
  for (int i=0; i<nroots-1; i++) {
    if (roots[i]==roots[i+1]) {
      for (int j=i; j<nroots-1; j++) roots[j] = roots[j+1];
      nroots--;
    }
  }
  return nroots;
}

int aspline_find_maxmin(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots) {
  ASPLINE_TYPE dd = c, dc = (ASPLINE_TYPE)2.0*b, db = (ASPLINE_TYPE)3.0*a;
  return aspline_cubic_solution_blinn(dd, dc, db, 0, roots);
}


//See https://courses.cs.washington.edu/courses/cse590b/13au/lecture_notes/solvecubic_p5.pdf
//Or better: https://par.nsf.gov/servlets/purl/10382084
//Follows the wikipedia algorithm for ax^3 + bx^2 + cx + d = 0
int aspline_cubic_solution(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots) {
  roots[0] = roots[1] = roots[2] = 0;
  if (a==0) return aspline_quadratic_solution(b, c, d, roots, roots+1);
  //Change of variables to t = x + (b/(3a)), so ax^3 + bx^2 + cx + d = t^3 + pt + q
  //We'll solve for roots in terms of t, then transform as x = t - (b/3a)
  ASPLINE_TYPE offset = -b/(3.0l*a);
  ASPLINE_TYPE p = (3.0l*a*c - b*b)/(3.0l*a*a);
  ASPLINE_TYPE q = (2.0l*b*b*b - 9.0l*a*b*c + 27.0l*a*a*d)/(27.0l*a*a*a);
  ASPLINE_TYPE disc = -(4.0l*p*p*p + 27.0l*q*q);

  if (fabsl(p*p*p)+fabsl(q*q)>1e10*fabsl(disc)) { //Numerical issues...need to make a more robust long-term fix
    aspline_quadratic_solution(b, c, d, roots, roots+1);
    aspline_improve_quad_roots(d, c, b, a, roots, 2);
    if (roots[1]<roots[0]) SWP(roots[0], roots[1]);
    return 2;
  }
  
  if (disc < 0) { //One real root, use Cardano's formula
    ASPLINE_TYPE dsqrt = sqrtl(disc/(-27.0l*4.0l));
    ASPLINE_TYPE u1 = -q/2.0l + dsqrt;
    ASPLINE_TYPE u2 = -q/2.0l - dsqrt;
    roots[0] = cbrtl(u1)+cbrtl(u2) + offset;
    return 1;
  } else if (disc > 0) { //Three real roots, use Viete's formula to avoid complex number manipulation
    for (int64_t k=0; k<3; k++) {
      roots[k] = 2.0l*sqrtl(-p/3.0)*cosl((acosl(3.0l*q/(2.0l*p)*sqrtl(-3.0l/p))/3.0l)-2.0l*M_PIl*k/3.0l) + offset;
    }
  } else { //multiple roots
    if (p==0) {
      roots[0] = roots[1] = roots[2] = offset;
      return 1;
    }
    else {
      roots[0] = 3.0l*q/p + offset;
      roots[1] = roots[2] = -3.0l*q/2.0l*p + offset;
      if (roots[1]<roots[0]) {
	roots[2] = roots[0];
	roots[0] = roots[1];
	roots[1] = roots[2];
	roots[2] = roots[0];
      }
      return 2;
    }
  }

  //sort roots:
  if (roots[0]>roots[2]) SWP(roots[0],roots[2]);
  if (roots[0]>roots[1]) SWP(roots[0],roots[1]);
  if (roots[1]>roots[2]) SWP(roots[1],roots[2]);
#undef SWP
      return 3;
}

//Find quadratic curve that passes through (x0, y0), (x1, y1), (x2, y2)
void aspline_quad_solve(struct aspline_segment *v, struct aspline_segment *res) {
  res->d = 0;
  ASPLINE_TYPE A=0, B=0;
  //Solve for y = A(x-x0)^2 + B(x-x0) + y0
  ASPLINE_TYPE dxa = v[1].x-v[0].x;
  ASPLINE_TYPE dxb = v[2].x-v[0].x;
  ASPLINE_TYPE div = dxa*dxa - dxa*dxb;
  assert(div < 0);
  assert(dxa > 0);
  assert(dxb > 0);
  A = (v[1].y - v[0].y - dxa*(v[2].y-v[0].y)/dxb)/div;
  B = (v[1].y - v[0].y)/dxa - A*dxa;
  res->c = A;
  res->b = B;
  res->a = v[0].y;
  res->x = v[0].x;
  /*if (fabs(v[0].y+0.008537)<1e-5) {
    fprintf(stderr, "AS: %Lf:%Lf %Lf:%Lf %Lf:%Lf\n", v[0].x, v[0].y, v[1].x, v[1].y,  v[2].x, v[2].y);
    fprintf(stderr, "AS: %Lf %Lf %Lf\n", res->a, res->b, res->c);
    }*/
}

//shifts a+bx+cx^2+dx^3 to a+b(x-dx)+c(x-dx)^2+d(x-dx)^3
void aspline_shift_coefficients(struct aspline_segment *v, ASPLINE_TYPE dx) {
  v->a += v->b*dx + v->c*dx*dx + v->d*dx*dx*dx;
  v->b += 2.0*v->c*dx + 3.0*v->d*dx*dx;
  v->c += 3.0*v->d*dx;
}

//shifts a+bx+cx^2+dx^3 to a+b(x-dx)+c(x-dx)^2+d(x-dx)^3
void aspline_parabolic_termination(struct aspline_segment *fit, struct aspline_segment *v) {
  ASPLINE_TYPE dx = v->x - fit->x;
  //v->a += v->b*dx + v->c*dx*dx + v->d*dx*dx*dx; //Leave unchanged
  v->b = fit->b + 2.0*fit->c*dx + 3.0*fit->d*dx*dx;
  v->c = fit->c + 3.0*fit->d*dx;
  v->d = fit->d;
}

void _aspline_psolve(struct aspline *a) {
  struct aspline_segment *v = a->v;
  v->a = v->b = v->c = v->d = 0;
  if (a->n == 1) {
    v->a=v->y;
  }
  else if (a->n == 2) {
    v->a = v->y;
    v->b = (v[1].y - v[0].y)/(v[1].x - v[0].x);
  }
  else if (a->n == 3) {
    aspline_quad_solve(v, v);
  }
  else {
    exit(EXIT_FAILURE);
  }

  for (int64_t i=1; i<a->n; i++) {
    v[i].a = v->a;
    v[i].b = v->b;
    v[i].c = v->c;
    v[i].d = v->d;
    aspline_shift_coefficients(v+i, v[i].x-v[0].x); //Check direction
  }
  a->extrapolate[0] = v[0];
  a->extrapolate[1] = v[0];
}


void parabolic_aspline_refactor(struct aspline *a) {
  struct aspline_segment *v = a->v;
  if (a->n < 4) exit(EXIT_FAILURE);
  aspline_quad_solve(v, a->extrapolate); //left extrapolation
  aspline_quad_solve(v+a->n-3, a->extrapolate+1); //right extrapolation

  for (int64_t i=0; i<a->n-1; i++)
    v[i].m = (v[i+1].y-v[i].y)/(v[i+1].x-v[i].x);
  
  v[0].s = _aspline_eval_derivative(a->extrapolate, v[0].x);
  v[1].s = _aspline_eval_derivative(a->extrapolate, v[1].x);
  v[a->n-2].s = _aspline_eval_derivative(a->extrapolate+1, v[a->n-2].x);
  v[a->n-1].s = _aspline_eval_derivative(a->extrapolate+1, v[a->n-1].x);
  
  for (int64_t i=2; i<a->n-2; i++) {
    ASPLINE_TYPE d1 = fabsl(v[i+1].m - v[i].m);
    ASPLINE_TYPE d2 = fabsl(v[i-1].m - v[i-2].m);
    if (d1+d2>0) {
      v[i].s = (d1*v[i-1].m + d2*v[i].m)/(d1+d2);
    } else {
      v[i].s = 0.5*(v[i-1].m+v[i].m);
    }
  }
  for (int64_t i=0; i<a->n-1; i++) {
    v[i].a = v[i].y;
    v[i].b = v[i].s;
    ASPLINE_TYPE dx = v[i+1].x-v[i].x;
    v[i].c = (3.0*v[i].m-2.0*v[i].s-v[i+1].s)/dx;
    v[i].d = (v[i].s + v[i+1].s-2.0*v[i].m)/(dx*dx);
  }
}



void natural_aspline_refactor(struct aspline *a) {
  struct aspline_segment *v = a->v;
  if (a->n < 4) exit(EXIT_FAILURE);
  aspline_quad_solve(v, a->extrapolate); //left extrapolation
  aspline_quad_solve(v+a->n-3, a->extrapolate+1); //right extrapolation
  for (int64_t i=0; i<a->n-1; i++)
    v[i].m = (v[i+1].y-v[i].y)/(v[i+1].x-v[i].x);
  v[0].s = v[0].m;
  v[1].s = 0.5*(v[0].m+v[1].m);
  v[a->n-2].s = 0.5*(v[a->n-3].m+v[a->n-2].m);
  v[a->n-1].s = v[a->n-2].m;
  
  for (int64_t i=2; i<a->n-2; i++) {
    ASPLINE_TYPE d1 = fabsl(v[i+1].m - v[i].m);
    ASPLINE_TYPE d2 = fabsl(v[i-1].m - v[i-2].m);
    if (d1+d2>0) {
      v[i].s = (d1*v[i-1].m + d2*v[i].m)/(d1+d2);
    } else {
      v[i].s = 0.5*(v[i-1].m+v[i].m);
    }
  }
  for (int64_t i=0; i<a->n-1; i++) {
    v[i].a = v[i].y;
    v[i].b = v[i].s;
    ASPLINE_TYPE dx = v[i+1].x-v[i].x;
    v[i].c = (3.0*v[i].m-2.0*v[i].s-v[i+1].s)/dx;
    v[i].d = (v[i].s + v[i+1].s-2.0*v[i].m)/(dx*dx);
  }

  //Parabolic extension:
  aspline_parabolic_termination(a->extrapolate, v);
  aspline_parabolic_termination(a->extrapolate+1, v+a->n-2);
}

int aspline_search(const void *a, const void *b) {
  const struct aspline_segment *c = a;
  const struct aspline_segment *d = b;
  if (c->x < d->x) return -1;
  if (d->x < c->x) return 1;
  return 0;
}

//Note that no duplicated or nan values are allowed in the x array.
int64_t aspline_lookup_interval(struct aspline *a, ASPLINE_TYPE x) {
  if (!a->n) exit(EXIT_FAILURE); //Not our problem...
  if (x<a->v[0].x) return -1;
  if (x>=a->v[a->n-1].x) return -2; //Use extrapolation in this case
  if (isnan(x)) exit(EXIT_FAILURE);  //Not our problem...
  int64_t lo=0, hi=a->n-1, ans=0;
  while (lo <= hi) {
    int64_t mid = lo + (hi-lo)/2;
    if (a->v[mid].x <= x) {
      ans = mid;
      lo = mid+1;
    } else {
      hi = mid-1;
    }
  }
  return ans;
}

struct aspline_segment *aspline_lookup_segment(struct aspline *a, int64_t interval) {
  if (interval>=0 && interval<(a->n-1)) return a->v+interval;
  if (interval==-1) return a->extrapolate;
  if (interval==-2 || interval>=a->n-1) return a->extrapolate+1;
  fprintf(stderr, "[Error]: Invalid spline interval passed (%"PRId64"), expected range of -2 to %"PRId64".\n", interval, a->n-1);
  exit(EXIT_FAILURE);
  return NULL;
}


void aspline_add_points(struct aspline *a, ASPLINE_TYPE *x, ASPLINE_TYPE *y, int64_t n) {
  if (n<=0) exit(EXIT_FAILURE);
  check_realloc_s(a->v, sizeof(struct aspline_segment), a->n+n);
  for (int64_t i=0; i<n; i++) {
    a->v[a->n+i].x = x[i];
    a->v[a->n+i].y = y[i];
    a->v[a->n+i].uncertainty = 0;
  }
  a->n += n;
  qsort(a->v, a->n, sizeof(struct aspline_segment), aspline_search);
  if (a->n>1) {
    for (int64_t i=0; i<a->n-1; i++) {
      if (a->v[i].x==a->v[i+1].x) {
	fprintf(stderr, "[Error]: Duplicate x-value %Lf in spline.\n", a->v[i].x);
	exit(EXIT_FAILURE);
      }
      if (isnan(a->v[i].x) || isnan(a->v[i].y)) {
	fprintf(stderr, "[Error]: NaN passed to spline: (%Lf, %Lf).\n", a->v[i].x, a->v[i].y);
	assert(0);
	exit(EXIT_FAILURE);
      }
    }
  }
  if (a->n>=4)
    parabolic_aspline_refactor(a);    
  else _aspline_psolve(a);
  a->flags -= (a->flags & ASPLINE_UNCERTAINTIES_CALCULATED);
}

ASPLINE_TYPE aspline_eval(struct aspline *a, ASPLINE_TYPE x) {
  int64_t res = aspline_lookup_interval(a, x);
  struct aspline_segment *v = aspline_lookup_segment(a, res);
  ASPLINE_TYPE p = x-v->x;
  ASPLINE_TYPE p2 = p*p;
  ASPLINE_TYPE p3 = p2*p;
  return v->a + v->b*p + v->c*p2 + v->d*p3;
}

ASPLINE_TYPE _aspline_eval_derivative(struct aspline_segment *v, ASPLINE_TYPE x) {
  ASPLINE_TYPE p = x-v->x;
  ASPLINE_TYPE p2 = p*p;
  return v->b + 2.0*v->c*p + 3.0*v->d*p2;
}

ASPLINE_TYPE aspline_eval_derivative(struct aspline *a, ASPLINE_TYPE x) {
  int64_t res = aspline_lookup_interval(a, x);
  struct aspline_segment *v = aspline_lookup_segment(a, res);
  return(_aspline_eval_derivative(v, x));
}

ASPLINE_TYPE _aspline_eval_second_derivative(struct aspline_segment *v, ASPLINE_TYPE x) {
  ASPLINE_TYPE p = x-v->x;
  return 2.0*v->c + 2.0*3.0*v->d*p;
}

ASPLINE_TYPE aspline_eval_second_derivative(struct aspline *a, ASPLINE_TYPE x) {
  int64_t res = aspline_lookup_interval(a, x);
  struct aspline_segment *v = aspline_lookup_segment(a, res);
  return(_aspline_eval_second_derivative(v, x));
}


void _aspline_estimate_uncertainties(struct aspline *a) {
  for (int64_t i=0; i<a->n; i++) a->v[i].uncertainty = 0;
  if (a->n<4) return;
  struct aspline b = {0};
  struct aspline_segment v[5] = {0};
  b.v = v;
  for (int64_t i=0; i<a->n; i++) {
    b.n=0;
    int64_t min_j = i-2;
    int64_t max_j = min_j+5;
    if (max_j > a->n) {
      min_j -= max_j - a->n;
      max_j = a->n;
    }
    if (min_j<0) min_j=0;

    for (int64_t j=min_j; j<max_j; j++) {
      if (j==i) continue;
      b.v[b.n] = a->v[j];
      b.n++;
    }
    if (b.n>=4) parabolic_aspline_refactor(&b);    
    else _aspline_psolve(&b);
    a->v[i].uncertainty = aspline_eval(&b, a->v[i].x)-a->v[i].y;
  }
  a->flags |= ASPLINE_UNCERTAINTIES_CALCULATED;
}



//Returns positive uncertainty coefficients: lin*x + quad*x*x
int64_t _aspline_estimate_uncertainty_coefficients(struct aspline *a, int64_t interval, ASPLINE_TYPE *lin, ASPLINE_TYPE *quad) {
  if (!(a->flags & ASPLINE_UNCERTAINTIES_CALCULATED)) _aspline_estimate_uncertainties(a);
  struct aspline_segment *v = aspline_lookup_segment(a, interval);
  *lin = *quad = 0;
  if (v-a->v >= (a->n-1)) return 0;
  struct aspline_segment *nv = v+1;
  ASPLINE_TYPE C = a->uncertainty_scaling*4.0*sqrt(0.5*((v->uncertainty*v->uncertainty) + (nv->uncertainty*nv->uncertainty)))*pow(nv->x - v->x, -2);
  *lin = (nv->x - v->x)*C;
  *quad = -C;
  return 1;
}

//Returns positive uncertainty
ASPLINE_TYPE aspline_estimate_uncertainty(struct aspline *a, ASPLINE_TYPE x) {
  int64_t interval = aspline_lookup_interval(a, x);
  ASPLINE_TYPE b, c;
  if (!_aspline_estimate_uncertainty_coefficients(a, interval, &b, &c)) return -1;
  struct aspline_segment *v = aspline_lookup_segment(a, interval);
  x -= v->x;
  return (b*x + c*x*x);
}


int64_t aspline_count_roots(struct aspline *a, ASPLINE_TYPE xmin) { //Counts roots for x>=xmin
  int64_t nroots=0;
  ASPLINE_TYPE roots[3];
  int64_t start_i = aspline_lookup_interval(a, xmin);
  //fprintf(stderr, "Npoints: %"PRId64"\n", a->n);
  if (start_i == -2) start_i = a->n-1;
  for (int64_t i=start_i; i<a->n; i++) {
    struct aspline_segment *v = aspline_lookup_segment(a, i);
    struct aspline_segment *nv = (i<(a->n-1)) ? a->v+i+1 : NULL;
    ASPLINE_TYPE seg_x_start =  (i<(a->n-1)) ? v->x : a->v[a->n-1].x;
    int64_t res = aspline_cubic_solution_blinn(v->a, v->b, v->c, v->d, roots);
    //fprintf(stderr, "Segment %"PRId64": %Lf %Lf %Lf %Lf %Lf %Lf; %"PRId64"\n", i, v->x, v->y, v->a, v->b, v->c, v->d, res);
    for (int64_t j=0; j<res; j++) {
      roots[j] += v->x;
      if (roots[j]<seg_x_start || (nv && roots[j]>=nv->x)) continue;
      //fprintf(stderr, "Root in segment %"PRId64" %Lf - %Lf at %.12Lf.\n", i, seg_x_start, (nv? nv->x : 1e10), roots[j]);
      nroots++;
    }
  }
  return nroots;
}

ASPLINE_TYPE aspline_search_nth_root(struct aspline *a, int64_t n, ASPLINE_TYPE xmin) {
  int64_t nroots=0;
  ASPLINE_TYPE roots[3];
  ASPLINE_TYPE last_root = 0;
  int print_position = 0;
  int64_t start_i = aspline_lookup_interval(a, xmin);
  //if (a->n >=4 && fabsl(a->v[3].x-0.722569)<1e-5) print_position=1;
  if (print_position) fprintf(stderr, "Npoints: %"PRId64"; start_i: %"PRId64"\n", a->n, start_i);
  if (start_i == -2) start_i = a->n-1;
  for (int64_t i=start_i; i<a->n; i++) {
    struct aspline_segment *v = aspline_lookup_segment(a, i);
    struct aspline_segment *nv = (i<(a->n-1)) ? a->v+i+1 : NULL;
    ASPLINE_TYPE seg_x_start =  (i<(a->n-1)) ? v->x : a->v[a->n-1].x;
    int64_t res = aspline_cubic_solution_blinn(v->a, v->b, v->c, v->d, roots);
    if (print_position && (i>5 && i<9)) {
      fprintf(stderr, "Segment %"PRId64": %Lf - %Lf, %Lf %Lf %Lf\n", i, a->v[i].x, a->v[i+1].x, roots[0]+a->v[i].x, roots[1]+a->v[i].x, roots[2]+a->v[i].x);
      fprintf(stderr, "%Le %Le %Le %Le\n", v->a, v->b, v->c, v->d);
    }
    for (int64_t j=0; j<res && nroots<n; j++) {
      roots[j] += v->x;
      if (roots[j]<seg_x_start || (nv && roots[j]>=nv->x) || roots[j]<xmin) continue;
      nroots++;
      if (nroots==n && i<a->n-1) {
	if (print_position) fprintf(stderr, "Expected root at %.12Lf.\n", roots[j]);
	return roots[j];
      }
      last_root = roots[j];
    }

    //Check for maxima/minima that might poke above/below 0 in regions with no roots
    if (nv) {
      ASPLINE_TYPE b=0, c=0;
      _aspline_estimate_uncertainty_coefficients(a, i, &b, &c);
      for (int sign = 1; sign > -2; sign-=2) {
	//Find max/min of curve +/- uncertainty.  Rationale:
	// evaluating the max/min will give us the most information about whether the curve is
	// likely to poke above/below zero 
	res = aspline_find_maxmin(v->a, v->b+sign*b, v->c+sign*c, v->d, roots); 
	for (int64_t j=0; j<res; j++) {
	  roots[j] += v->x;
	  if (roots[j]<seg_x_start || (nv && roots[j]>=nv->x) || roots[j]<xmin) continue;
	  ASPLINE_TYPE maxmin_v = aspline_eval(a, roots[j]);
	  ASPLINE_TYPE maxmin_d = aspline_eval_second_derivative(a, roots[j]);
	  //Skip the case where max/min already known to poke above zero, so roots are already accounted for
	  if (maxmin_v * maxmin_d < 0) continue; 
	  //We want to check instead if the uncertainty will lead to potential roots that we missed earlier:
	  ASPLINE_TYPE uncertainty = aspline_estimate_uncertainty(a, roots[j]);
	  if ((maxmin_d < 0 && ((maxmin_v+uncertainty) > 0)) ||  //maximum
	      (maxmin_d > 0 && ((maxmin_v-uncertainty) < 0)))  //minimum
	    {
	      return(roots[j]); //Make sure that the maximum / minimum is actually below / above zero
	    }
	}
      }
    }
  }
  //Nth root not found.
  ASPLINE_TYPE last_x = a->v[a->n-1].x;
  ASPLINE_TYPE previous_x = a->v[a->n-2].x;

  //Basic Heuristic: look for the first maximum/minimum/root in the extrapolated region

  //Find next max/min
  ASPLINE_TYPE next_maxmin = 0;
  struct aspline_segment *v = a->extrapolate+1;
  if (v->c!=0) {
    next_maxmin = -v->b/(2.0l*v->c)+v->x;
  }
  
  //Start with next root, move to max/min if closer AND a->n > 3
  ASPLINE_TYPE new_trial = (last_root > last_x) ? last_root : 0;
  if (print_position) fprintf(stderr, "New trial: %Lf", new_trial);
  if ((next_maxmin > last_x) && ((new_trial==0) || (new_trial > next_maxmin && a->n > 3))) new_trial = next_maxmin;
  if (print_position) fprintf(stderr, " %Lf", new_trial);

  //If nothing found, use the distance to the previous maximum/minimum as a characteristic distance
  if (new_trial==0 && next_maxmin!=0) new_trial = last_x + (last_x - next_maxmin);
  if (print_position) fprintf(stderr, " %Lf", new_trial);

  //If no previous max/min (unlikely), increase the search distance by a factor of 5
  if (new_trial==0) new_trial = last_x + 5.0l*(last_x-previous_x);
  if (print_position) fprintf(stderr, " %Lf", new_trial);

  //Limit searches from exploding in size, needed for root finding for sin/cos
  ASPLINE_TYPE max_distance = last_x + 5.0l*(last_x-previous_x);
  ASPLINE_TYPE intermediate_distance = last_x + 3.0l*(last_x-previous_x);
  if ((a->n>3) && (new_trial > max_distance)) new_trial = max_distance;
  if (print_position) fprintf(stderr, " %Lf", new_trial);
  if ((a->n>3) && (new_trial > intermediate_distance)) new_trial = 0.5*(last_x+new_trial);
  if (print_position) fprintf(stderr, " %Lf", new_trial);

  //Avoid asymptotically creeping towards a solution
  if ((new_trial > last_x) && (a->n>3) && (new_trial<max_distance)) {
    new_trial += (last_x-previous_x)*0.2; 
  }
  if (print_position) fprintf(stderr, " %Lf\n", new_trial);
  return new_trial;
}

void aspline_free(struct aspline **a) {
  if ((*a)==NULL) return;
  if ((*a)->v) { free((*a)->v); (*a)->v = NULL; }
  free(*a);
  *a = NULL;
}

#ifdef ASPLINE_MAIN

ASPLINE_TYPE interp_fn(ASPLINE_TYPE x) {
  return sinl(x); //cosl(x);
}

ASPLINE_TYPE interp_fn_derivative(ASPLINE_TYPE x) {
  return -sinl(x);
}


void test_quad_solution(ASPLINE_TYPE a, ASPLINE_TYPE b, ASPLINE_TYPE c) {
  ASPLINE_TYPE roots[2] = {0};
  int n = aspline_quadratic_solution(a,b,c,roots, roots+1);
  aspline_improve_quad_roots(c, b, a, 0, roots, n);
  printf("%Lex^2 + %Lex + %Le=0: %d roots; %Le (%Le) %Le (%Le)\n", a, b, c, n, roots[0], a*roots[0]*roots[0]+b*roots[0]+c,
	 roots[1], a*roots[1]*roots[1]+b*roots[1]+c);
}

ASPLINE_TYPE eval_cubic(ASPLINE_TYPE  a, ASPLINE_TYPE b, ASPLINE_TYPE c, ASPLINE_TYPE d, ASPLINE_TYPE x) {
  ASPLINE_TYPE x2 = x*x;
  ASPLINE_TYPE x3 = x2*x;
  return d + c*x + b*x2 + a*x3;
}

void test_cubic_solution(ASPLINE_TYPE a, ASPLINE_TYPE b, ASPLINE_TYPE c, ASPLINE_TYPE d) {
  ASPLINE_TYPE roots[3] = {0};
  int n = aspline_cubic_solution_blinn(d, c, b, a,roots);
  //aspline_improve_cubic_roots(d, c, b, a, roots);
  printf("%Lex^3 + %Lex^2 + %Lex + %Le=0: %d roots;", a, b, c, d, n);
  for (int i=0; i<n; i++) 
    printf(" %Le (%Le)", roots[i], eval_cubic(a,b,c,d,roots[i]));
  printf("\n");
}


#define NPOINTS 20
int main(void) {
  ASPLINE_TYPE x[NPOINTS] = {0};
  ASPLINE_TYPE y[NPOINTS];
  x[1] = 0.01;
  x[2] = -0.01;

  //Test quad solver:
  test_cubic_solution(0,0,0,0);
  test_cubic_solution(0, 0,1e-32,1e-16);
  test_cubic_solution(0, 0,1,2);
  test_cubic_solution(0, 1e-32,1,2);
  test_cubic_solution(0, 1,2,-1);
  test_cubic_solution(0, 1,2,1);
  test_cubic_solution(0, 2,0,0);
  test_cubic_solution(0, 2,0,-2);
  printf("\n");
  test_cubic_solution(1,0,0,0);
  test_cubic_solution(1e-16,1,2,-1);
  test_cubic_solution(1e-8,1,2,-1);
  test_cubic_solution(1,-5,8,-4); //B^3 D < AC^3
  test_cubic_solution(8,2,1,1); //B^3 D = AC^3, 1 root, C_bar > 0
  test_cubic_solution(8.1,2,1,1); //B^3 D < AC^3, 1 root, C_bar > 0
  test_cubic_solution(7.9,2,1,1); //B^3 D > AC^3, 1 root, C_bar > 0

  test_cubic_solution(1,1,2,8); //B^3 D = AC^3, 1 root, C_bar < 0
  test_cubic_solution(1,1,2,8.1); //B^3 D > AC^3, 1 root, C_bar < 0
  test_cubic_solution(1,1,2,7.9); //B^3 D < AC^3, 1 root, C_bar < 0

  test_cubic_solution(1,1,0,0); //C_bar = D_bar = 0
  test_cubic_solution(1,-0.5,-0.25,0.125); //C_bar = D_bar = 0
  test_cubic_solution(-2.000450e-15, 1.966817e-01, -3.317995e-01, 5.467307e-02);
    
  return 0;
  for (int64_t i=0; i<3; i++)
    y[i] = interp_fn(x[i]);
  
  struct aspline *a = aspline_new();
  aspline_add_points(a, x, y, 3);
  while (a->n<NPOINTS-1) {
    ASPLINE_TYPE new_trial = aspline_search_nth_root(a, 5, 0);
    x[a->n] = new_trial;
    y[a->n] = interp_fn(x[a->n]);
    aspline_add_points(a, x+a->n, y+a->n, 1);
    if (y[a->n-1]>1e4) break;
    if (fabs(y[a->n-1])<1e-7) break;
  }

  for (int64_t i=0; i<1000; i++) {
    double x = (i*0.001*(a->v[a->n-1].x+3.0))-1.0;
    printf("%f %Lf %Lf\n", x, interp_fn(x), aspline_eval(a, x));
  }
  
  fprintf(stderr, "Total roots: %"PRId64"\n", aspline_count_roots(a,0));
  return 0;
}
#endif /*ASPLINE_MAIN defined*/
