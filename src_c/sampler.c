/**************************************
Acrobatic Sampler Collection
Including:
Angular/Standard Metropolis Sampler
Alexander Hamiltonian Sampler 
Adaptive Searchlight Sampler
Copyright (C) 2019-2024, Peter Behroozi
License: MIT/Expat

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <assert.h>
#include "mt_rand.h"
#include "check_syscalls.h"
#include "sampler.h"
#include "jacobi_dim.h"
#include "asplines.h"

/**************************
Sampler init/free functions
***************************/

//Allocates and returns a new sampler object
struct Sampler *new_sampler(int64_t num_walkers, int64_t dims, int64_t total_samples) {
  int64_t i;
  struct Sampler *e = NULL;
  assert(num_walkers > 0 && dims > 0);
  
  r250_init(6962772806745128773L); //Use a large prime
  check_calloc_s(e, sizeof(struct Sampler), 1);
  e->num_walkers = num_walkers;
  e->dims = dims;
  e->resampling_dims = dims;
  e->step_thinning = e->dimension_thinning = 1;
  e->mode = ALEXANDER_SAMPLER;
   
  //Default exploration properties; temp set further below
  e->adaptive_samples = total_samples * SAMPLER_F_ADAPT;
  e->ln_prob_reject_threshold = SAMPLER_DELTA_PROB;
  e->tmax_scatter = SAMPLER_TMAX_SCATTER;
  e->theta_rotate = SAMPLER_THETA_ROTATE;
  e->success_target = SAMPLER_SUCCESS_TARGET;
  e->running_avg_length = SAMPLER_RUNNING_AVERAGE_LENGTH;
  e->dt = SAMPLER_DT;
  e->dv = SAMPLER_DV;
  e->exploration_dx = SAMPLER_EXPLORATION_DX;
  e->velocity_scaling = 1;
  e->tmax_scaling = 1;
  e->exploration_max_steps = 10 + log(dims)/log(2);
  e->avg_ln_prob = SAMPLER_INVALID_LN_PROB;
  e->tunneling_threshold = 20.0*sqrt(e->dims)+SAMPLER_SEARCHLIGHT_TUNNELING_THRESHOLD;
  e->searchlight_max_steps = SAMPLER_SEARCHLIGHT_MAX_STEPS;
  
  //Success Array
  sampler_init_success_array(e, e->dt);
  
  //For angular metropolis
  e->dtheta = SAMPLER_DTHETA;
  e->dr = SAMPLER_DR;
  e->boundary_retries = SAMPLER_DEFAULT_BOUNDARY_RETRIES;
  
  //For searchlight:
  e->metropolis_frac = SAMPLER_DEFAULT_METROPOLIS_FRAC;
  e->lnp_tol = SAMPLER_DEFAULT_LNP_TOL;
  e->dx_tol = SAMPLER_DEFAULT_DX_TOL;
  
  //Origins
  e->num_origins = 1;
  check_calloc_s(e->origins, sizeof(double), e->dims);
  e->origin_type = SAMPLER_ORIGIN_AVERAGE;

  //Static dimensions
  check_calloc_s(e->dim_flags, sizeof(char), e->dims);

  //Walkers
  check_calloc_s(e->w, sizeof(struct Walker), num_walkers);
  for (i=0; i<e->num_walkers; i++) sampler_setup_walker(e, e->w+i);
  int64_t basis_dims = SAMPLER_DEFAULT_BASIS_DIMS;
  if (basis_dims > dims) basis_dims = dims;
  sampler_set_basis_dims(e, basis_dims); //Also allocates queues
  sampler_set_temp(e, 1);
  
  //Summary statistics of distribution
  check_calloc_s(e->avg, sizeof(double), e->dims);
  check_calloc_s(e->new_avg, sizeof(double), e->dims);
  e->best_ln_prob = SAMPLER_INVALID_LN_PROB;
  check_calloc_s(e->eigenvalues, sizeof(double), e->dims);
  e->use_covariance_matrix = (e->dims > SAMPLER_COV_MATRIX_DIM_THRESH) ? 0 : 1;
  if (e->adaptive_samples == 0) e->use_covariance_matrix = 0;
  sampler_use_covariance_matrix(e, e->use_covariance_matrix);
  sampler_set_identity_covariance(e);
  sampler_reset_min_covariance_steps(e);

  //Temporary space for converting to/from orthonormal basis
  check_calloc_s(e->stack, sizeof(double), SAMPLER_STACKSIZE*e->dims);

  //Output
  e->output_file = stdout;
  e->error_file = stderr;
  e->save_adaptive_steps = 1;
  return e;
}


//Releases all allocated memory
void free_sampler(struct Sampler *e) {
  if (!e) return;
  if (e->w) {
    for (int64_t i=0; i<e->num_walkers; i++) {
      struct Walker *w = e->w+i;
      check_free(w->params);
      check_free(w->derivatives);
      check_free(w->basis);
      check_free(w->x);
      check_free(w->v);
      aspline_free(&(w->aspline));
    }
    free(e->w);
  }

  check_free(e->origins);
  check_free(e->dim_flags);
  check_free(e->avg);
  check_free(e->new_avg);
  check_free(e->eigenvalues);
  check_free(e->cov_matrix);
  check_free(e->orth_matrix);
  check_free(e->variance);
  check_free(e->queue);
  check_free(e->queue_params);
  check_free(e->stack);
  free(e);
}


//Sets sampler type
void sampler_set_type(struct Sampler *e, int64_t mode) {
  e->mode = mode;
}


//Called to initialize each walker
void sampler_setup_walker(struct Sampler *e, struct Walker *w) {
  w->dims = e->dims;
  w->dim_flags = e->dim_flags;
  w->dt_factor = 1;
  w->success_streak = 1;
  w->initial_ln_prob = w->initial_net_prob = SAMPLER_INVALID_LN_PROB;
  w->origin = e->origins;
  check_realloc_s(w->params, sizeof(double), e->dims);
}

//Initializes rest of walker structure; allocates queues
void sampler_set_basis_dims(struct Sampler *e, int64_t basis_dims) {
  int64_t i;
  //Validate basis dims
  if (basis_dims > (e->dims - e->static_dims)) {
    basis_dims = e->dims - e->static_dims;
    check_log(e->error_file, "[Sampler:Warning] Reducing basis dims to max allowed: %"PRId64"\n", basis_dims);
    assert(basis_dims > 0);
  }

  //Set basis dims for all walkers
  e->basis_dims = basis_dims;
  for (i=0; i<e->num_walkers; i++)
    sampler_set_walker_basis_dims(e, e->w+i, basis_dims); //Sets w->num_queue
  
  //Allocate and setup queues
  e->queue_allocated = e->num_walkers*e->w[0].num_queue;
  check_calloc_s(e->queue, sizeof(struct QueueSample), e->queue_allocated);
  check_calloc_s(e->queue_params, sizeof(double), e->dims*e->queue_allocated);
  for (i=0; i<e->num_walkers; i++) e->w[i].queue = e->queue + (e->w[i].num_queue)*i;
  for (i=0; i<e->queue_allocated; i++) {
    e->queue[i].params = e->queue_params + i*(e->dims);
    e->queue[i].state = QUEUESAMPLE_COMPLETE;
  }
}

//Allocates walker structure that is dependent on # of basis dims; sets t_max
void sampler_set_walker_basis_dims(struct Sampler *e, struct Walker *w, int64_t basis_dims) {
  w->basis_dims = basis_dims;
  w->num_queue = basis_dims+1;
  w->queue_expected = w->num_queue;
  if (w->num_queue < SAMPLER_MIN_QUEUE_SIZE) w->num_queue = SAMPLER_MIN_QUEUE_SIZE;
  check_realloc_s(w->derivatives, sizeof(double), w->num_queue);
  check_realloc_s(w->basis, sizeof(double), w->dims*w->basis_dims);
  check_realloc_s(w->x, sizeof(double), w->basis_dims);
  check_realloc_s(w->v, sizeof(double), w->basis_dims);
  sampler_set_t_max(w);
  if (e->velocity_scaling) w->t_max /= e->velocity_scaling;
}

//Sets maximum integration time
//Optimal t_max = 0.5 * pi * sqrt((D-1) / ((N-1)*(1-1/N))), from Behroozi+ 2022.
void sampler_set_t_max(struct Walker *w) {
  double D = w->dims - w->static_dims;
  double N = w->basis_dims;
  w->t_max = 0.5*M_PI*sqrt((D-1.0)/((N-1.0)/(1.0-1.0/N)));
}

//Sets whether to use a full covariance matrix or to keep only diagonal components
void sampler_use_covariance_matrix(struct Sampler *e, int64_t use_cov_matrix) {
  e->use_covariance_matrix = use_cov_matrix;
  if (e->use_covariance_matrix) {
    check_calloc_s(e->cov_matrix, sizeof(double), e->dims*e->dims);
    check_calloc_s(e->orth_matrix, sizeof(double), e->dims*e->dims);
    for (int64_t i=0; i<e->dims; i++) e->orth_matrix[i*e->dims+i] = 1;
    check_free(e->variance);
  } else {
    check_calloc_s(e->variance, sizeof(double), e->dims);
    check_free(e->cov_matrix);
    check_free(e->orth_matrix);
  }
  e->avg_steps = 0;
}

//Sets covariance matrix to unity
void sampler_set_identity_covariance(struct Sampler *e) {
  int64_t i;
  sampler_clear_averages(e);
  if (e->use_covariance_matrix) {
    for (i=0; i<e->dims; i++) e->orth_matrix[i*e->dims + i] = 1;
  }
  for (i=0; i<e->dims; i++) e->eigenvalues[i] = 1;
  e->last_adaptation = 0;
}

//Sets sampler temperature; 1 is default.
void sampler_set_temp(struct Sampler *e, double temp) {
  assert(temp > 0);
  e->inv_temp = 1.0/temp;
  for (int64_t i=0; i<e->num_walkers; i++) e->w[i].inv_temp = e->inv_temp;
  check_log(e->log_file, "#Set sampler temperature to %g.\n", temp);
}

/********************************
Self-explanatory settor functions
*********************************/
void sampler_set_adaptive_samples(struct Sampler *e, int64_t adaptive_samples)
{ e->adaptive_samples = adaptive_samples; }

void sampler_set_target_success_ratio(struct Sampler *e, double ratio)
{ e->success_target = ratio; }

void sampler_set_output_file(struct Sampler *e, FILE *output)
{ e->output_file = output; }

void sampler_set_log_file(struct Sampler *e, FILE *output)
{ e->log_file = output; }

void sampler_check_state(struct Sampler *e, int64_t state, char *name) {
  char *states[6] = {"before walker position initialization",
		     "before full exploration begins",
		     "before full exploration begins",
		     "before full exploration begins",
		     "before full exploration begins",
		     "during full exploration"};
  if (e->state != state) {
    check_log(e->error_file, "[Sampler:Error] sampler_%s() must be called %s.\n",
	      name, states[state]);
    exit(EXIT_FAILURE);
  }
}

void sampler_set_valid_function(struct Sampler *e, int (*valid)(const double *)) {
  sampler_check_state(e, SAMPLER_STATE_INVALID, "set_valid_function");
  e->valid = valid;
}

void sampler_set_parameterization_function(struct Sampler *e, void (*param)(double *))
{ e->enforce_parameterization = param; }

void sampler_save_adaptive_steps(struct Sampler *e, int64_t save_steps)
{ e->save_adaptive_steps = save_steps; }

void sampler_set_step_completed_function(struct Sampler *e, void (*step_completed)(int64_t, const double *, double)) {
  e->step_completed = step_completed;
  e->output_file = NULL;
}

void sampler_change_origin_type(struct Sampler *e, int64_t type)
{ e->origin_type = type; }

//For Standard Metropolis:
void sampler_set_step_scaling(struct Sampler *e, double step_scale) {
  check_log(e->log_file, "#Setting DR to %g.\n", step_scale);
  e->dr = step_scale;
}

//For Angular Metropolis:
void sampler_set_radial_scaling(struct Sampler *e, double radial_scale) {
  check_log(e->log_file, "#Setting DR to %g.\n", radial_scale);
  e->dr = radial_scale;
}

void sampler_set_theta_scaling(struct Sampler *e, double theta_scale) {
  check_log(e->log_file, "#Setting DTheta to %g.\n", theta_scale);
  e->dtheta = theta_scale;
}

//For Alexander Hamiltonian:
void sampler_set_ln_prob_reject_threshold(struct Sampler *e, double threshold)
{ e->ln_prob_reject_threshold = threshold; }

void sampler_set_tmax_scatter(struct Sampler *e, double scatter)
{ e->tmax_scatter = scatter; }

void sampler_set_theta_rotate(struct Sampler *e, double theta) {
  check_log(e->log_file, "#Setting theta_rotate to %g.\n", theta);
  e->theta_rotate = theta;
}

void sampler_set_dt(struct Sampler *e, double dt) {
  check_log(e->log_file, "#Setting DT to %g.\n", dt);
  e->dt = dt;
}

void sampler_set_tmax_scaling(struct Sampler *e, double tmax_scaling)
{ e->tmax_scaling = tmax_scaling; }

void sampler_set_numerical_derivative_step(struct Sampler *e, double dv)
{ e->dv = dv; }

void sampler_set_exploration_derivative_step(struct Sampler *e, double dx)
{ e->exploration_dx = dx; }


//Sets scaling of the Hamiltonian initial velocity distribution
//Default is the same size as the covariance matrix
void sampler_set_velocity_scaling(struct Sampler *e, double velocity_scaling) {
  e->velocity_scaling = velocity_scaling;
  for (int64_t i=0; i<e->num_walkers; i++) {
    sampler_set_t_max(e->w+i);
    e->w[i].t_max /= velocity_scaling;
  }
  e->dt = 1.0/velocity_scaling;
}


//Designates a dimension as non-explorable
void sampler_set_static_dim(struct Sampler *e, int64_t dim) {
  assert(dim >= 0);
  assert(dim < e->dims);
  assert(e->dims > e->static_dims + e->basis_dims);
  sampler_check_state(e, SAMPLER_STATE_INVALID, "set_static_dim");
  if (!SAMPLER_STATIC_DIM(e,dim)) {
    e->dim_flags[dim] |= SAMPLER_STATIC_DIM_FLAG;
    e->static_dims++;
    for (int64_t i=0; i<e->num_walkers; i++) e->w[i].static_dims = e->static_dims;
    sampler_set_velocity_scaling(e, e->velocity_scaling); //Resets t_max for all walkers
  }
  sampler_reset_min_covariance_steps(e);
}

//Resets all dimensions to being explorable again
void sampler_clear_static_dims(struct Sampler *e) {
  for (int64_t i=0; i<e->dims; i++)
    e->dim_flags[i] -= (e->dim_flags[i] & SAMPLER_STATIC_DIM_FLAG); 
  e->static_dims = 0;
  for (int64_t i=0; i<e->num_walkers; i++) e->w[i].static_dims = e->static_dims;
  sampler_set_velocity_scaling(e, e->velocity_scaling); //Resets t_max for all walkers
  sampler_reset_min_covariance_steps(e);
}

//Designates a dimension as metropolis-explorable
void sampler_set_metropolis_dim(struct Sampler *e, int64_t dim) {
  if (e->mode != ANGULAR_METROPOLIS_SAMPLER) {
    check_log(e->error_file, "[Sampler:Error] Setting metropolis dimensions only valid for Angular Metropolis sampler.\n");
    check_log(e->error_file, "[Sampler:Error] Switch sampler mode to Angular Metropolis before setting metropolis dimensions.\n");
    exit(EXIT_FAILURE);
  }
  e->mix_metropolis = 1;
  e->dim_flags[dim] |= SAMPLER_METROPOLIS_DIM_FLAG;
}

//Designates a dimension as metropolis-explorable
void sampler_clear_metropolis_dim(struct Sampler *e, int64_t dim) {
  e->dim_flags[dim] -= (e->dim_flags[dim] & SAMPLER_METROPOLIS_DIM_FLAG);
}


//Assigns minimum covariance steps
void sampler_reset_min_covariance_steps(struct Sampler *e) {
  e->min_covariance_steps = SAMPLER_MIN_COVARIANCE_STEPS;
  e->steps_per_adaptation = SAMPLER_STEPS_PER_ADAPTATION;
}

/***************
Stack Management
****************/

double *sampler_get_stack(struct Sampler *e) {
  if (e->stack_counter==SAMPLER_STACKSIZE) {
    check_log(e->error_file, "[Sampler:Error] Maximum sampler stack size exceeded.  This is a sampler code bug.\n");
    exit(EXIT_FAILURE);
  }
  e->stack_counter++;
  return e->stack+(e->stack_counter-1)*e->dims;
}

void sampler_release_stack(struct Sampler *e, double **ptr) {
  e->stack_counter--;
  if ((*ptr) != (e->stack+(e->stack_counter*e->dims))) {
    check_log(e->error_file, "[Sampler:Error] Stack released out of order.  This is a sampler code bug.\n");
    exit(EXIT_FAILURE);
  }
  *ptr = NULL;
}

/**************************
Origin management
***************************/

//Copy current average to origin
void sampler_set_origin_as_average(struct Sampler *e) {
  memcpy(e->origins, e->avg, sizeof(double)*e->dims);
}

//Test if parameter set is better than that for current origin
void sampler_check_bestfit_origin(struct Sampler *e, double *params, double ln_prob) {
  if (ln_prob == SAMPLER_INVALID_LN_PROB) return;
  if ((e->best_ln_prob != SAMPLER_INVALID_LN_PROB) && (e->best_ln_prob >= ln_prob)) return;
  memcpy(e->origins, params, sizeof(double)*e->dims);
  e->best_ln_prob = ln_prob;
}

//Allows the user to set origins manually.  Stops any automatic origin updates.
void sampler_add_to_origins(struct Sampler *e, double *params) {
  if (e->origin_type != SAMPLER_ORIGIN_MANUAL) {
    e->origin_type = SAMPLER_ORIGIN_MANUAL;
    e->num_origins = 0;
  }
  check_realloc_s(e->origins, sizeof(double), e->dims*(e->num_origins+1));
  memcpy(e->origins+(e->dims*e->num_origins), params, sizeof(double)*e->dims);
  e->num_origins++;
  for (int64_t i=0; i<e->num_walkers; i++) e->w[i].origin = e->origins;
}

//Clears any manually-set origins and returns the origin to the sampler average.
void sampler_clear_origins(struct Sampler *e) {
  e->origin_type = SAMPLER_ORIGIN_AVERAGE;
  e->num_origins = 1;
  check_realloc_s(e->origins, sizeof(double), e->dims);
  sampler_set_origin_as_average(e);
  for (int64_t i=0; i<e->num_walkers; i++) e->w[i].origin = e->origins;
}


/****************************
Walker initial position setup
*****************************/

//Set specific walker position, without specifying probability
void sampler_init_walker_position(struct Walker *w, double *params) {
  memcpy(w->params, params, sizeof(double)*w->dims);
  w->initial_ln_prob = w->initial_net_prob = SAMPLER_INVALID_LN_PROB;
}

//Set nonspecific walker position, without specifying probability
void sampler_init_position(struct Sampler *e, double *params) {
  int64_t i = (e->cur_walker % e->num_walkers);
  //Update averages if necessary; this occurs when resuming an existing run.
  if (i==0 && (e->cur_walker >= e->num_walkers) && (e->avg_steps < e->adaptive_samples)) {
    sampler_update_averages(e);
    sampler_stats_to_step(e);
    if (e->origin_type == SAMPLER_ORIGIN_AVERAGE) sampler_set_origin_as_average(e);
  }

  //Setup walker position and add to queue
  struct Walker *w = e->w+i;
  sampler_init_walker_position(w, params);
  for (int64_t j=0; j<w->basis_dims+1; j++) {
    struct QueueSample *q = sampler_reset_queue_sample(e, w, j);
    if (j==0) memcpy(q->params, params, sizeof(double)*e->dims);
    else q->state = QUEUESAMPLE_COMPLETE;
  }

  e->cur_walker++;
  e->accepted++;
  if (e->cur_walker >= e->num_walkers) e->state = SAMPLER_STATE_INITIAL_EVALUATION;
}

//Set nonspecific walker position, specifying probability
void sampler_init_position_lnprob(struct Sampler *e, double *params, double ln_prob) {
  sampler_init_position(e, params);
  int64_t i = (e->cur_walker-1) % e->num_walkers;
  e->w[i].initial_ln_prob = ln_prob;
  if (e->origin_type == SAMPLER_ORIGIN_BESTFIT) sampler_check_bestfit_origin(e, params, ln_prob);
  if (e->cur_walker >= e->num_walkers) e->state = SAMPLER_STATE_FULL_EXPLORATION;
  e->walkers_done++;
  if (e->walkers_done > e->num_walkers) e->walkers_done = e->num_walkers;
}

//For convenience, also allow specifying chi^2
void sampler_init_position_chi2(struct Sampler *e, double *params, double chi2) {
  sampler_init_position_lnprob(e, params, -0.5*chi2);
}


/*************
Validity Tests
**************/

//Find a new valid point in the direction of new-cur, assuming cur is valid
double sampler_find_valid_point(struct Sampler *e, const double *cur, double *new) {
  if (e->valid == NULL) return 1.0;
  
  double max_a = 1.0;
  double min_a = 0.0;
  if (e->valid(new)) return max_a;
  double *test_vector = sampler_get_stack(e);
  for (int64_t i=0; i<e->dims; i++) test_vector[i] = new[i] - cur[i];
  while ((max_a - min_a) > 1e-15) {
    double a = min_a+0.5*(max_a-min_a);
    for (int64_t i=0; i<e->dims; i++) new[i] = cur[i] + a*test_vector[i];
    if (e->valid(new)) min_a = a;
    else max_a = a;
  }
  for (int64_t i=0; i<e->dims; i++) new[i] = cur[i] + min_a*test_vector[i];
  sampler_release_stack(e, &test_vector);
  //check_log(e->log_file, "Returning %f\n", min_a);
  return min_a;
}

//Find a new valid point in the direction of new-cur, assuming cur is valid; negative values allowed
double sampler_find_valid_point_bidirectional(struct Sampler *e, const double *cur, double *new) {
  double res = sampler_find_valid_point(e, cur, new);
  if (res>1e-15) return res;
  for (int64_t i=0; i<e->dims; i++) new[i] = cur[i] - (new[i]-cur[i]);
  res = sampler_find_valid_point(e, cur, new);
  return -res;
}

double sampler_find_valid_limit(struct Sampler *e, const double *cur, const double *basis, double max) {
  if (e->valid == NULL) return max;
  
  double max_a = max;
  double min_a = 0.0;
  double *test_vector = sampler_get_stack(e);
  for (int64_t i=0; i<e->dims; i++) test_vector[i] = cur[i] + max*basis[i];
  if (e->valid(test_vector)) min_a = max;
  while ((max_a - min_a) > 1e-15*max_a) {
    double a = min_a+0.5*(max_a-min_a);
    for (int64_t i=0; i<e->dims; i++) test_vector[i] = cur[i] + a*basis[i];
    if (e->valid(test_vector)) min_a = a;
    else max_a = a;
  }
  sampler_release_stack(e, &test_vector);
  //check_log(e->log_file, "Returning %f\n", min_a);
  return min_a;  
}


/**************************
Generic Derivative Queueing
***************************/

//Add derivatives at current walker position to queue
void sampler_generic_queue_derivatives(struct Sampler *e, struct Walker *w, double dx, int64_t calc_volume) {
  int64_t i, j;
  double steps[3] = {0, -dx, dx};
  w->samples_present = 0;
  w->queue_expected = 3;
  
  //current/backward/forward difference
  for (j=0; j<3; j++) {
    struct QueueSample *q = sampler_reset_queue_sample(e, w, j);
    q->stepsize = steps[j];
    q->x = w->x[0] + q->stepsize;
    if (w->translate_function) w->translate_function(w, q->x, q->params, &(q->v));
    else {
      for (i=0; i<e->dims; i++)
	q->params[i] = w->params[i] + q->x*w->basis[i];
      if (calc_volume) q->ln_volume = sampler_calc_ln_volume(w, &(q->x), 1);
      if (w->ergodic_dims) {
	long double e_parallel = w->ergodic_length + q->x*w->ergodic_unit_parallel;
	long double e_perp =  q->x*w->ergodic_unit_perp;
	q->v = sqrtl(e_parallel*e_parallel + e_perp*e_perp);
      } else {
	q->v = 0;
      }
    }
  }
}

void sampler_skip_derivatives(struct Sampler *e, struct Walker *w) {
  if (w->queue_expected==3) {
    w->queue_expected = 1;
    w->queue[1].state = QUEUESAMPLE_SKIPPED;
    w->queue[2].state = QUEUESAMPLE_SKIPPED;
  }
}

void sampler_unskip_derivatives(struct Sampler *e, struct Walker *w) {
  if (w->queue_expected==1) {
    w->queue_expected = 3;
    w->queue[1].state = QUEUESAMPLE_NEW;
    w->queue[2].state = QUEUESAMPLE_NEW;
  }
}

//Compute first and second derivatives of total probability
void sampler_generic_derivatives(struct Sampler *e, struct Walker *w) {
  struct QueueSample *q = w->queue;
  double dx1 = (q[1].net_prob - q[0].net_prob) / q[1].stepsize; //Backward derivative
  double dx2 = (q[2].net_prob - q[0].net_prob) / q[2].stepsize; //Forward derivative
  w->derivatives[0] = 0.5*(dx1+dx2);
  //dx# is derivative at 0.5 x q[#].stepsize
  w->derivatives[1] =  (dx2 - dx1) / (0.5*(q[2].stepsize - q[1].stepsize)); //Second derivative
  if (w->ergodic_dims && e->state == SAMPLER_STATE_FULL_EXPLORATION) {
    double q0 = sampler_ergodic_ln_likelihood(q[0].v);
    double q1 = sampler_ergodic_ln_likelihood(q[1].v);
    double q2 = sampler_ergodic_ln_likelihood(q[2].v);
    dx1 = (q1-q0)/q[1].stepsize;
    dx2 = (q2-q0)/q[2].stepsize;
    w->derivatives[0] += 0.5*(dx1+dx2);
    w->derivatives[1] +=  (dx2 - dx1) / (0.5*(q[2].stepsize - q[1].stepsize)); //Second derivative
    //fprintf(stderr, "[Derivatives]: %f %f %f\n", q0, 0.5*(dx1+dx2), (dx2 - dx1) / (0.5*(q[2].stepsize - q[1].stepsize)));
    //fprintf(stderr, "[Total Derivatives]: %f %f %f\n", q[0].ln_prob+q0, w->derivatives[0], w->derivatives[1]);
    if (isnan(q0)) exit(EXIT_FAILURE);
  }
}

/*************************************
Initial exploration of parameter space
**************************************/

//Given an origin, setup directions for walkers to explore
void sampler_initial_exploration(struct Sampler *e, double *params, double *stddev) {
  int64_t i,j;
  e->state = SAMPLER_STATE_INITIAL_EXPLORATION;
  if (stddev && stddev != e->eigenvalues)
    memcpy(e->eigenvalues, stddev, sizeof(double)*e->dims);

  memcpy(e->origins, params, sizeof(double)*e->dims);
  double *new_params = sampler_get_stack(e);
  for (i=0; i<e->num_walkers; i++) {
    struct Walker *w = e->w + i;
    memcpy(w->params, params, sizeof(double)*e->dims);
    sampler_random_unit_walker(w, new_params);
    w->step = w->min_x = w->max_x = 0;
    sampler_translate_basis(e, w, new_params, w->basis, 0);
    w->x[0] = sampler_find_valid_limit(e, w->params, w->basis, 1.0);
    if (w->x[0] < 1.0) {
      check_log(e->log_file, "Limited walker %"PRId64" to %f\n", i, w->x[0]);
      w->x[0] *= 1.0-2.0*e->exploration_dx;
      for (j=0; j<e->dims; j++) w->basis[j] *= w->x[0];
      w->x[0] = 1;
    }
    sampler_exploration_queue_derivatives(e, w);
  }
  sampler_release_stack(e, &new_params);
}

//Add derivatives at current walker position to queue
void sampler_exploration_queue_derivatives(struct Sampler *e, struct Walker *w) {
  sampler_generic_queue_derivatives(e, w, e->exploration_dx, 1);
}

//Finalize walker setup when exploration has completed
void sampler_finalize_exploration(struct Sampler *e, struct Walker *w, double ln_prob) {
  int64_t i;
  w->initial_ln_prob = ln_prob;
  for (i=0; i<e->dims; i++) w->params[i] += w->x[0]*w->basis[i];
  for (i=0; i<w->num_queue; i++) w->queue[i].state = QUEUESAMPLE_COMPLETE;
  e->walkers_done++;
}

//Find a location x such that W(x) = (|x|^(D-1)*P(x)) is at least as large as
//some local maximum of W(x).  Exploration occurs along current direction using Newton's method.
//End when maximum iterations are reached or maximum is located within tolerance.
void sampler_explore(struct Sampler *e, struct Walker *w) {
  struct QueueSample *q = w->queue;
  double cur_net_prob = q->net_prob;
  double new_x = w->x[0];
  w->step++;
  sampler_generic_derivatives(e, w);

  int64_t valid = 1;
  //Check for validity
  for (int64_t i=0; i<3; i++)
    if (w->queue[i].ln_prob == SAMPLER_INVALID_LN_PROB) valid = 0;
  
  double old_x = w->x[0];
  if (!valid) {
    w->max_x = w->x[0];
    new_x = 0.5*(w->min_x + w->max_x);
  }
  else {  
    //If we're near a maximum, W''(x) should be negative
    if (w->derivatives[1]<0) {
      int64_t replace_min = 0, replace_max = 0, do_half = 0;
      //If W'(x) is positive, then we're below the maximum
      if (w->derivatives[0] > 0) {
	//We can increase min_x if W(x) > W(min_x) OR W(max_x) > W(min_x)
	if (w->min_x == 0 || (w->min_x_net_prob < cur_net_prob) ||
	    ((w->max_x > 0) && (w->min_x_net_prob < w->max_x_net_prob))) replace_min = 1;
	else replace_max = do_half = 1; //Otherwise, replace max
      }
      else if (w->derivatives[0] < 0) { //If W'(x) is negative, we're above the maximum
	//We can decrease max_x if W(x) > W(max_x) OR W(min_x) > W(max_x)
	if (w->max_x == 0 || (w->max_x_net_prob < cur_net_prob) ||
	    ((w->min_x > 0) && (w->max_x_net_prob < w->min_x_net_prob))) replace_max = 1;
	else replace_min = do_half = 1; //Otherwise, replace min
      }
      else if (w->derivatives[0] == 0) { //Shouldn't happen if W'(x) is continuous, but...
	if ((w->min_x == 0 || cur_net_prob > w->min_x_net_prob) &&
	    (w->max_x == 0 || cur_net_prob > w->max_x_net_prob)) { //Found maximum
	  sampler_finalize_exploration(e, w, q->ln_prob);  //Call this walker done.
	  return;
	}
	else if (w->min_x == 0) replace_min = do_half = 1;
	else if (w->max_x == 0) replace_max = do_half = 1;
	else if (w->min_x_net_prob < w->max_x_net_prob) replace_min = do_half = 1;
	else replace_max = do_half = 1;
      }
      
      assert(replace_max || replace_min); //Guarantees that interval must shrink
      
      if (replace_min) {
	w->min_x = w->x[0];
	w->min_x_net_prob = cur_net_prob;
      }
      else if (replace_max) {
	w->max_x = w->x[0];
	w->max_x_net_prob = cur_net_prob;
      }
      
      new_x = w->x[0] - (w->derivatives[0] / w->derivatives[1]);
      if (new_x < w->min_x) new_x = 0.5*(w->min_x+w->x[0]);
      else if (w->max_x && new_x > w->max_x) new_x = 0.5*(w->max_x+w->x[0]);
      if (do_half) new_x = 0.5*(w->max_x + w->min_x);
    }
    else { //Second derivative is zero/positive, so we're near an inflection point or minimum.
      //Replace max_x if either: a) it does not exist, or b) min_x has a better net prob
      if (w->max_x==0 || ((w->min_x > 0) && w->min_x_net_prob > w->max_x_net_prob)) {
	w->max_x = w->x[0];
	w->max_x_net_prob = cur_net_prob;
      }
      else { //max_x exists and has a better net prob than min_x
	w->min_x = w->x[0];
	w->min_x_net_prob = cur_net_prob;
      }
      assert(w->max_x > 0); //Max_x guaranteed to exist by above
      new_x = 0.5*(w->max_x+w->min_x);
    }
  }
  
  if ((fabs(new_x - w->x[0]) < e->exploration_dx) //Change is within tolerance
      || ((w->min_x==0 || w->max_x==0) && w->step>e->exploration_max_steps) //Still haven't found interval
      || (w->step > 10*e->exploration_max_steps)) { //or something majorly wrong is happening
    //Call this walker done
    if (!valid) w->x[0] = w->min_x;
    sampler_finalize_exploration(e, w, q->ln_prob);
    return;
  }
  
  w->x[0] = new_x;
  double res = sampler_find_valid_limit(e, w->params, w->basis, new_x+e->exploration_dx);
  if (res < new_x+e->exploration_dx) {
    w->x[0] = res - 1.1*e->exploration_dx;
    w->max_x = res;
    check_log(e->log_file, "#Limited walker %d to %f\n", (int)(w-e->w), res);
  }
  check_log(e->log_file, "#Walker %"PRId64": x=%g, lnP=%g, NP=%g; d0: %f d1: %f; min: %f; max: %f; new: %f; actual: %f\n", (int64_t)(w-e->w), old_x, w->queue[0].ln_prob, w->queue[0].net_prob, w->derivatives[0], w->derivatives[1], w->min_x, w->max_x, new_x, w->x[0]);

  sampler_exploration_queue_derivatives(e, w);
}


/****************************
Standard Deviation Estimation
*****************************/
void sampler_estimate_stddev_and_covariance(struct Sampler *e, double *avg, double *stddev_guess) {
  int64_t i;
  if (avg && avg != e->avg)  memcpy(e->avg, avg, sizeof(double)*e->dims);
  if (stddev_guess) memcpy(e->eigenvalues, stddev_guess, sizeof(double)*e->dims);
  else for (i=0; i<e->dims; i++) e->eigenvalues[i] = 1;
  e->state = SAMPLER_STATE_INITIAL_STDDEV;
  e->cov_element = -1;
  e->cov_elements_done = 0;
  sampler_increment_cov_element(e, 0);
  for (i=0; i<e->num_walkers; i++) {
    struct Walker *w = e->w+i;
    sampler_queue_covariance_derivatives(e, w, 0);
  }
}

void sampler_evaluate_stddev_results(struct Sampler *e, struct Walker *w) {
  int64_t i;
  for (i=1; i<3; i++) {
    int64_t hit_max = 0, hit_min = 0;
    if ((i==1 && (w->flags & WALKER_HIT_LEFT_MAX_FLAG)) ||
	(i==2 && (w->flags & WALKER_HIT_RIGHT_MAX_FLAG))) hit_max = 1;
    if ((i==1 && (w->flags & WALKER_HIT_LEFT_MIN_FLAG)) ||
	(i==2 && (w->flags & WALKER_HIT_RIGHT_MIN_FLAG))) hit_min = 1;
    //requeue with smaller step size
    if (w->queue[i].ln_prob == SAMPLER_INVALID_LN_PROB || (!hit_min && (w->queue[i].ln_prob < w->queue[0].ln_prob - 1.0))) {
      for (int64_t j=0; j<e->dims; j++) {
	w->queue[i].params[j] -= 0.5*(w->queue[i].params[j]-e->avg[j]);
      }
      w->queue[i].stepsize *= 0.5;
      w->queue[i].state = QUEUESAMPLE_NEW;
      if (i==1) w->flags |= WALKER_HIT_LEFT_MAX_FLAG;
      if (i==2) w->flags |= WALKER_HIT_RIGHT_MAX_FLAG;
      w->samples_present--;
      if ((fabs(w->queue[i].stepsize) < exp(-5)*fabs(w->queue[3-i].stepsize)) &&
	  w->queue[3-i].ln_prob != SAMPLER_INVALID_LN_PROB) {
	for (int64_t j=0; j<e->dims; j++) {
	  w->queue[i].params[j] = e->avg[j] + 0.5*(w->queue[3-i].params[j]-e->avg[j]);
	}
	w->queue[i].stepsize = 0.5*w->queue[3-i].stepsize;
      }
    }
    else if (!hit_max && (w->queue[i].ln_prob > w->queue[0].ln_prob - 0.25)) {
      for (int64_t j=0; j<e->dims; j++) {
	w->queue[i].params[j] += (w->queue[i].params[j]-e->avg[j]);
      }
      w->queue[i].stepsize *= 2.0;
      w->queue[i].state = QUEUESAMPLE_NEW;
      if (i==1) w->flags |= WALKER_HIT_LEFT_MIN_FLAG;
      if (i==2) w->flags |= WALKER_HIT_RIGHT_MIN_FLAG;
      double mul = sampler_find_valid_point(e, e->avg, w->queue[i].params);
      w->queue[i].stepsize *= mul;
      if (mul < 1) {
	if (i==1) w->flags |= WALKER_HIT_LEFT_MAX_FLAG;
	if (i==2) w->flags |= WALKER_HIT_RIGHT_MAX_FLAG;
      }
      w->samples_present--;
    }
  }
  
  if (w->samples_present<3) return;
  double mul = 0.5*(fabs(w->queue[1].stepsize) + fabs(w->queue[2].stepsize));
  e->eigenvalues[w->step] *= mul;
  //fprintf(stderr, "%"PRId64" %g (x%g)\n", w->step, e->eigenvalues[w->step], mul);
  

  if (w->step==0) {
    e->avg_ln_prob = w->queue->ln_prob;
    for (i=1; i<e->num_walkers; i++)
      if (e->w[i].queue->state == QUEUESAMPLE_PENDING)
	_sampler_assign_ln_prob(e, e->w[i].queue, e->avg_ln_prob);
  }
  sampler_queue_covariance_derivatives(e, w, 0);

  e->cov_elements_done++;
  int64_t nonstatic_dims = e->dims - e->static_dims;
  if (e->cov_elements_done == nonstatic_dims) {
    //exit(EXIT_SUCCESS);
    sampler_estimate_covariance_matrix(e, e->avg, e->eigenvalues);
  }
}


/******************************************
Covariance Matrix Initialization/Estimation
*******************************************/
void sampler_output_covariance_samples(struct Sampler *e, int64_t num_samples, char *filename) {
  FILE *output = check_fopen(filename, "w");
  double *v = sampler_get_stack(e);
  double *bv = sampler_get_stack(e);
  for (int64_t i=0; i<num_samples; i++) {
    for (int64_t j=0; j<e->dims; j++) bv[j] = normal_random(0, 1);
    sampler_translate_basis(e, e->w, bv, v, 1);
    for (int64_t j=0; j<e->dims; j++)
      fprintf(output, "%g ", v[j]);
    fprintf(output, "\n");
  }
  sampler_release_stack(e, &bv);
  sampler_release_stack(e, &v);
  fclose(output);
}


void sampler_set_covariance_matrix(struct Sampler *e, double *avg, double *cov_matrix) {
  if (avg && avg != e->avg)  memcpy(e->avg, avg, sizeof(double)*e->dims);
  if (e->use_covariance_matrix) {
    if (cov_matrix && (cov_matrix != e->cov_matrix))
      memcpy(e->cov_matrix, cov_matrix, sizeof(double)*e->dims*e->dims);
  } else {
    if (cov_matrix && (cov_matrix != e->variance))
      memcpy(e->variance, cov_matrix, sizeof(double)*e->dims);
  }
  int64_t min_cov_steps = e->min_covariance_steps;
  e->min_covariance_steps = 0;
  e->avg_steps = e->accepted = 1;
  sampler_stats_to_step(e);
  e->min_covariance_steps = min_cov_steps;
  e->avg_steps = e->last_adaptation = 0;
  e->accepted = e->rejected = 0;
}

void sampler_estimate_covariance_matrix(struct Sampler *e, double *avg, double *stddev) {
  int64_t i;
  memcpy(e->avg, avg, sizeof(double)*e->dims);
  if (stddev==NULL) for (i=0; i<e->dims; i++) e->eigenvalues[i] = 1;
  else if (stddev != e->eigenvalues) memcpy(e->eigenvalues, stddev, sizeof(double)*e->dims);
  e->state = SAMPLER_STATE_INITIAL_COV_MATRIX;
  e->cov_element = -1;
  e->cov_elements_done = 0;
  sampler_increment_cov_element(e, e->use_covariance_matrix);
  for (i=0; i<e->num_walkers; i++)
    sampler_queue_covariance_derivatives(e, e->w+i, e->use_covariance_matrix);
}

void sampler_increment_cov_element(struct Sampler *e, int64_t use_covariance_matrix) {
  e->cov_element++;
  if (use_covariance_matrix) {
    int64_t dim1 = e->cov_element / e->dims;
    int64_t dim2 = e->cov_element - dim1*e->dims;
    while (dim1 < e->dims && SAMPLER_STATIC_DIM(e, dim1)) dim1++;
    while (dim2 < e->dims && (dim2 < dim1 || SAMPLER_STATIC_DIM(e,dim2))) dim2++;
    e->cov_element = dim1*e->dims + dim2;
    if (dim2 == e->dims && dim1<e->dims) { e->cov_element--; return sampler_increment_cov_element(e, use_covariance_matrix); }
  } else {
    while (e->cov_element < e->dims && SAMPLER_STATIC_DIM(e, e->cov_element))
      e->cov_element++;
  }
}

void sampler_queue_covariance_derivatives(struct Sampler *e, struct Walker *w, int64_t use_covariance_matrix) {
  int64_t j;
  double steps[3] = {0, -1, 1};
  int64_t dim1 = e->cov_element/e->dims;
  int64_t dim2 = e->cov_element - dim1*e->dims;
  w->flags = 0;
  if (!use_covariance_matrix) dim1 = dim2 = e->cov_element;
  if (dim1>=e->dims) return;
  
  w->samples_present = 0;
  w->step = e->cov_element;
  
  //current/backward/forward difference
  for (j=0; j<3; j++) {
    struct QueueSample *q = sampler_reset_queue_sample(e, w, j);
    q->stepsize = steps[j];
    q->ln_volume = 0;
    memcpy(q->params, e->avg, sizeof(double)*e->dims);
    q->params[dim1] += steps[j]*e->eigenvalues[dim1];
    if (dim1 != dim2) {
      q->params[dim2] += steps[j]*e->eigenvalues[dim2];
      q->stepsize *= sqrt(2);
    }
    double mul = sampler_find_valid_point(e, e->avg, q->params);
    q->stepsize *= mul;
    if (mul < 1) {
      if (j==1) w->flags |= WALKER_HIT_LEFT_MAX_FLAG;
      if (j==2) w->flags |= WALKER_HIT_RIGHT_MAX_FLAG;
    }
    if (j==0 && e->cov_element != 0) {
      if (e->avg_ln_prob == SAMPLER_INVALID_LN_PROB)
	q->state = QUEUESAMPLE_PENDING;
      else {
	q->state = QUEUESAMPLE_COMPLETE;
	q->ln_prob = e->avg_ln_prob;
	w->samples_present = 1;
      }
    }
  }
  //If maximum likelihood point is right at boundary, then will need to
  // sample 2 points away from boundary.
  double step_difference = log(fabs(w->queue[1].stepsize))-log(fabs(w->queue[2].stepsize));
  if (fabs(step_difference)>5) {
    int64_t max_q = (step_difference > 0) ? 1 : 2;
    int64_t min_q = 3 - max_q;
    w->queue[min_q].stepsize = w->queue[max_q].stepsize / 2.0;
    for (j=0; j<e->dims; j++) {
      w->queue[min_q].params[j] =
	e->avg[j] + 0.5*(w->queue[max_q].params[j]-e->avg[j]);
    }
  }
  sampler_increment_cov_element(e, use_covariance_matrix);
}

void sampler_estimate_bounded_eigenvalues(struct Sampler *e) {
  int64_t i,j;
  struct Walker *w = e->w;
  memset(w->basis, 0, sizeof(double)*e->dims);
  double min, max;
  for (i=0; i<e->dims; i++) {
    w->basis[i] = 1;
    sampler_translate_basis(e, w, w->basis, w->params, 0);
    max = sampler_find_valid_limit(e, w->origin, w->params, 1.0);
    for (j=0; j<e->dims; j++) w->params[j] = -w->params[j];
    min = sampler_find_valid_limit(e, w->origin, w->params, 1.0);
    w->basis[i] = 0;
    //fprintf(stderr, "Max/min: %g %g\n", max, min);
    if (max < min) max = min;
    if (max < 1.0) e->eigenvalues[i] *= max;
  }
}


void sampler_finalize_covariance_matrix(struct Sampler *e) {
  int64_t i, j;  
  if (e->use_covariance_matrix) {
    for (i=0; i<e->dims; i++) {
      if (SAMPLER_STATIC_DIM(e,i)) continue;
      for (j=0; j<i; j++) {
	if (SAMPLER_STATIC_DIM(e,j)) continue;
	e->cov_matrix[j*e->dims+i] -= 0.5*(e->cov_matrix[i*e->dims+i]+e->cov_matrix[j*e->dims+j]);
      }
    }
    for (i=0; i<e->dims; i++) {
      if (SAMPLER_STATIC_DIM(e,i) || !e->eigenvalues[i]) continue;
      for (j=0; j<e->dims; j++) {
	if (SAMPLER_STATIC_DIM(e,j) || !e->eigenvalues[j]) continue;
	fprintf(stderr, "%.7f ", e->cov_matrix[i*e->dims+j]);
      }
      fprintf(stderr, "\n");
    }

    for (i=0; i<e->dims; i++) {
      if (SAMPLER_STATIC_DIM(e,i) || !e->eigenvalues[i]) continue;
      for (j=0; j<=i; j++) {
	if (SAMPLER_STATIC_DIM(e,j) || !e->eigenvalues[j]) continue;
	e->cov_matrix[j*e->dims+i] /= (e->eigenvalues[i]*e->eigenvalues[j]);
	e->cov_matrix[i*e->dims+j] = e->cov_matrix[j*e->dims+i];
      }
    }
    for (i=0; i<e->dims; i++) {
      if (SAMPLER_STATIC_DIM(e,i) || !e->eigenvalues[i]) continue;
      for (j=0; j<e->dims; j++) {
	if (SAMPLER_STATIC_DIM(e,j) || !e->eigenvalues[j]) continue;
	fprintf(stderr, "%.1f ", e->cov_matrix[i*e->dims+j]);
      }
      fprintf(stderr, "\n");
    }
    sampler_set_covariance_matrix(e, NULL, NULL);
  } else {
    for (i=0; i<e->dims; i++) {
      if (SAMPLER_STATIC_DIM(e,i)) continue;
      if (e->eigenvalues[i]) e->variance[i] /= (e->eigenvalues[i]*e->eigenvalues[i]);
    }
    sampler_set_covariance_matrix(e, NULL, NULL);
  }

  //We actually calculated the precision matrix above, so need to invert resulting eigenvalues
  for (i=0; i<e->dims; i++) {
    if (e->eigenvalues[i] != 0) e->eigenvalues[i] = fabs(1.0/e->eigenvalues[i]);
    else e->eigenvalues[i] = 0;
  }
  if (e->use_covariance_matrix) {
    e->min_covariance_steps = 2*e->dims*e->dims;
    if (e->min_covariance_steps < SAMPLER_MIN_COVARIANCE_STEPS)
      e->min_covariance_steps = SAMPLER_MIN_COVARIANCE_STEPS;
    memset(e->cov_matrix, 0, sizeof(double)*e->dims*e->dims);
  } else {
    memset(e->variance, 0, sizeof(double)*e->dims);
  }
  e->avg_steps = 0;
  sampler_set_origin_as_average(e);
  //if (e->valid) sampler_estimate_bounded_covariance(e);
  for (j=0; j<e->dims; j++)
    check_log(e->log_file, "#Avg, Eig %"PRId64" %g, %e\n", j, e->origins[j], e->eigenvalues[j]);
  if (e->valid) sampler_estimate_bounded_eigenvalues(e);
  for (j=0; j<e->dims; j++)
    check_log(e->log_file, "#Avg, Eig %"PRId64" %g, %e\n", j, e->origins[j], e->eigenvalues[j]);

  sampler_initial_exploration(e, e->avg, e->eigenvalues);
}

void sampler_evaluate_covariance_derivatives(struct Sampler *e, struct Walker *w) {
  int64_t i;
  for (i=1; i<3; i++) {
    //requeue with smaller step size
    if (w->queue[i].ln_prob == SAMPLER_INVALID_LN_PROB) {
      for (int64_t j=0; j<e->dims; j++) {
	w->queue[i].params[j] -= 0.5*(w->queue[i].params[j]-e->avg[j]);
      }
      w->queue[i].stepsize *= 0.5;
      w->queue[i].state = QUEUESAMPLE_NEW;
      w->samples_present--;
      if ((fabs(w->queue[i].stepsize) < exp(-5)*fabs(w->queue[3-i].stepsize)) &&
	  w->queue[3-i].ln_prob != SAMPLER_INVALID_LN_PROB) {
	for (int64_t j=0; j<e->dims; j++) {
	  w->queue[i].params[j] = e->avg[j] + 0.5*(w->queue[3-i].params[j]-e->avg[j]);
	}
	w->queue[i].stepsize = 0.5*w->queue[3-i].stepsize;
      }
    }
  }
  if (w->samples_present<3) return;


  double dxf = (w->queue[2].ln_prob - w->queue[0].ln_prob)/w->queue[2].stepsize; //Forward derivative
  double dxb = (w->queue[1].ln_prob - w->queue[0].ln_prob)/w->queue[1].stepsize; //Backwards derivative
  double d2 = -1.0*((dxf - dxb)/(0.5*(w->queue[2].stepsize - w->queue[1].stepsize))); //Double derivative of -ln(P)

  if (d2<0) {
    check_log(e->log_file, "[Sampler:Warning] Positive second derivative for covariance matrix; should not happen if supplied point is at a maximum.\n");
    check_log(e->log_file, "Supplied position: ");
    for (i=0; i<e->dims; i++) check_log(e->log_file, "%.16e ", e->avg[i]);
    check_log(e->log_file, "(Ln(P)=%g)\n", w->queue[0].ln_prob);
    int64_t max_q = (w->queue[2].ln_prob > w->queue[1].ln_prob) ? 2 : 1;
    if (w->queue[max_q].ln_prob > w->queue[0].ln_prob) {
      check_log(e->log_file, "Improved position: ");
      for (i=0; i<e->dims; i++) check_log(e->log_file, "%.16e ", w->queue[max_q].params[i]);
      check_log(e->log_file, "(Ln(P)=%g; diff: %g)\n", w->queue[max_q].ln_prob,  w->queue[max_q].ln_prob-w->queue[0].ln_prob);
    }
    check_log(e->log_file, "Step sizes: %g, %g\n", w->queue[1].stepsize, w->queue[2].stepsize);
    check_log(e->log_file, "LnP: %g, %g, %g\n", w->queue[0].ln_prob, w->queue[1].ln_prob, w->queue[2].ln_prob);
    check_log(e->log_file, "dxf: %g; dxb: %g; d2: %g\n", dxf, dxb, -d2);
    //Could also increase step size automatically
    //check_log(e->log_file, "[Error] Probability surface has positive second derivative; covariance matrix near probability minimum is undefined.  Refit to find actual minimum or increase initial step size for dimension %"PRId64" and/or %"PRId64" (%g)\n", w->step/e->dims, w->step%e->dims);
    //d2 = 0;
    exit(EXIT_FAILURE);
  }

  if (e->use_covariance_matrix) {
    int64_t dim1 = w->step / e->dims;
    int64_t dim2 = w->step % e->dims;
    fprintf(stderr, "(%"PRId64",%"PRId64"): Fwd: %g; Bck: %g; d2: %g\n", dim1, dim2, dxf, dxb, d2);
    e->cov_matrix[w->step] = d2;
  }
  else e->variance[w->step] = d2;
  
  if (w->step==0) {
    e->avg_ln_prob = w->queue->ln_prob;
    for (i=1; i<e->num_walkers; i++)
      if (e->w[i].queue->state == QUEUESAMPLE_PENDING)
	_sampler_assign_ln_prob(e, e->w[i].queue, e->avg_ln_prob);
  }
  sampler_queue_covariance_derivatives(e, w, e->use_covariance_matrix);

  e->cov_elements_done++;
  int64_t nonstatic_dims = e->dims - e->static_dims;
  int64_t max_elements_done = nonstatic_dims*(nonstatic_dims+1)/2;
  if (!e->use_covariance_matrix) max_elements_done = nonstatic_dims;
  if (e->cov_elements_done == max_elements_done) sampler_finalize_covariance_matrix(e);
}

void sampler_guess_valid_point(struct Sampler *e, struct Walker *w, const double *in_params) {
  int64_t i, j;
  do {
    memcpy(w->basis, in_params, sizeof(double)*e->dims);
    for (i=0; i<e->resampling_dims; i++) {
      j = r250()%e->dims;
      w->basis[j] = normal_random(0, 1);
    }
    sampler_translate_basis(e, w, w->basis, w->params, 1);
  } while (e->valid && !(e->valid(w->params)));
}
  

void sampler_estimate_bounded_covariance(struct Sampler *e) {
  if (!e->valid) return;
  int64_t j,k;
  int64_t max_steps = 100.0*e->dims*e->dims;
  for (j=0; j<e->num_walkers; j++)
    memset(e->w[j].basis, 0, sizeof(double)*e->dims);

  double *params = sampler_get_stack(e);
  for (k=0; k<max_steps; k++) {
    j = k%e->num_walkers;
    memcpy(params, e->w[j].basis, sizeof(double)*e->dims);
    sampler_guess_valid_point(e, e->w+j, params);
    if (j==(e->num_walkers-1))
      sampler_update_averages(e);
    if (!(k%1000)) check_log(e->log_file, "#Processed %"PRId64" samples.\n", k);
  }
  sampler_release_stack(e, &params);
  
  double mul = 1.0/(double)max_steps;
  if (e->use_covariance_matrix) {
    for (j=0; j<e->dims; j++)
      for (k=0; k<e->dims; k++)
	e->cov_matrix[j*e->dims+k] *= mul;
  } else {
      for (k=0; k<e->dims; k++)
	e->variance[k] *= mul;    
  }
  sampler_set_covariance_matrix(e, NULL, NULL);
  if (e->use_covariance_matrix) memset(e->cov_matrix, 0, sizeof(double)*e->dims*e->dims);
  else memset(e->variance, 0, sizeof(double)*e->dims);
}


/*************************************
Manage statistics about walkers
**************************************/

//Reset averages (e.g., after a burn-in run).
void sampler_clear_averages(struct Sampler *e) {
  memset(e->avg, 0, sizeof(double)*e->dims);
  if (e->use_covariance_matrix) {
    memset(e->cov_matrix, 0, sizeof(double)*e->dims*e->dims);
    memset(e->orth_matrix, 0, sizeof(double)*e->dims*e->dims);
  } else {
    memset(e->variance, 0, sizeof(double)*e->dims);
  }
  memset(e->eigenvalues, 0, sizeof(double)*e->dims);
  e->avg_steps = 0;
}

//Add accepted steps from current walkers to the covariance/average statistics
void sampler_update_averages(struct Sampler *e) {
  int64_t i, j, k;

  memset(e->new_avg, 0, sizeof(double)*e->dims);
  for (i=0; i<e->num_walkers; i++)
    for (j=0; j<e->dims; j++)
      e->new_avg[j] += e->w[i].params[j];

  for (j=0; j<e->dims; j++) e->new_avg[j] /= (double)e->num_walkers;

  //Online covariance algorithm from https://doi.org/10.1109/CLUSTR.2009.5289161
  //Note that this stores the *sum* of the (co)variances, and so should be divided by e->avg_steps
  //to get the *average* (co)variance.
  double ratio = (double)(e->avg_steps * e->num_walkers) / (double)(e->avg_steps + e->num_walkers);
  if (e->use_covariance_matrix) {
    for (j=0; j<e->dims; j++) {
      for (k=j; k<e->dims; k++) {
	double new_cov = 0;
	for (i=0; i<e->num_walkers; i++)
	  new_cov += (e->w[i].params[j]-e->new_avg[j])*(e->w[i].params[k]-e->new_avg[k]);
	e->cov_matrix[j*e->dims + k] += new_cov + ratio*(e->new_avg[j]-e->avg[j])*(e->new_avg[k]-e->avg[k]);
	e->cov_matrix[k*e->dims + j] = e->cov_matrix[j*e->dims + k];
      }
    }
  } else {
    for (j=0; j<e->dims; j++) {
	double new_cov = 0;
	for (i=0; i<e->num_walkers; i++)
	  new_cov += (e->w[i].params[j]-e->new_avg[j])*(e->w[i].params[j]-e->new_avg[j]);
	e->variance[j] += new_cov + ratio*(e->new_avg[j]-e->avg[j])*(e->new_avg[j]-e->avg[j]);
    }
  }

  //Update average; must be done *after* (co)variance calculation
  ratio = (double)(e->num_walkers) / (double)(e->avg_steps + e->num_walkers);
  for (j=0; j<e->dims; j++)
    e->avg[j] += ratio*(e->new_avg[j]-e->avg[j]);
  
  e->avg_steps += e->num_walkers;
}

//Calculate averages from current walkers only
void sampler_reset_averages(struct Sampler *e) {
  sampler_clear_averages(e);
  sampler_update_averages(e);
}

//Decompose covariance matrix into orthonormal basis + eigenvalues
void sampler_stats_to_step(struct Sampler *e) {
  double mul = 1.0/(double)e->avg_steps;
  int64_t i, rescale_eigs = 1;

  for (i=0; i<e->dims; i++)
    fprintf(stderr, "Pre Eig %"PRId64": %g!\n", i, e->eigenvalues[i]);
  sampler_prep_covariance_matrix(e, e->avg_steps);
  if (e->use_covariance_matrix) {
    if (e->accepted > e->min_covariance_steps) {
      jacobi_decompose_dim(e->cov_matrix, e->eigenvalues, e->orth_matrix, e->dims-e->static_dims);
    } else if (e->min_covariance_steps == SAMPLER_MIN_COVARIANCE_STEPS) {
      for (i=0; i<e->dims; i++) e->eigenvalues[i] = e->cov_matrix[i*e->dims+i];
    } else {
      rescale_eigs = 0;
    }
    sampler_remap_orthogonal_bases(e);
    for (i=0; i<e->dims; i++) {
      fprintf(stderr, "Eig %"PRId64": %g!\n", i, rescale_eigs ? copysign(sqrt(fabs(e->eigenvalues[i]*mul)),e->eigenvalues[i]) : e->eigenvalues[i]);
      //if (e->eigenvalues[i] <=0) e->eigenvalues[i] = 0;
      if (rescale_eigs) e->eigenvalues[i] = sqrt(fabs(e->eigenvalues[i])*mul);
    }
  } else {
    for (i=0; i<e->dims; i++) {
      if (e->variance[i] <=0 || SAMPLER_STATIC_DIM(e,i)) e->variance[i] = 0;
      e->eigenvalues[i] = sqrt(e->variance[i]*mul);
    }
  }
  e->last_adaptation = e->avg_steps;
}

//Shifts covariance matrix to remove static dimensions
void sampler_prep_covariance_matrix(struct Sampler *e, int64_t avg_steps) {
  if (!e->static_dims || !e->use_covariance_matrix) return;
  int64_t elem = 0;
  int64_t eig_elem = 0;
  for (int64_t i=0; i<e->dims; i++) {
    if (SAMPLER_STATIC_DIM(e, i)) continue;
    e->eigenvalues[eig_elem] = e->eigenvalues[i];
    eig_elem++;
    for (int64_t j=0; j<e->dims; j++) {
      if (SAMPLER_STATIC_DIM(e, j)) continue;
      e->cov_matrix[elem] = e->cov_matrix[i*e->dims + j];
      e->orth_matrix[elem] = e->orth_matrix[i*e->dims + j]; //In case jacobi_decompose_dim is not called
      elem++;
    }
  }
}

//Remaps eigenvectors to appropriate static dimensions
void sampler_remap_orthogonal_bases(struct Sampler *e) {
  if (!e->use_covariance_matrix || !e->static_dims) return;
  int64_t nonstatic_dims = e->dims - e->static_dims;
  int64_t elem = nonstatic_dims*nonstatic_dims-1;
  int64_t eig_elem = nonstatic_dims-1;
  for (int64_t i=e->dims-1; i>=0; i--) {
    if (SAMPLER_STATIC_DIM(e, i)) continue;
    e->eigenvalues[i] = e->eigenvalues[eig_elem];
    eig_elem--;
    for (int64_t j=e->dims-1; j>=0; j--) {
      if (SAMPLER_STATIC_DIM(e, j)) continue;
      e->cov_matrix[i*e->dims + j] = e->cov_matrix[elem];
      e->orth_matrix[i*e->dims + j] = e->orth_matrix[elem];
      elem--;
    }
  }

  //Zero out static dimensions
  for (int64_t i=0; i<e->dims; i++) {
    if (SAMPLER_STATIC_DIM(e,i)) e->eigenvalues[i]=0;
    for (int64_t j=0; j<=i; j++) {
      if (SAMPLER_STATIC_DIM(e,i) || SAMPLER_STATIC_DIM(e,j)) {
	e->cov_matrix[i*e->dims+j] = e->cov_matrix[j*e->dims+i] =
	  e->orth_matrix[i*e->dims+j] = e->orth_matrix[j*e->dims+i] = 0;	  
      }
    }
  }
}

/*************************************
Basis transformations
**************************************/

void sampler_translate_origin(struct Walker *w, double *params) {
  for (int64_t i=0; i<w->dims; i++) {
    params[i] += w->origin[i];
    if (SAMPLER_STATIC_DIM(w,i)) params[i] = w->origin[i];
  }
}

//Convert from orthonormal basis representation (in_params) to user's parameterization (out_params)
void sampler_translate_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params, int64_t translate_origin) {
  int64_t i, j;
  if (e->use_covariance_matrix) {
    memset(out_params, 0, sizeof(double)*e->dims);
    for (j=0; j<e->dims; j++) {
      double dx = e->eigenvalues[j]*in_params[j];
      for (i=0; i<e->dims; i++)
	out_params[i] += e->orth_matrix[i+e->dims*j]*dx;
    }
  }
  else {
    for (i=0; i<e->dims; i++)
	out_params[i] = in_params[i]*e->eigenvalues[i];
  }
  if (translate_origin) sampler_translate_origin(w, out_params);
  else {
    for (i=0; i<w->dims; i++)
      if (SAMPLER_STATIC_DIM(w,i)) out_params[i] = 0;
  }
}

//Convert from user's parameterization (in_params) to orthonormal basis representation (out_params)
void sampler_detranslate_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params) {
  int64_t i, j;
  if (e->use_covariance_matrix) {
    memset(out_params, 0, sizeof(double)*e->dims);
    for (j=0; j<e->dims; j++) {
      double dx = in_params[j]-w->origin[j];
      for (i=0; i<e->dims; i++)
	out_params[i] += e->orth_matrix[j+e->dims*i]*dx;
    }
  }
  else {
    for (i=0; i<e->dims; i++) out_params[i] = in_params[i] - w->origin[i];
  }
  for (i=0; i<e->dims; i++) if (e->eigenvalues[i]>0) out_params[i] /= e->eigenvalues[i];
}


/***************
Queue Management
****************/

//Reset a queue sample and return it; currently, only evaluation-type samples are supported
struct QueueSample *sampler_reset_queue_sample(struct Sampler *e, struct Walker *w, int64_t id) {
  struct QueueSample *q = w->queue + id;
  assert((q-e->queue) < e->queue_allocated);
  assert(id <= w->num_queue);
  q->state = QUEUESAMPLE_NEW;
  q->dims = w->dims;
  q->walker = w - e->w;
  q->type = EVALUATION_TYPE;
  q->id = id;
  q->net_prob = q->ln_prob = SAMPLER_INVALID_LN_PROB;
  q->ln_volume = q->stepsize = 0;
  return q;
}

//Find an unevaluated parameter set in the queue and return it.
//Returns NULL if there are no unevaluated samples.
struct QueueSample *sampler_get_sample(struct Sampler *e) {
  struct QueueSample *q = NULL;
#if defined(_OPENMP)
#pragma omp critical
{
#endif
  int64_t i, queue_max = e->cur_queue+e->queue_allocated;
  sampler_update_state(e);
  for (; e->cur_queue < queue_max; e->cur_queue++) {
    i = e->cur_queue % e->queue_allocated;
    if (e->queue[i].state==QUEUESAMPLE_NEW) {
      e->queue[i].state = QUEUESAMPLE_PENDING;
      e->cur_queue++;
      e->total_likelihoods++;
      q = e->queue + i;
      break;
    }
  }
#if defined(_OPENMP)
} //End critical section
#endif
  return q;
}

//Let the sampler know the probability of acceptance
//To specify an invalid LN_PROB, use SAMPLER_INVALID_LN_PROB
void _sampler_assign_ln_prob(struct Sampler *e, struct QueueSample *q, double ln_prob) {
  //Validate user inputs
  assert(e->queue <= q);
  assert((q-e->queue) < e->queue_allocated);
  assert((q->walker>=0) && (q->walker < e->num_walkers));
  if (q->state != QUEUESAMPLE_PENDING) {
    check_log(e->error_file, "[Sampler:Error] Duplicate or unmarked queue sample returned!\n");
    exit(EXIT_FAILURE);
  }
  if (!(ln_prob < SAMPLER_INVALID_LN_PROB))
    ln_prob = SAMPLER_INVALID_LN_PROB;
  
  //Set queue sample as complete
  q->state = QUEUESAMPLE_COMPLETE;
  q->ln_prob = ln_prob;
  q->net_prob = q->ln_prob + q->ln_volume;
  
  //Perform state-dependent action if all samples for walker are complete.
  struct Walker *w = e->w + q->walker;
  w->samples_present++;
  if (e->state==SAMPLER_STATE_INITIAL_EVALUATION) {
    w->initial_ln_prob = w->final_ln_prob = q->ln_prob;
    memcpy(w->params, w->queue->params, sizeof(double)*e->dims);
    e->walkers_done++;
    sampler_update_state(e);
  }
  else if (e->state==SAMPLER_STATE_INITIAL_STDDEV) {
    if (e->origin_type == SAMPLER_ORIGIN_BESTFIT)
      sampler_check_bestfit_origin(e, q->params, q->ln_prob);
    if (w->samples_present == 3)
      sampler_evaluate_stddev_results(e, w);    
  }
  else if (e->state==SAMPLER_STATE_INITIAL_COV_MATRIX) {
    if (e->origin_type == SAMPLER_ORIGIN_BESTFIT)
      sampler_check_bestfit_origin(e, q->params, q->ln_prob);
    if (w->samples_present == 3)
      sampler_evaluate_covariance_derivatives(e, w);
  }
  else if (e->state==SAMPLER_STATE_INITIAL_EXPLORATION) {
    if (w->samples_present == 3) {
      sampler_explore(e, w);
    }
  }
  else if (e->state==SAMPLER_STATE_FULL_EXPLORATION) {
    if (w->samples_present == w->queue_expected) {
      if (w->mode == ALEXANDER_SAMPLER) {
	sampler_derivatives(e, w);
	sampler_leapfrog(e, w);
	if (w->step > w->max_steps) sampler_decide_acceptance(e, w);
      }
      else if (w->mode == ANGULAR_METROPOLIS_SAMPLER || w->mode == STANDARD_METROPOLIS_SAMPLER) {
	w->final_ln_prob = q->ln_prob;
	w->final_net_prob = q->net_prob;
	sampler_decide_acceptance(e, w);
      }
      else if (w->mode == SEARCHLIGHT_SAMPLER) {
	sampler_searchlight(e, w);
      }
    }
  }
}

//Wraps _sampler_assign_ln_prob in openmp critical region
void sampler_assign_ln_prob(struct Sampler *e, struct QueueSample *q, double ln_prob) {
#if defined(_OPENMP)
#pragma omp critical
{
#endif
  _sampler_assign_ln_prob(e,q,ln_prob);
#if defined(_OPENMP)
} //End critical section
#endif
}

//Let the sampler know chi^2 for the given sample
//negative chi^2 is interpreted as an invalid values.
void sampler_assign_chi2(struct Sampler *e, struct QueueSample *q, double chi2) {
  double ln_prob = (chi2 >= 0) ? (-0.5*chi2) : SAMPLER_INVALID_LN_PROB;
  sampler_assign_ln_prob(e, q, ln_prob);
}

//Calculate the natural log of the volume compression factor: ln(r^{D-N})
//See Behroozi+ 2019 for explanation.
double sampler_calc_ln_volume(struct Walker *w, double *x, double basis_dims) {
  int64_t i;
  double ds = 0;
  for (i=0; i<basis_dims; i++) ds += x[i]*x[i]; //ds = |x|^2
  double dim_factor = w->dims - basis_dims - w->static_dims;
  return dim_factor*log(ds)/2.0; //Divide by 2.0 to take square root.
}

double sampler_calc_ke(struct Sampler *e, struct Walker *w) {
   int64_t i;
   double ke = 0;
   for (i=0; i<w->basis_dims; i++)
     ke += w->v[i]*w->v[i];
   ke *= e->velocity_scaling*e->velocity_scaling / 2.0;
   return ke;
}

//Calculate final KE of sample, use it to compute final net ln probability.
void sampler_calc_net_prob(struct Sampler *e, struct Walker *w) {
  if (w->mode == ALEXANDER_SAMPLER) {
    w->final_ke = sampler_calc_ke(e, w);
    w->final_net_prob = w->final_ln_prob + w->final_ln_volume - w->final_ke;
    if (e->alexander_variant) w->final_net_prob = w->final_ln_prob - w->final_ke;
  }
  else if (w->mode == SEARCHLIGHT_SAMPLER) {
    w->final_net_prob = w->final_ln_prob + w->final_ln_volume - w->final_ke;
  }
  /*Note: no need to calculate anything for ANGULAR_METROPOLIS_SAMPLER or STANDARD_METROPOLIS_SAMPLER*/

  if (w->final_ln_prob == SAMPLER_INVALID_LN_PROB)
    w->final_net_prob = SAMPLER_INVALID_LN_PROB;
}



/*******************************************
Functions for Alexander Hamiltonian Sampler
********************************************/

/* Set up bases that are orthogonal in the orthonormal space defined by
   the covariance matrix.  Translate these back into the user basis for
   efficiency. */
void sampler_init_alexander_step(struct Sampler *e, struct Walker *w) {
  int64_t i, j, k;
  double norm = 0;

  w->mode = ALEXANDER_SAMPLER;
  
  //Find norm of current position in orthonormal basis;
  //set basis 0 to point to current position.
  int64_t origin = dr250()*e->num_origins;
  w->origin = e->origins + origin*e->dims;
  memset(w->x, 0, sizeof(double)*w->basis_dims);
  sampler_detranslate_basis(e, w, w->params, w->basis);
  norm = sampler_dot_product(w->dims, w->basis, w->basis);
  w->x[0] = sqrt(norm);
  w->initial_norm = w->x[0];
  if (e->alexander_variant) e->radial_scaling = 1.0/sqrt(e->dims);
  else e->radial_scaling = 1.0;
  w->dim_correction = 1.0/e->radial_scaling - 1.0;
  sampler_unit_vectorize(w->dims, w->basis);
  
  //Generate N-1 orthonormal bases in the covariance matrix space
  for (i=1; i<w->basis_dims; i++) {
    double *v = w->basis + e->dims*i;
    sampler_random_unit_walker(w, v);
    for (j=0; j<i; j++) {
      double *v2 = w->basis + e->dims*j;
      double dot = sampler_dot_product(w->dims, v2, v);
      for (k=0; k<e->dims; k++) v[k]-= dot*v2[k];
      sampler_unit_vectorize(w->dims, v);
    }
  }

  //Translate all bases back into the user basis, without translating origin
  double *qp = sampler_get_stack(e);
  for (i=0; i<w->basis_dims; i++) {
    memcpy(qp, w->basis+i*e->dims, sizeof(double)*e->dims);
    sampler_translate_basis(e, w, qp, w->basis+i*e->dims, 0);
  }
  sampler_release_stack(e, &qp);
  
  //Draw kinetic energy from a Gaussian distribution with width
  //set by velocity scaling.
  for (i=0; i<w->basis_dims; i++)
    w->v[i] = normal_random(0, 1.0/e->velocity_scaling);
  
  //Test:
  //w->v[1] = fabs(w->v[1]);
  //w->stopping_criterion = normal_random(0, 1);

  w->initial_ke = sampler_calc_ke(e, w);
  w->initial_ln_volume = sampler_calc_ln_volume(w, w->x, w->basis_dims);
  w->initial_net_prob = w->initial_ln_prob + w->initial_ln_volume - w->initial_ke;
  
  //Reset step count, randomize total time elapsed by varying dt
  if (w->success_streak==0 && w->longest_streak < 2) w->dt_factor *= 0.5;
  else w->dt_factor = 1.0;
  w->step = 0;
  w->max_steps = (e->tmax_scaling*w->t_max / e->dt)+1;
  w->dt = e->dt * pow(10, normal_random(0, e->tmax_scatter))*w->dt_factor;

  //Queue derivatives, copy ln_prob from initial state
  sampler_queue_derivatives(e, w);
  w->queue[0].state = QUEUESAMPLE_PENDING;
  _sampler_assign_ln_prob(e, w->queue, w->initial_ln_prob);
}

void sampler_translate_position(struct Sampler *e, struct Walker *w, double *params) {
  double r = 0;
  if (e->alexander_variant) {
    for (int64_t j=0; j<w->basis_dims; j++) r += w->x[j]*w->x[j];
    r = sqrt(r);
    r = w->initial_norm*pow(r/w->initial_norm, e->radial_scaling)/r;
  }
  
  memset(params, 0, sizeof(double)*e->dims);
  for (int64_t j=0; j<w->basis_dims; j++)
    for (int64_t i=0; i<e->dims; i++)
      params[i] += w->x[j]*w->basis[j*e->dims + i];
  if (e->alexander_variant) for (int64_t i=0; i<e->dims; i++) params[i] *= r;
  sampler_translate_origin(w, params);  
}


//Currently, find probability at x and x+basis*dv for all vectors in basis
//Assumes that walker current position passes validity test
void sampler_queue_derivatives(struct Sampler *e, struct Walker *w) {
  int64_t i, j;
  w->samples_present = 0;
  w->queue_expected = w->basis_dims+1;

  //Translate current position to user basis; add to queue
  struct QueueSample *q = sampler_reset_queue_sample(e,w,0);
  sampler_translate_position(e, w, q->params);
  q->ln_volume = sampler_calc_ln_volume(w, w->x, w->basis_dims);

  //Calculate current position + basis*dv; translate; add to queue
  for (j=0; j<w->basis_dims; j++) {
    q = sampler_reset_queue_sample(e,w,j+1);
    q->stepsize = e->dv;
    while (1) {
      memcpy(q->params, w->x, sizeof(double)*w->basis_dims);
      q->params[j] += q->stepsize;
      q->ln_volume = sampler_calc_ln_volume(w, q->params, w->basis_dims);
      for (i=0; i<e->dims; i++)
	q->params[i] = w->queue[0].params[i] + q->stepsize*w->basis[j*e->dims + i];
      if (e->valid && !(e->valid(q->params))) q->stepsize *= -1.0*M_SQRT1_2;
      else break;
    }
  }
}

//Calculate derivatives from returned queue samples
void sampler_derivatives(struct Sampler *e, struct Walker *w) {
  int64_t i;
  double net_pe = w->queue[0].ln_prob + w->queue[0].ln_volume;
  for (i=0; i<w->basis_dims; i++) {
    double new_pe = w->queue[i+1].ln_prob + w->queue[i+1].ln_volume;
    w->derivatives[i] = (new_pe - net_pe) / w->queue[i+1].stepsize;
  }
  w->final_ln_prob = w->queue[0].ln_prob;
  w->final_ln_volume = w->queue[0].ln_volume;
  sampler_calc_net_prob(e, w);
}

//Find boundary normal given a point located at the boundary.  Params should be in user coordinates
void sampler_find_boundary_normal(struct Sampler *e, struct Walker *w, const double *params, double *normal) {
  if (!e->valid || !e->valid(params)) return;
  double *tmp_normal = sampler_get_stack(e);
  double *tmp_params = sampler_get_stack(e);
  for (int64_t i=0; i<w->basis_dims; i++) normal[i] = normal_random(0,1);
  sampler_unit_vectorize(w->basis_dims, normal);
  memset(tmp_normal, 0, sizeof(double)*e->dims);
  for (int64_t i=0; i<w->basis_dims; i++) {
    for (int64_t j=0; j<e->dims; j++)
      tmp_normal[j] += w->basis[i*e->dims+j]*normal[i];
  }
  for (int64_t i=0; i<w->basis_dims; i++) {
    //Find second valid point
    double dv = -e->dv*2.0;
    do {
      dv *= -0.5;
      for (int64_t j=0; j<e->dims; j++)
	tmp_params[j] = params[j] + tmp_normal[j]*dv;
    } while (!e->valid(tmp_params));

    double dv2max = 10.0, dv2p=0, dv2n=0, pdv=dv;
    while ((dv2p==dv2n || (fabs(dv2p) > fabs(pdv) && fabs(dv2n) > fabs(pdv))) && (fabs(dv) > fabs(e->dv)*1e-10)) {
      dv2p = sampler_find_valid_limit(e, tmp_params, w->basis+(i*e->dims), dv2max);
      for (int64_t j=0; j<w->basis_dims; j++) w->basis[i*e->dims+j]*=-1.0;
      dv2n = sampler_find_valid_limit(e, tmp_params, w->basis+(i*e->dims), dv2max);
      for (int64_t j=0; j<w->basis_dims; j++) w->basis[i*e->dims+j]*=-1.0;
      if (dv2p==dv2n || (fabs(dv2p) > fabs(pdv) && fabs(dv2n) > fabs(pdv))) {
	dv *= 0.5;
	for (int64_t j=0; j<e->dims; j++)
	  tmp_params[j] = params[j] + tmp_normal[j]*dv;
      }
    }
    if (dv2p!=dv2n) { //need to adjust normal vector
      double min = dv2p;
      if (dv2n < dv2p) min = -dv2n;
      //Need to find x such that (a + xb[i])*c = 0, where a = dv*normal and c=(dv*normal+min*b[i])
      double ac = dv*(dv + min*normal[i]);
      double bc = dv*normal[i]+min;
      if (bc != 0) {
	double x = -ac/bc;
	normal[i] -= x;
	for (int64_t j=0; j<e->dims; j++) tmp_normal[j] -= x*w->basis[i*e->dims+j];
	sampler_unit_vectorize(w->basis_dims, normal);
	sampler_unit_vectorize(e->dims, tmp_normal);
      }
    }
  }
  sampler_release_stack(e, &tmp_params);
  sampler_release_stack(e, &tmp_normal);
}

//Reflects component of vec that is perpendicular to boundary
//params should be in user coordinates; vec should be in basis coordinates
void sampler_reflect_off_boundary(struct Sampler *e, struct Walker *w, const double *params, double *vec) {
  if (!e->valid || !e->valid(params)) return;
  double *normal = sampler_get_stack(e);
  sampler_find_boundary_normal(e, w, params, normal);
  double dot = sampler_dot_product(w->basis_dims, vec, normal);
  for (int64_t i=0; i<w->basis_dims; i++) vec[i] -= 2.0*dot*normal[i];
  fprintf(stderr, "#reflected off boundary w/ normal %f %f\n", normal[0], normal[1]);
  sampler_release_stack(e, &normal);
}



//Update position/velocity of walker using a single leapfrog step
void sampler_leapfrog(struct Sampler *e, struct Walker *w) {
  int64_t i;
  check_log(e->log_file, "#%"PRId64" %"PRId64" %f %f %f x=%g v=%g d=%g; x=%g v=%g d=%g\n",
	    (int64_t)(w-e->w), w->step, w->final_net_prob - w->initial_net_prob, w->dt,
	    sqrt(sampler_dot_product(w->basis_dims, w->v, w->v)), w->x[0], w->v[0], w->derivatives[0],
	    w->x[1], w->v[1], w->derivatives[1]);
    
  //Terminate steps that go beyond the allowed probability range
  if (fabs(w->final_net_prob - w->initial_net_prob) >
      e->ln_prob_reject_threshold/e->inv_temp) {
    //&& (e->accepted + e->rejected < 4*e->num_walkers)) {
    w->final_ln_prob = SAMPLER_INVALID_LN_PROB;
    w->step = w->max_steps+1;
    return;
  }
  double vs = e->velocity_scaling*e->velocity_scaling;
    
  //Integrate position & velocity evolution
  //Note that derivatives are of -U = ln(prob*vol)
  double vdt = w->dt;
  if (w->step == 0) vdt *= 0.5; //Half-step kick

  if (w->step < w->max_steps) {
    for (i=0; i<w->basis_dims; i++) {
      w->v[i] += vdt*w->derivatives[i];     //Kick    
      w->x[i] += vs*w->dt*w->v[i];          //Drift
    }
  }
  
  //Check validity:
  if (e->valid) {
    sampler_translate_position(e, w, w->queue[1].params);
    double dt_used = 0;
    double dt_rem = w->dt;
    int64_t bounce = 0, max_bounces = 1000;
    while (!e->valid(w->queue[1].params) && bounce < max_bounces) {
      dt_used += dt_rem * sampler_find_valid_point(e, w->queue[0].params, w->queue[1].params);
      memcpy(w->queue[0].params, w->queue[1].params, sizeof(double)*e->dims);
      dt_rem = w->dt - dt_used;
      if (dt_rem < 0) dt_rem = 1e-9*w->dt; //In case of roundoff error...
      for (i=0; i<w->basis_dims; i++) w->x[i] -= dt_rem*vs*w->v[i]; //Back up from boundary
      fprintf(stderr, "#%"PRId64" %g %g %g %g x=%g v=%g\n", (int64_t)(w-e->w), dt_used, dt_rem, w->x[0], w->v[0], sqrt(sampler_dot_product(w->basis_dims, w->x, w->x)), sqrt(sampler_dot_product(w->basis_dims, w->v, w->v)));
      //Rotate and invert velocity
      memcpy(w->queue[2].params, w->v, sizeof(double)*w->basis_dims);
      sampler_random_rotate(w->basis_dims, 0, NULL, w->queue[2].params, w->v, e->theta_rotate);
      //Reflect off boundary
      /*sampler_translate_position(e, w, w->queue[1].params);
	sampler_reflect_off_boundary(e, w, w->queue[1].params, w->v);*/

      for (i=0; i<w->basis_dims; i++) {
	//w->v[i] = norm*w->v[i];
	w->x[i] += dt_rem*vs*w->v[i];
      }
      fprintf(stderr, "%"PRId64" New: %g %g x=%g v=%g\n", (int64_t)(w-e->w), w->x[0], w->v[0], sqrt(sampler_dot_product(w->basis_dims, w->x, w->x)), sqrt(sampler_dot_product(w->basis_dims, w->v, w->v)));
      sampler_translate_position(e, w, w->queue[1].params);
      bounce++;
    }
    if (bounce==max_bounces) return sampler_reject(e, w);
  }
  

  //No U-Turn criterion test
  /*if ((w->step < w->max_steps) && (w->x[0] < w->stopping_criterion)) {
    for (i=0; i<w->basis_dims; i++) {
      w->x[i] -= vs*w->dt*w->v[i];               //Drift
      w->v[i] -= w->dt*w->derivatives[i];     //Kick, full-step
    }
    w->step = w->max_steps;
    }*/

  
  if (w->step == w->max_steps) {
    for (i=0; i<w->basis_dims; i++) {
      w->v[i] += w->dt*w->derivatives[i]/2.0; //Kick, half-step
      w->v[i] *= -1.0;                        //Momentum flip
    }
    //check_log(e->log_file, "Stopped at %f %f %f %f\n", w->x[0], w->x[1], w->v[0], w->v[1]);
  }

  //Queue new derivatives if not yet finished.
  if (w->step < w->max_steps) sampler_queue_derivatives(e, w);
  w->step++;
}


/***************************************
Helper functions for vector manipulation
****************************************/

//Dot product of x and y
double sampler_dot_product(int64_t dims, const double *x, const double *y) {
  double norm = 0;
  for (int64_t i=0; i<dims; i++) norm += x[i]*y[i];
  return norm;
}

//Divides x by its norm
void sampler_unit_vectorize(int64_t dims, double *x) {
  double norm = sampler_dot_product(dims, x, x);
  if (!norm) return;
  norm = 1.0/sqrt(norm);
  for (int64_t i=0; i<dims; i++) x[i]*=norm;
}

//Generates a random vector on the unit sphere; note that a unit
//multivariate Gaussian is spherically symmetric.
void sampler_random_unit(int64_t dims, char *dim_flags, double *basis) {
  for (int64_t i=0; i<dims; i++) {
    if (dim_flags && (dim_flags[i]&SAMPLER_STATIC_DIM_FLAG)) basis[i] = 0;
    else basis[i] = normal_random(0, 1);
  }
  sampler_unit_vectorize(dims, basis);
}

//Generates a random vector that is perpendicular to the supplied basis vector
void sampler_random_unit_perp(int64_t dims, char *dim_flags, const double *basis, double *basis2) {
  sampler_random_unit(dims, dim_flags, basis2);
  double n = sqrt(sampler_dot_product(dims, basis, basis));
  if (!n) return;
  double d = sampler_dot_product(dims, basis, basis2)/n;
  for (int64_t i=0; i<dims; i++) {
    basis2[i] -= d*basis[i]/n;
    if (dim_flags && (dim_flags[i]&SAMPLER_STATIC_DIM_FLAG)) basis2[i] = 0;
  }
  sampler_unit_vectorize(dims, basis2);
}

void sampler_ergodic_unit_vectorize(long double *a, long double *b, long double *c, long double *d) {
  double total = sqrtl((*a)*(*a) + (*b)*(*b) + (*c)*(*c) + (*d)*(*d));
  if (!(total>0)) return;
  (*a) /= total;
  (*b) /= total;
  (*c) /= total;
  (*d) /= total;
}

//Generates a random vector of length ergodic_dims, and generates a random unit vector that is perpendicular
// to the combined vector: (regular, ergodic) 
void sampler_random_unit_perp_ergodic(int64_t dims, int64_t static_dims, char *dim_flags, const double *basis, double *basis2,
				      int64_t ergodic_dims, long double *ergo_length, long double *ergo_parallel, long double *ergo_perp) {
  *ergo_length = 0;
  *ergo_parallel = 0;
  *ergo_perp = 0;
  if (ergodic_dims<=0) return sampler_random_unit_perp(dims, dim_flags, basis, basis2);
  *ergo_length = sampler_chi_dist(ergodic_dims);
  long double n = sqrtl(sampler_dot_product(dims, basis, basis));
  long double full_length = sqrtl(n*n + (*ergo_length)*(*ergo_length));
  
  //Create unit vector uniformly distributed over the sphere by sampling from a normal distribution
  long double reg_perp = sampler_chi_dist(dims-static_dims-1);
  long double reg_parallel = normal_random(0,1);
  *ergo_perp = sampler_chi_dist(ergodic_dims-1);
  *ergo_parallel = normal_random(0,1);
  sampler_ergodic_unit_vectorize(&reg_perp, &reg_parallel, ergo_perp, ergo_parallel);

  //Take dot product with unit-normalized combined vector
  long double dot = (reg_parallel*n + (*ergo_parallel)*(*ergo_length))/full_length;

  //Subtract dot product with unit-normalized combined vector
  reg_parallel -= dot*n/full_length;
  (*ergo_parallel) -= dot*(*ergo_length)/full_length;

  //renormalize
  sampler_ergodic_unit_vectorize(&reg_perp, &reg_parallel, ergo_perp, ergo_parallel);
  
  //create random unit vector
  sampler_random_unit_perp(dims, dim_flags, basis, basis2);
  
  //fprintf(stderr, "[Info]: reg length: %f; reg_parallel: %f; reg_perp: %f; ergodic length: %f; perp: %f; parall: %f\n", n, reg_parallel, reg_perp, *ergo_length, *ergo_perp, *ergo_parallel);
  
  //Rescale and rotate:
  for (int64_t i=0; i<dims; i++) {
    basis2[i] = reg_parallel*basis[i]/n + reg_perp*basis2[i];
    if (dim_flags && (dim_flags[i]&SAMPLER_STATIC_DIM_FLAG)) basis2[i] = 0;
  }
}


//Convenience function
void sampler_random_unit_walker(struct Walker *w, double *basis) {
  sampler_random_unit(w->dims, w->dim_flags, basis);
}

//Generate the cosine of a random angle between two vectors in dims
//dimensions, for angles between 0 and max_theta.
//PDF of C=cos(theta) is propto (1-C^2)^((D-3)/2)
//Hence, PDF of X = (1-C)/2 is propto (X^((D-3)/2))(1-X)^((D-3)/2),
//Which has integral given by Beta_X((D-1)/2,(D-1)/2)
double sampler_cos_random_angle(double dims, double max_theta) {
  if (fabs(max_theta)>M_PI) max_theta = M_PI;
  double max_x = (1.0 - cos(max_theta))/2.0;
  double dim_const = (dims-1.0)/2.0;

  if (dims==1) return 1;
  else if (dims == 2) //D=2, theta has const distribution
    return cos(dr250()*max_theta);
  else if (dims == 3) //D=3, cos(theta) has const distribution
    return 1.0-dr250()*max_x*2.0;
  double rand_x = sampler_rand_incbeta(max_x, dim_const, dim_const);
  return 1.0-rand_x*2.0;
}

//Rotates a vector within a given random angle.
//Algorithm: generate a random perpendicular unit vector to *in, calculate the
//cosine of a random angle (from sampler_sin_random_angle), and return
//cos(theta)*in + |in|*sin(theta)*(rand. perp. unit)
void sampler_random_rotate(int64_t dims, int64_t static_dim_count, char *static_dims, const double *in, double *out, double theta) {
  if ((dims-static_dim_count)<=1) {
    memcpy(out, in, sizeof(double)*dims);
    return;
  }
  double norm = sampler_dot_product(dims, in, in);
  if (!norm) return;
  sampler_random_unit_perp(dims, static_dims, in, out);
  double c = sampler_cos_random_angle(dims-static_dim_count, theta);
  double s = sqrt(1.0-c*c);
  s *= sqrt(norm);
  for (int64_t i=0; i<dims; i++) out[i] = c*in[i] + s*out[i];
}

/*********************************
Functions for step size management
**********************************/

void sampler_set_stepsize(struct Sampler *e, double stepsize) {
  if (e->mode == ALEXANDER_SAMPLER) {
    e->dt=stepsize;
    check_log(e->log_file, "#Set DT to %lf after %"PRId64" steps (%"PRId64" accepted).\n",
	      e->dt, e->avg_steps, e->accepted);
  }
  else if (e->mode == ANGULAR_METROPOLIS_SAMPLER) {
    e->dtheta = stepsize;
    e->dr = e->dtheta;
    if (e->dtheta > SAMPLER_DTHETA_MAX) e->dtheta = SAMPLER_DTHETA_MAX;
    if (e->dr > SAMPLER_DR_MAX) e->dr = SAMPLER_DR_MAX;
    check_log(e->log_file, "#Set DTheta,DR to %lf, %lf after %"PRId64" steps (%"PRId64" accepted).\n",
			e->dtheta, e->dr, e->avg_steps, e->accepted);
  }
  else if (e->mode == STANDARD_METROPOLIS_SAMPLER) {
    e->dr = stepsize;
    if (e->dr > SAMPLER_DR_MAX) e->dr = SAMPLER_DR_MAX;
    check_log(e->log_file, "#Set DR to %lf after %"PRId64" steps (%"PRId64" accepted).\n",
	      e->dr, e->avg_steps, e->accepted);
  }
}


/*************************************
Functions for success array management
**************************************/
void sampler_init_success_array(struct Sampler *e, double stepsize) {
  check_calloc_s(e->success, sizeof(struct SamplerSuccess), 1);
  e->step_min = e->step_max = stepsize;
  e->num_success = 1;
}

double sampler_draw_stepsize(struct Sampler *e, double target, double max_step) {
  int64_t i;
  double t=0, w=0;
  for (i=0; i<e->num_success; i++) {
    double frac = (e->success[i].success+1)/(e->success[i].tried+1);
    if (frac > target) break;
    t+=frac*frac;
    w+=frac;
  }

  //Test if we need to increase step size
  if (i==0) {
    if (e->success->tried>0 && e->step_max*SAMPLER_SUCCESS_ARRAY_SPACING <= max_step) {
      check_realloc_s(e->success, sizeof(struct SamplerSuccess), e->num_success+1);
      memmove(e->success+1, e->success, sizeof(struct SamplerSuccess)*e->num_success);
      e->success->success = e->success->tried = 0;
      e->step_max *= SAMPLER_SUCCESS_ARRAY_SPACING;
      e->num_success++;
    }
  }

  //Test if we need to decrease step size
  else if (i==e->num_success) {
    if (e->success[i-1].tried>0) {
      check_realloc_s(e->success, sizeof(struct SamplerSuccess), e->num_success+1);
      e->success[i].success=e->success[i].tried=0;
      e->num_success++;
    } else {
      i--;
    }
  }

  //Otherwise, draw from existing distribution
  else {
    assert(w>0);
    double lower_frac = t/w;
    double higher_frac = (e->success[i].success+1)/(e->success[i].tried+1);
    //(1-x)*lower_frac + x*higher_frac = target, so x = (target - lower_frac) / (higher_frac-lower_frac)
    double x = (target - lower_frac)/(higher_frac - lower_frac);
    if (dr250()>x) { //should pick a lower value of i
      double r = dr250()*t;
      t = 0;
      for (i=0; i<e->num_success; i++) {
	double frac = (e->success[i].success+1)/(e->success[i].tried+1);
	if (t+frac*frac > r) break;
	t+=frac*frac;
      }
    }
  }

  //We now randomly select within the success bin
  return e->step_max*pow(SAMPLER_SUCCESS_ARRAY_SPACING, -(i+dr250()));
}


void sampler_update_success(struct Sampler *e, double stepsize, double tried, double success) {
  int64_t i = log(e->step_max/stepsize)/log(SAMPLER_SUCCESS_ARRAY_SPACING);
  if (i<0 || i>=e->num_success) fprintf(stderr, "%"PRId64" %f %f %f %f\n", i, stepsize, e->step_min, e->step_max, e->dt);
  assert(i>=0 && i<e->num_success);
  e->success[i].success += success;
  e->success[i].tried += tried;
}

/**********************************
Functions for managing walker state
***********************************/
void sampler_adapt_steps(struct Sampler *e, int64_t initial) {
  e->ignore_steps += e->new_ignore_steps;
  double new_steps = e->num_walkers - e->new_ignore_steps;
  double successful = e->accepted - e->prev_accepted;
  if (e->accepted+e->rejected<e->running_avg_length) {
    e->running_success_fraction = ((double)e->accepted/(double)(e->avg_steps-e->ignore_steps));
  } else if (new_steps) {
    double weight = exp(-new_steps / (double)e->running_avg_length);
    double new_f =  ((double)e->accepted-(double)e->prev_accepted)/new_steps;
    e->running_success_fraction *= weight;
    e->running_success_fraction += (1.0-weight)*new_f;
  }
  e->new_ignore_steps = 0;
  e->prev_accepted = e->accepted;

  if (initial) return;
  
  double success_delta_new = 0;
  
  if (e->adaptation_mode == SAMPLER_ADAPT_SUCCESS_RATIO) {
    double success_delta = (e->running_success_fraction - e->success_target);
    success_delta_new = success_delta*new_steps/(double)e->num_walkers;
  }
  else if (e->adaptation_mode == SAMPLER_ADAPT_SUCCESS_ARRAY) {
    double stepsize = 0, max_step = 0;
    if (e->mode == ALEXANDER_SAMPLER) { stepsize = e->dt; max_step = SAMPLER_DT_MAX; }
    if (e->mode == ANGULAR_METROPOLIS_SAMPLER) { stepsize = e->dtheta; max_step = SAMPLER_DTHETA_MAX; }
    if (e->mode == STANDARD_METROPOLIS_SAMPLER) { stepsize = e->dr; max_step = SAMPLER_DR_MAX; }
    sampler_update_success(e, stepsize, new_steps, successful);
    double new_stepsize = sampler_draw_stepsize(e, e->success_target, max_step);
    success_delta_new = log(new_stepsize / stepsize);
  }
    
    
  if (e->accepted == 0) e->ignore_steps = e->avg_steps;    
  
  if (e->mode == ALEXANDER_SAMPLER) {
    e->dt *= exp(success_delta_new);
    check_log(e->log_file, "#Adapted DT to %lf after %"PRId64" steps (%"PRId64" accepted).\n",
	      e->dt, e->avg_steps, e->accepted);
  }
  else if (e->mode == ANGULAR_METROPOLIS_SAMPLER) {
    e->dtheta *= exp(success_delta_new);
    e->dr *= exp(success_delta_new);
    if (e->dtheta > SAMPLER_DTHETA_MAX) e->dtheta = SAMPLER_DTHETA_MAX;
    if (e->dr > SAMPLER_DR_MAX) e->dr = SAMPLER_DR_MAX;
    if (e->dtheta < SAMPLER_DR_MAX) { e->dr = e->dtheta; }
    check_log(e->log_file, "#Adapted DTheta,DR to %lf, %lf after %"PRId64" steps (%"PRId64" accepted) SD: %f.\n",
	      e->dtheta, e->dr, e->avg_steps, e->accepted, success_delta_new);
  }
  else if (e->mode == STANDARD_METROPOLIS_SAMPLER) {
    e->dr *= exp(success_delta_new);
    if (e->dr > SAMPLER_DR_MAX) e->dr = SAMPLER_DR_MAX;
    check_log(e->log_file, "#Adapted DR to %lf after %"PRId64" steps (%"PRId64" accepted) SD: %f.\n",
	      e->dr, e->avg_steps, e->accepted, success_delta_new);
  }
}



//Check whether all walkers are complete; if so, then update walker trajectories.
void sampler_update_state(struct Sampler *e) {
  int64_t i, j, initial = 0;
  if (e->state == SAMPLER_STATE_INVALID) {
    check_log(e->error_file, "[Sampler:Error] You must initialize sampler with parameter values before calling sampler_get_sample()\n");
    exit(EXIT_FAILURE);
  }
  if (e->state==SAMPLER_STATE_INITIAL_EVALUATION) {
    if (e->walkers_done == e->num_walkers) {
      e->state = SAMPLER_STATE_FULL_EXPLORATION;
      initial = 1;
    }
  }
  if (e->state==SAMPLER_STATE_INITIAL_EXPLORATION) {
    if (e->walkers_done == e->num_walkers) {
      e->state = SAMPLER_STATE_FULL_EXPLORATION;
      initial = 1;
    }
  }

  //If we've finished the initial eval/explore state,
  // immediately proceed to setting up full exploration
  if (e->state==SAMPLER_STATE_FULL_EXPLORATION) {
    while (e->walkers_done == e->num_walkers) {
      e->walkers_done = 0;
      e->total_steps += e->num_walkers;

      //Update covariance matrix, DT, and origin if still in adaptive phase
      if ((e->avg_steps < e->adaptive_samples)) {
	sampler_update_averages(e);
	if ((e->avg_steps >= e->last_adaptation + e->steps_per_adaptation) &&
	    !e->freeze_covariance_matrix)
	  sampler_stats_to_step(e);
	if (e->avg_steps > e->num_walkers)
	  sampler_adapt_steps(e, initial);
	
	if (e->origin_type == SAMPLER_ORIGIN_BESTFIT) {
	  for (i=0; i<e->num_walkers; i++)
	    sampler_check_bestfit_origin(e, e->w[i].params, e->w[i].initial_ln_prob);
	}
	else if (e->origin_type == SAMPLER_ORIGIN_AVERAGE && e->avg_steps > e->min_covariance_steps) {
	  sampler_set_origin_as_average(e);
	}
      }
      
      //Set up new velocities and bases
      if (e->mode == ALEXANDER_SAMPLER) {
	for (i=0; i<e->num_walkers; i++) sampler_init_alexander_step(e, e->w+i);
      } else if (e->mode == ANGULAR_METROPOLIS_SAMPLER) {
	for (i=0; i<e->num_walkers; i++) sampler_angular_step(e, e->w+i, e->dtheta);
      } else if (e->mode == STANDARD_METROPOLIS_SAMPLER) {
	for (i=0; i<e->num_walkers; i++) sampler_metropolis_step(e, e->w+i);
      } else if (e->mode == SEARCHLIGHT_SAMPLER) {
	for (i=0; i<e->num_walkers; i++) sampler_init_searchlight_step(e, e->w+i);
      }

      
      //Print accepted positions + chi2 values
      int64_t iterations = (e->accepted+e->rejected) / e->num_walkers;
      if (((iterations%e->step_thinning)==0) &&
	  (e->save_adaptive_steps || (e->avg_steps >= e->adaptive_samples))) {
	for (i=0; i<e->num_walkers; i++) {
	  if (e->step_completed) e->step_completed(i, e->w[i].params, e->w[i].initial_ln_prob);
	  if (e->output_file) {
	    for (j=0; j<e->dims; j+=e->dimension_thinning)
	      check_fprintf(e->output_file, "%.16e ", e->w[i].params[j]);
	    check_fprintf(e->output_file, "%.16e %.16e\n",
			  e->w[i].initial_ln_prob, -2.0*e->w[i].initial_ln_prob);
	    fflush(e->output_file);
	  }
	}
      }
    }
  }
}

//Check if a new state should be accepted/rejected
void sampler_decide_acceptance(struct Sampler *e, struct Walker *w) {
  int64_t accept = 1;
  sampler_calc_net_prob(e, w);

  //Reject new state if invalid or if its probability is too low
  double delta_prob = w->inv_temp*(w->final_net_prob - w->initial_net_prob);
  if (w->final_ln_prob >= SAMPLER_INVALID_LN_PROB) accept = 0;
  if ((w->initial_ln_prob < SAMPLER_INVALID_LN_PROB)
      && (dr250() > exp(delta_prob)))
    accept = 0;
      
  //If accepted, update w->params and w->initial_ln_prob
  if (accept) {
    w->initial_ln_prob = w->final_ln_prob;
    memcpy(w->params, w->queue->params, sizeof(double)*e->dims);
    w->success_streak++;
    if (w->success_streak > w->longest_streak) w->longest_streak = w->success_streak;
    e->accepted++;
  }
  else {
    e->rejected++; //Otherwise, leave w->params as-is
    fprintf(stderr, "Delta Prob: %e\n", delta_prob);
    /*int64_t total_rejected = 0;
    for (int64_t i=0; i<SAMPLER_MAX_REJECTION_TYPES; i++) total_rejected += e->rejection_stats[i];
    if (e->rejected < 1000) { e->rejection_stats[20] += e->rejected-total_rejected; }
    else assert(total_rejected == e->rejected);*/
    w->success_streak=0;
  }
  e->walkers_done++;
  sampler_update_state(e);
}

void sampler_reject(struct Sampler *e, struct Walker *w) {
  w->final_ln_prob = SAMPLER_INVALID_LN_PROB;
  sampler_decide_acceptance(e, w);
}

/*************************************
Functions generic to boundary sampling
**************************************/

void sampler_decide_boundary_acceptability(struct Sampler *e, struct Walker *w, int64_t samples_needed_old, int64_t samples_needed_new) {
  double acceptance_rate = (double)samples_needed_new / (double)samples_needed_old;
  if (dr250()>acceptance_rate) {
    w->queue->state = QUEUESAMPLE_COMPLETE;
    e->walkers_done++;
    e->rejected++;
    e->new_ignore_steps++;
    //fprintf(stderr, "Rejected sample (%"PRId64" vs. %"PRId64")\n", samples_needed_old, samples_needed_new); 
  }
}


/*************************************************
Functions specific to Standard Metropolis Sampling
*************************************************/

void sampler_metropolis_step(struct Sampler *e, struct Walker *w) {
  w->mode = STANDARD_METROPOLIS_SAMPLER;
  double step_size = e->dr*SAMPLER_METROPOLIS_SCALING / sqrt(e->dims-e->static_dims);
  double *params = sampler_get_stack(e);
  double *new_params = sampler_get_stack(e);
  double *new_params2 = sampler_get_stack(e);
  sampler_detranslate_basis(e, w, w->params, params);

  int64_t samples = 0, old_samples = 0, round = 0;
  w->samples_present = 0;
  w->queue_expected = 1;
  w->initial_net_prob = w->initial_ln_prob;
  struct QueueSample *q = sampler_reset_queue_sample(e,w,0);
  q->ln_volume = 0;
  double *results = q->params;

  do {
    for (int64_t j=0; j<e->dims; j++)
      new_params[j] = params[j] + normal_random(0, step_size);
    sampler_translate_basis(e, w, new_params, results, 1);
    samples++;
    if (e->valid && e->valid(results)) {
      if (round==0) {
	memcpy(params, new_params, sizeof(double)*e->dims);
	old_samples = samples;
	samples = 0;
	results = new_params2;
      }
      round++;
    }
  } while (e->valid && round<2);
  sampler_release_stack(e, &new_params2);
  sampler_release_stack(e, &new_params);
  sampler_release_stack(e, &params);
  if (round==2) sampler_decide_boundary_acceptability(e, w, old_samples, samples);
}

void sampler_angular_step(struct Sampler *e, struct Walker *w, double dtheta) {
  w->mode = ANGULAR_METROPOLIS_SAMPLER;
  int64_t origin = dr250()*e->num_origins;
  w->origin = e->origins + origin*e->dims;
  double *params = sampler_get_stack(e);
  double *new_params = sampler_get_stack(e);
  double *new_params2 = sampler_get_stack(e);
  double *metro_params = sampler_get_stack(e);
  
  double theta=dtheta, dr=e->dr;
  int64_t old_samples = 0, samples = 0, round = 0;

  w->samples_present = 0;
  w->queue_expected = 1;
  w->initial_net_prob = w->initial_ln_prob;
  struct QueueSample *q = sampler_reset_queue_sample(e,w,0);
  double *results = q->params;
  sampler_detranslate_basis(e, w, w->params, params);
  
  do {
    if (!(samples%100)) {
      theta = normal_random(0, dtheta);
      dr = normal_random(0, e->dr);
    }
    sampler_random_unit_perp(w->dims, w->dim_flags, params, new_params);
    double n = sqrt(sampler_dot_product(w->dims, params, params));
    double r_factor = (n+dr)/n;
    double ln_volume = ((double)w->dims-w->static_dims-1.0)*log(fabs(r_factor));
    double c = cos(theta);
    double s = sin(theta);
    s*=n;
    for (int64_t j=0; j<e->dims; j++)
      new_params[j] = r_factor*(params[j]*c + new_params[j]*s);
    sampler_translate_basis(e, w, new_params, results, 1);
    /*if (e->mix_metropolis) {
      int64_t mdims=0;
      for (int64_t j=0; j<e->dims; j++)
	if (SAMPLER_METROPOLIS_DIM(e,j)) {
	  do { results[j] = normal_random(0, 1); } while (fabs(results[j])>1);
	}
      ln_volume = ((double)w->dims-w->static_dims-1.0-mdims)*log(fabs(r_factor));
      }*/
    if (round==0) q->ln_volume = ln_volume;
    
    samples++;

    if (e->valid && e->valid(results)) {
      if (round==0) {
	q->ln_volume = ln_volume;
	memcpy(params, new_params, sizeof(double)*e->dims);
	old_samples = samples;
	samples = 0;
	results = new_params2;
      }
      round++;
    }
  } while (e->valid && round<2);
  sampler_release_stack(e, &metro_params);
  sampler_release_stack(e, &new_params2);
  sampler_release_stack(e, &new_params);
  sampler_release_stack(e, &params);
  if (round==2) sampler_decide_boundary_acceptability(e, w, old_samples, samples);
}


/************************************************
Functions specific to Searchlight Sampling
*************************************************/

//Add derivatives at current walker position to queue
void sampler_searchlight_queue_derivatives(struct Sampler *e, struct Walker *w) {
  double xi = w->x[0];
  w->x[0] = w->final_x + w->direction*w->x[0];
  double dx = e->exploration_dx;
  sampler_generic_queue_derivatives(e, w, dx, 0);
  w->x[0] = xi;
}

//Solves ax^2 + bx + c = 0; stores solutions in x1 and x2, with *x1 <= *x2
int sampler_quadratic_solution(long double a, long double b, long double c, long double *x1, long double *x2) {
  long double b2 = b*b;
  long double ac4 = 4.0l*a*c;
  if (a==0) { //Linear equation
    if (b==0) return 0;
    *x1 = *x2 = (-c/b);
  }
  if (b2 < ac4) return 0;
  long double sq_disc = sqrtl(b2 - ac4);
  *x1 = (-b - sq_disc)/(2.0l*a);
  *x2 = (-b + sq_disc)/(2.0l*a);
  if (*x1 > *x2) {
    long double tmp = *x1;
    *x1 = *x2;
    *x2 = tmp;
  }
  return 1;
}


void sampler_detranslate_searchlight_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params) {
  sampler_detranslate_basis(e,w,in_params,out_params);
}

void sampler_translate_searchlight_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params, int64_t translate_origin) {
  sampler_translate_basis(e,w,in_params,out_params,translate_origin);
}

long double sampler_u_to_theta(struct Walker *w, long double s) {
  if (w->phi > 0) s = -s;
  if (w->flip_u) s = -s;
  long double A = w->refraction_gradient;
  long double D = w->dims - w->static_dims + w->ergodic_dims;
  long double exponent = A/(D*(1.0l-A))-1.0l;
  long double r_c = w->r_0*powl(cosl(w->phi), -exponent);
  long double u_0 = r_c*sqrtl(powl(cosl(w->phi), exponent*2.0l)-1.0l);
  long double sign = (s>u_0) ? 1.0l : -1.0l;
  if (w->phi > 0) sign = -sign;
  long double theta = 1.0l/(1.0l-A)*(-w->phi + sign*acosl(powl((powl((s-u_0)/r_c,2)+1.0l),1.0l/(2.0l*exponent))));
  return theta;
}

void sampler_translate_refraction_gradient(struct Walker *w, double x, double *out_params, long double *v) {
  //Treat x as theta parameter
  //n(r') = r'^(-A)
  //r'(theta) = r_0[cos((1-A)theta + phi]
  x = sampler_u_to_theta(w, x);
  long double orig_norm = w->r_0;
  long double A = w->refraction_gradient;
  long double D = w->dims - w->static_dims + w->ergodic_dims;
  long double power = -A/(D*(1.0l-A))+1.0l;
  long double new_r = w->r_0*powl(cosl(w->phi) / cosl((1.0l-A)*x+w->phi), power);
  long double c = cosl(x);
  long double s = sinl(x);
  if (orig_norm==0) {
    orig_norm = 1;
    c = s = 0;
  }
  for (int64_t i=0; i<w->dims; i++) {
    out_params[i] = c*(w->params[i]-w->origin[i])*new_r/orig_norm + s*w->basis[i]*new_r + w->origin[i];
    if (SAMPLER_STATIC_DIM(w,i)) out_params[i] = w->origin[i];
  }

  if (w->ergodic_dims) {
    long double e_parallel = w->ergodic_length*c*new_r/orig_norm + s*w->ergodic_unit_parallel*new_r;
    long double e_perp = s*w->ergodic_unit_perp*new_r;
    *v = sqrtl(e_parallel*e_parallel + e_perp*e_perp);
  } else {
    *v = 0;
  }
}


void sampler_searchlight_set_ergodic_dims(struct Sampler *e, double dims) {
  if (dims!=0 && (dims < 1)) {
    fprintf(stderr, "[Error] Ergodic dimensions must be set to either 0 or a value that is at least 1.\n");
    exit(EXIT_FAILURE);
  }
  e->ergodic_dims = dims;
  for (int64_t i=0; i<e->num_walkers; i++)
    e->w[i].ergodic_dims = dims;
}


void sampler_searchlight_write_path(struct Sampler *e, struct Walker *w, int64_t id, double max_x) {
  double *params = sampler_get_stack(e);
  char buffer[1024];
  int64_t wid = w-e->w;
  snprintf(buffer, 1024, "path_w%"PRId64"_p%"PRId64".txt", wid, id);
  FILE *out = check_fopen(buffer, "w");
  for (int64_t i=0; i<=1000; i++) {
    double x = i*max_x/1e3;
    long double v=0;
    if (w->translate_function) w->translate_function(w, x, params, &v);
    else {
      for (int64_t j=0; i<e->dims; i++)
	params[j] = w->params[j] + x*w->basis[j];
      if (w->ergodic_dims) {
	long double e_parallel = w->ergodic_length + x*w->ergodic_unit_parallel;
	long double e_perp =  x*w->ergodic_unit_perp;
	v = sqrtl(e_parallel*e_parallel + e_perp*e_perp);
      } else {
	v = 0;
      }
    }
  
    for (int64_t j=0; j<e->dims && j<10; j++) 
      fprintf(out, "%e ", params[j]);
    fprintf(out, "%Le\n", v);
  }
  fclose(out);
  sampler_release_stack(e, &params);
}


void sampler_searchlight_write_spline(struct Sampler *e, struct Walker *w, int64_t id, int64_t dir, int64_t step, double new_root) {
  char buffer[1024];
  int64_t wid = w-e->w;
  snprintf(buffer, 1024, "spline_w%"PRId64"_p%"PRId64"_d%"PRId64"_s%06"PRId64".txt", wid, id, dir, step);
  FILE *out = check_fopen(buffer, "w");
  double max_x = w->aspline->v[w->aspline->n-1].x;
  fprintf(out, "%e 0 0\n\n", new_root);
  for (int64_t i=0; i<=1000; i++) {
    double x = i*max_x/1e3;
    double px = (dir <0) ? w->final_x-x : x;
    fprintf(out, "%e %e %e\n", px, (double)aspline_eval(w->aspline, x), (double)aspline_estimate_uncertainty(w->aspline, x));
  }
  if (new_root > max_x) {
    fprintf(out, "\n");
    for (int64_t i=0; i<=1000; i++) {
      double x = i*(new_root-max_x)/1e3 + max_x;
      double px = (dir <0) ? w->final_x-x : x;
      fprintf(out, "%e %e 0\n", px, (double)aspline_eval(w->aspline, x));
    }
  }
  fprintf(out, "\n");
  for (int64_t i=0; i<w->aspline->n; i++) {
    double x = w->aspline->v[i].x;
    double px = (dir <0) ? w->final_x-x : x;
    fprintf(out, "%e %e 0\n", px, (double)(w->aspline->v[i].y));
  }

  fclose(out);
}


#define REJECT(x) { fprintf(stderr, "Reject %d, %"PRId64" (W%"PRId64" C%"PRId64" S%"PRId64")\n", x, w->direction, (int64_t)(w-e->w), w->count, w->step); if (x<SAMPLER_MAX_REJECTION_TYPES) { e->rejection_stats[x]++; } }
void sampler_init_searchlight_step(struct Sampler *e, struct Walker *w) {
  w->translate_function = NULL;

  //Do angular step with specified probability
  if (dr250() < e->metropolis_frac) {
    sampler_angular_step(e, w, e->dtheta/1000.0);
    return;
  }

  //Otherwise, do searchlight step
  w->mode = SEARCHLIGHT_SAMPLER;
  double *params = sampler_get_stack(e);
  double *new_params = sampler_get_stack(e);
  sampler_detranslate_searchlight_basis(e, w, w->params, params);
  if (!w->ergodic_dims)
    sampler_random_unit_perp(w->dims, w->dim_flags, params, new_params);
  else
    sampler_random_unit_perp_ergodic(w->dims, w->static_dims, w->dim_flags, params, new_params,
				     w->ergodic_dims, &(w->ergodic_length), &(w->ergodic_unit_parallel),
				     &(w->ergodic_unit_perp));

  //fprintf(stderr, "[Info]: p1: %le; p2: %le\n", params[0], new_params[0]);
  //Need to generate dtheta for each w.
  long double c = -sampler_cos_random_angle(e->dims+e->ergodic_dims-e->static_dims, e->dtheta/2.0);
  long double n = sqrtl(sampler_dot_product(w->dims, params, params));
  long double s = -sqrtl(1.0l-c*c);

  //fprintf(stderr, "[Info]: Initial length: %f; ergodic length: %f; perp: %f; parall: %f\n", n, w->ergodic_length, w->ergodic_unit_perp, w->ergodic_unit_parallel);
  //fprintf(stderr, "[Info]: c=%g\n", c);
  
  if (!w->refraction_gradient) {
    if (n>0) c/=n;
    for (int64_t j=0; j<e->dims; j++)
      new_params[j] = params[j]*c + new_params[j]*s;
    sampler_translate_searchlight_basis(e, w, new_params, w->basis, 1);
  } else {
    w->translate_function = sampler_translate_refraction_gradient;
    sampler_translate_searchlight_basis(e, w, new_params, w->basis, 0);
    w->phi = M_PI/2.0 - acos(c);
    w->r_0 = sqrt((n*n)+(w->ergodic_length*w->ergodic_length));
  }
  sampler_release_stack(e, &new_params);
  sampler_release_stack(e, &params);
  fprintf(stderr, "[Info]: b1: %le; b2: %le\n", w->basis[0], w->basis[1]);
  
  w->step = 0;
  w->x[0] = w->final_x = 0;
  w->adventuring = 0;
  w->root_count = 0;
  w->direction = 1;
  w->flip_u = 0;
  sampler_searchlight_queue_derivatives(e, w);
  w->samples_present = 1;
  w->queue_expected = 3;
  w->queue[0].ln_prob = w->queue[0].net_prob = w->initial_ln_prob;
  w->queue[0].state = QUEUESAMPLE_COMPLETE;
  w->initial_ln_volume = 0;
  w->ergodic_length = w->queue->v; // Re-setting here because of floating-point imprecision issues...
  w->initial_ke = (w->ergodic_dims) ? -sampler_ergodic_ln_likelihood(w->ergodic_length) : 0;
  w->initial_net_prob = w->initial_ln_prob -  w->initial_ke;
  w->final_ln_prob = w->initial_ln_prob;
  w->final_ke = w->initial_ke;

  w->tunneling_threshold = e->tunneling_threshold / sqrt(dr250());
  
  //Excluded region: (a+x*b)*b < cos(e->dtheta)*|a+x*b|
  //  where a = vector to current params, b = basis vector
  //Solution: (1-C^2)*x^2 + 2B(1-C^2)*x + B^2 - C^2 A^2 = 0
  //  where A = |a|, B = a*b, C = cos(e->dtheta)
  if (!w->refraction_gradient) {
    long double A = n;
    long double B = c*n*n; //c = a*b/|n|;
    long double C = cosl(e->dtheta/2.0l);
    int res = sampler_quadratic_solution(1.0l-C*C, 2.0l*B*(1.0l-C*C), B*B-C*C*A*A, &(w->exclude_min), &(w->exclude_max));  
    if (!res) { //Impossible to satisfy equation, so reject point
      w->queue[1].state = w->queue[2].state = QUEUESAMPLE_COMPLETE;
      REJECT(0);
      sampler_reject(e, w);
    }
  } else {
    w->exclude_min = w->exclude_max = 0;
  }

  if (w->aspline==NULL) w->aspline = aspline_new();
  aspline_reset(w->aspline);
  w->count++;
  if ((w->count %100)==0) sampler_searchlight_write_path(e, w, w->count,  10*sqrt(w->dims+w->ergodic_dims-w->static_dims));
  
}

void sampler_searchlight_victory(struct Sampler *e, struct Walker *w) {
  if (!w->translate_function) {
    for (int64_t i=0; i<e->dims; i++)
      w->queue->params[i] = w->params[i] + w->final_x*w->basis[i]; 
  } else {
    long double v;
    w->translate_function(w, w->final_x, w->queue->params, &v);
    w->final_ke = -sampler_ergodic_ln_likelihood(v);
  }
  //w->final_net_prob = w->initial_net_prob; w->final_ln_volume=w->initial_ln_volume; w->final_ke = w->final_ln_prob + w->initial_ke - w->initial_ln_prob;
  sampler_decide_acceptance(e, w);
}


long double sampler_dL_ds(struct Walker *w) {
  long double derivative_multiplier = 1;
  if (w->refraction_gradient) {
    long double A = w->refraction_gradient;
    long double dtheta_du = (sampler_u_to_theta(w, w->queue[1].x)-sampler_u_to_theta(w, w->queue[2].x))/(w->queue[1].stepsize - w->queue[2].stepsize);
    //long double ds_dtheta = powl(cos((1.0l-A)*w->queue[0].x+w->phi), 1.0l/(A-1.0l)-1.0l);
    long double ds_dtheta = powl(cos((1.0l-A)*sampler_u_to_theta(w, w->queue[0].x)+w->phi), 1.0l/(A-1.0l)-1.0l);
    derivative_multiplier = fabsl(ds_dtheta*dtheta_du);
  }
  return fabsl(w->derivatives[0]*derivative_multiplier);
}

double sampler_searchlight_delta_ln_prob(struct Walker *w) {
  double cur_ln_prob = w->queue->ln_prob + sampler_ergodic_ln_likelihood(w->queue->v);
  double final_ln_prob = w->final_ln_prob - w->final_ke;
  return cur_ln_prob - final_ln_prob;
}

double sampler_searchlight_delta_ln_prob_queue(struct Walker *w, struct QueueSample *q) {
  double cur_ln_prob = q->ln_prob + sampler_ergodic_ln_likelihood(q->v);
  double final_ln_prob = w->final_ln_prob - w->final_ke;
  return cur_ln_prob - final_ln_prob;
}

int64_t sampler_poisson_distribution(double mu) {
  int64_t n=-1;
  double sum = 0;
  while (sum<mu) {
    sum -= log(dr250());
    n++;
  }
  return n;
}

//Find a location y such that P(y) matches P(x)
void sampler_searchlight(struct Sampler *e, struct Walker *w) {
  int64_t i, flip_direction = 0;
  if (w->step == 0) {
    sampler_generic_derivatives(e, w);
    if (isnan(w->derivatives[0]) || isnan(w->derivatives[1])) {
      REJECT(9); sampler_reject(e,w); return;
    }
    w->derivatives[0]*=w->direction; //Second derivative is unchanged
    //Decide direction of travel
    if (!w->derivatives[0]) { REJECT(1); sampler_reject(e, w); return; } //Reject direction if zero derivative: not possible to define acceptance probability
    if (w->direction==1) {
      if (dr250()<e->adventuring_prob) {
	w->adventuring=1;
	fprintf(stderr, "ADVENTURING!!!\n");
      }
      if ((w->derivatives[0]<0 && !w->adventuring) ||
	  (w->derivatives[0]>0 && w->adventuring)) { //Switch direction to point towards positive / negative probability
	flip_direction = 1;
	if (!w->refraction_gradient) {
	  for (i=0; i<e->dims; i++) w->basis[i]*=-1.0;
	  if (w->ergodic_dims) w->ergodic_unit_parallel*=-1.0;
	  w->derivatives[0] *= -1.0; //second derivative is unchanged.
	  double tmp = w->exclude_max;
	  w->exclude_max = -(w->exclude_min);
	  w->exclude_min = -tmp;
	} else {
	  w->derivatives[0] *= -1.0; //second derivative is unchanged.
	  w->flip_u = 1;
	}
      }

      w->root_count = 1;
      if (e->tunneling_prob>0 && e->tunneling_prob < 1) 
	w->root_count = 1+sampler_poisson_distribution(1.0/(1.0-e->tunneling_prob)-1.0);
    }
    
    //Set min_x based on excluded region
    w->min_x = w->exclude_max; //While there could be a nearby point, avoid it...
    if (w->min_x < 0) w->min_x = 0; //Must go in positive direction
    w->max_x = 0;
    
    if (w->refraction_gradient) {
      w->max_x = 3*sqrt(w->dims+w->ergodic_dims-w->static_dims);
    }
    
    //Set ln_volume to -ln of |derivative|
    if (w->direction==1) {
      w->initial_dL_du = w->derivatives[0];
      w->initial_ln_volume = -log(sampler_dL_ds(w));
      //fprintf(stderr, "Set ILNV to %e\n", w->initial_ln_volume);
      w->initial_net_prob = w->initial_ln_prob + w->initial_ln_volume - w->initial_ke;
    }

    if (w->exclude_min < 0 && w->exclude_max > 0) {
      check_log(e->error_file, "[Sampler:Warning] Bug in min/max exclusion for Searchlight!\n");
      REJECT(2);
      sampler_reject(e, w); //Should never happen, but...
      return;
    }

    //Add queue samples to spline
    long double x[2], y[2];
    for (int64_t j=0; j<2; j++) {
      x[j] = w->queue[j+1].x;
      if (flip_direction) x[j] = -x[j];
      y[j] = sampler_searchlight_delta_ln_prob_queue(w, w->queue+j+1);
    }
    aspline_add_points(w->aspline, x, y, 2);
  } else if (fabs(sampler_searchlight_delta_ln_prob(w)) < e->lnp_tol) { //Finished

    //Need to make sure derivatives happen...!
    if (w->queue_expected==1) {
      sampler_unskip_derivatives(e, w);
      return;
    } else {
      sampler_generic_derivatives(e, w);
      w->derivatives[0]*=w->direction;
    }
    
    if (w->direction==1) { //switch directions
      if ((w->derivatives[0]>=0 && !w->adventuring) ||
	  (w->derivatives[0]<=0 && w->adventuring))
	{
	  double prob = (w->adventuring) ? (e->adventuring_prob) : (1.0-e->adventuring_prob);
	  if (dr250()*prob > (1.0-prob)) { //Too much luminosity to be reversible
	    fprintf(stderr, "%f %f %f %f %"PRId64"\n", sampler_searchlight_delta_ln_prob(w), w->x[0], w->derivatives[0], w->derivatives[1], w->direction);
	    REJECT(3); sampler_reject(e, w);  return;
	  } 
	}
      w->final_ln_volume = -log(sampler_dL_ds(w));
      w->final_ln_prob = w->queue->ln_prob;
      w->final_ke = (w->ergodic_dims) ? -sampler_ergodic_ln_likelihood(w->queue->v) : 0;
      w->final_net_prob = w->final_ln_prob + w->final_ln_volume - w->final_ke;
      fprintf(stderr, "DProb: %e; FLP: %e; FLNV: %e; FKE: %e; ILP: %e; ILNV: %e; IKE: %e\n",w->inv_temp*(w->final_net_prob - w->initial_net_prob), w->final_ln_prob, w->final_ln_volume, w->final_ke, w->initial_ln_prob, w->initial_ln_volume, w->initial_ke);
      if (w->final_net_prob < w->initial_net_prob) {
	double delta_prob = w->inv_temp*(w->final_net_prob - w->initial_net_prob);
	int64_t accept=1;
	if (w->final_ln_prob >= SAMPLER_INVALID_LN_PROB) accept = 0;
	if ((w->initial_ln_prob < SAMPLER_INVALID_LN_PROB)
	    && (dr250() > exp(delta_prob)))
	  accept = 0;
	if (accept) { w->final_net_prob = w->initial_net_prob; w->final_ln_volume=w->initial_ln_volume; }
	else {   REJECT(8); sampler_reject(e, w);  return; }
      }
      //fprintf(stderr, "IP: %f; FP: %f; IV: %f; FV: %f\n", w->initial_ln_prob - w->initial_ke,
      w->final_x = w->x[0];
      //Not worth checking reversibility if moved less than user-specified derivative calculations
      if (fabsl(w->final_x) < e->exploration_dx) {
	sampler_searchlight_victory(e,w);
	return;
      }
      w->direction = -1;
      aspline_reset(w->aspline);
      w->step = 0;
      w->x[0] = 0;
      for (int64_t j=1; j<3; j++) w->queue[j].x = w->final_x -  w->queue[j].x;
      if (w->refraction_gradient) {
	w->min_x = 0;
	w->max_x = 3*sqrt(w->dims+w->ergodic_dims-w->static_dims);
      } else {
	w->min_x = w->exclude_max; //While there could be a nearby point, avoid it...
	w->max_x = 0;
      }
      return sampler_searchlight(e, w);
    }
    else {
      if (fabsl(w->final_x + w->direction*w->x[0]) > fabsl(e->dx_tol*w->final_x)) {
	//Not reversible
	if (fabsl(w->final_x+ w->direction*w->x[0]) > e->exploration_dx) { REJECT(4); }
	else { REJECT(5); }
	fprintf(stderr, "I: %Lg; F: %g; D: %Lg\n", w->final_x, w->x[0], fabsl(w->final_x + w->direction*w->x[0])/fabsl(w->final_x));
	sampler_reject(e, w);
	return;
      }
      sampler_searchlight_victory(e,w);
      return;
    }
  }

  if (w->step > e->searchlight_max_steps) { REJECT(7); sampler_reject(e, w);  return; }
  
  //Add steps to spline
  long double x = w->queue->x;
  if (w->direction == -1) x = w->final_x - x;
  /*if (w->direction == -1 && x>w->final_x * 3.0) {
    REJECT(4); fprintf(stderr, "Short-circuit: I: %Lg; F: %g; D: %Lg\n", w->final_x, w->x[0], fabsl(w->final_x + w->direction*w->x[0])/fabsl(w->final_x)); sampler_reject(e, w); return; //Very likely not reversible
    }*/

  long double y = sampler_searchlight_delta_ln_prob(w);
  aspline_add_points(w->aspline, &x, &y, 1);
  if (y < -1.0*w->tunneling_threshold)  { REJECT(10); sampler_reject(e, w);  return; }
  
  double new_x = aspline_search_nth_root(w->aspline, w->root_count, 1e-10);
  if ((w->count%1000 == 0)) sampler_searchlight_write_spline(e, w, w->count, w->direction, w->step, new_x);
  if (isnan(new_x) || isnan(y)) { assert(0); REJECT(6); sampler_reject(e, w);  return; }

  
  w->x[0] = new_x;
  //fprintf(stderr, "[Info]: new_x: %f; dir: %f; %Lf; %"PRId64"; %"PRId64"\n", new_x, (float)w->direction, y, w->root_count, w->count);
  if (w->direction== -1 && fabsl(w->final_x + w->direction*w->x[0]) < fabsl(e->dx_tol*w->final_x)) {
    if (fabsl(w->initial_dL_du)*fabsl(w->final_x + w->direction*w->x[0]) < e->lnp_tol) { //Declare victory without having to repeat gradient step
      sampler_searchlight_victory(e,w);
      return;
    }
  }
  sampler_searchlight_queue_derivatives(e, w);
  sampler_skip_derivatives(e,w);
  w->step++;
}


void sampler_searchlight_rejection_stats(struct Sampler *e, FILE *output) {
  int64_t max_named_rejections = 11;
  char *named_rejections[11] = {"Rejected at initialization", "Zero derivative at current position", "Bug in min/max exclusion for Searchlight", "Too much luminosity to be reversible [reduce by moving adventuring probability closer to 0.5]",
    "Reverse transition landed at different spot [reduce by increasing uncertainty scaling]",  "Reverse transition landed at different spot (distance traveled < DX for derivatives)", "NaN encountered", "Too many steps", "Final net prob too low [future searchlight versions may include better algorithms]",
    "NaN for derivatives",  "Exceeded tunneling threshold (dead end)",};
  for (int64_t i=0; i<SAMPLER_MAX_REJECTION_TYPES; i++) {
    if (!e->rejection_stats[i] && i>=max_named_rejections) continue;
    fprintf(output, "Rejection %"PRId64": %"PRId64, i, e->rejection_stats[i]);
    if (i<max_named_rejections) fprintf(output, " (%s)", named_rejections[i]);
    fprintf(output,"\n");
  }
  fprintf(output, "Total Acceptances: %"PRId64"\n", e->accepted);
  fprintf(output, "Total Rejected: %"PRId64"\n", e->rejected);
  fprintf(output, "Acceptance Ratio: %f\n", (double)e->accepted/(double)(e->accepted+ e->rejected));
}

void sampler_set_refraction_gradient(struct Sampler *e, double refraction_gradient) {
  for (int64_t i=0; i<e->num_walkers; i++)
    e->w[i].refraction_gradient = refraction_gradient;
}

/**********************************************
Functions for monitoring acceptance/convergence
***********************************************/

//For monitoring acceptance fraction
double sampler_acceptance_fraction(struct Sampler *e) {
  int64_t t = e->accepted + e->rejected;
  if (!t) return 0;
  return ((double)e->accepted / (double)t);
}

int64_t sampler_total_steps(struct Sampler *e) {
  return (e->total_steps);
}

int64_t sampler_total_likelihoods(struct Sampler *e) {
  return (e->total_likelihoods);
}

double autocorrelation(double *params, int64_t nwalkers, int64_t total_steps, int64_t ss, int64_t absolute) {
  int64_t max_step_size = total_steps/nwalkers;
  if (ss < 1) ss=1;
  if (ss > max_step_size) ss = max_step_size;
  int64_t i, max_i = total_steps - (ss*nwalkers);
  double a1=0, a2=0, s1=0, s2=0, s12=0;
  for (i=0; i<max_i; i++) {
    a1 += params[i];
    a2 += params[i+ss*nwalkers];
  }
  a1 /= (double)max_i;
  a2 /= (double)max_i;

  double aa1 = 0, aa2 = 0;
  if (absolute) {
    for (i=0; i<max_i; i++) {
      aa1 += fabs(params[i]-a1);
      aa2 += fabs(params[i+ss*nwalkers]-a2);
    }
    aa1 /= (double)max_i;
    aa2 /= (double)max_i;
  }
  
  int64_t duplicates = 0;
  if (absolute) {
    for (i=0; i<max_i; i++) {
      /*if ((params[i])==(params[i+ss*nwalkers])) {
	duplicates++;
	continue;
	}*/
      double d1 = fabs(params[i]-a1)-aa1;
      s1 += d1*d1;
      double d2 = fabs(params[i+ss*nwalkers] - a2)-aa2;
      s2 += d2*d2;
      s12 += d1*d2;
    }
  } else {
    for (i=0; i<max_i; i++) {
      /*if ((params[i])==(params[i+ss*nwalkers])) {
	duplicates++;
	continue;
	}*/
      double d1 = params[i]-a1;
      s1 += d1*d1;
      double d2 = params[i+ss*nwalkers]-a2;
      s2 += d2*d2;
      s12 += d1*d2;
    }
  }
  s1 /= (double)(max_i-duplicates);
  s2 /= (double)(max_i-duplicates);
  s12 /= (double)(max_i-duplicates);
  double fdup = (double)duplicates / (double) max_i;
  return fdup + (1.0-fdup)*fabs(((s1 > 0 && s2 > 0) ? s12/sqrt(s1*s2) : 0));
}

//Autocorrelation time estimator, from Sokal
double autocorrelation_time_estimate(double *params, int64_t nwalkers, int64_t total_steps, int64_t absolute) {
  double tau = 1;
  int64_t i, max_step_size = total_steps/nwalkers;
  double skip_size = 1.0;
  for (i=1; i<max_step_size; i+=skip_size) {
    tau += 2.0*skip_size*fabs(autocorrelation(params, nwalkers, total_steps, i, absolute));
    if (i>15*skip_size) skip_size *= 2.0;
    if (i>5.0*tau) break;
  }
  return tau;
}

//Helper function to perform autocorrelation and print results
void autocorrelation_print(double *params, int64_t nwalkers, int64_t total_steps, int64_t absolute, FILE *out) {
  int64_t i;
  int64_t max_step_size = total_steps/nwalkers;
  for (i=1; i<max_step_size; i++) {
    fprintf(out, "%"PRId64" %f\n", i, autocorrelation(params, nwalkers, total_steps, i, absolute));
  }
  fprintf(out, "\n");
}


/**************************
Basic Statistical Functions
***************************/


//Natural log of beta function
double sampler_lbeta(double a, double b) {
  return (lgamma(a)+lgamma(b)-lgamma(a+b));
}

//Natural log of regularized incomplete beta function
//Based on suggested algorithm from https://codeplea.com/incomplete-beta-function-c
double sampler_lincbeta(double x, double a, double b) {
  assert(x>0 && x <= 1);
  if (x==1) return 0;
  if (x>0.5) return log1p(-exp(sampler_lincbeta(1.0-x,a,b)));
  double prefactor = a*log(x) + b*log(1-x) - log(a) - sampler_lbeta(a,b);
  //Calculate continued fraction using Lentz's algorithm
  double fraction = 1.0;
  double top = 1;
  double bottom = 0;
  for (int64_t i=1; i<1000; i++) {
    double r;
    //See http://functions.wolfram.com/06.21.10.0001.01 for continued fraction form
    if ((i%2)==1) {
      double k = (i-1)/2;
      r = -(a+k)*(a+b+k)*x / ((a+2*k)*(a+2*k+1));
    } else {
      double k = i/2;
      r = k*(b-k)*x/((a+2*k-1)*(a+2*k));
    }
    top = 1.0+r/top;
    bottom = 1.0/(1.0+r*bottom);
    if (fabs(top) < 1e-30) top = 1e-30;
    if (fabs(bottom) < 1e-30) bottom = 1e-30;
    fraction *= top * bottom;
    if (fabs(1.0-top*bottom) < 1e-14) break;
  }
  return (prefactor - log(fraction));
}

//Generate a random value from a probability distribution
//P(x) \propto (x)^(a-1) (1-x)^(b-1), over the interval x=(0,max)
double sampler_rand_incbeta(double max, double a, double b) {
  double min = 0;
  double rmax = sampler_lincbeta(max, a, b);
  double lb = sampler_lbeta(a,b);
  double rand = rmax + log(dr250());
  double x=0.999*max;
  double diff = rand - sampler_lincbeta(x,a,b);
  while (fabs(max-min)>1e-14*max && fabs(diff)>1e-14) {
    if (diff > 0) min = x;
    else max = x;
    //d(log(Ix(a,b)))/dx
    double der = exp((a-1.0)*log(x)+(b-1.0)*log1p(-x) - lb - (rand - diff));
    x = x + (diff)/der;
    if (!(x <= max && x >= min) || fabs(diff/der)<1e-14*fabs(x) ||
    ((diff > 0 && x==max) || (diff <= 0 && x==min)))
      x = min+0.5*(max-min);
    diff = rand - sampler_lincbeta(x,a,b);
  }
  return x;
}



double sampler_optimal_refraction_gradient(int64_t dims) {
  double A_D = 0;
  if (dims < 1000) {
    double result = 1.0;
    if (dims%2) {
      for (double x=1; x<=(dims-1)/2; x++) {
	double num = (dims-1)/2;
	result *= (num+x)/(4*x);
      }
      A_D =result*((dims-1.0)/2.0);
    } else {
      for (double x=1; x<=dims/2-1; x++) {
	double num = dims/2-1;
	result *= (4*x)/(num+x);
      }
      A_D = result/(M_PI);
    }
  } else {
    A_D = sqrt(((double)dims/2.0-1.0)/(M_PI));
  }
  double mean_cos = -2.0*A_D/((double)dims-1.0);
  double opt_A = 1.0 + 4.0/M_PI*asin(mean_cos);
  fprintf(stderr, "[Info] Optimal refraction gradient set to %g\n", opt_A);
  return opt_A;
}



/* Implements Cheng & Feast, 1980 */
double sampler_gamma_dist_gbh(double alpha) {
  if (alpha <=0.25) return 0;
  double a = alpha-0.25;
  double b = alpha/a;
  double c = 2.0/a;
  double d = c+2.0;
  double t=1.0/sqrt(alpha);
  double h1 = (0.4417+0.0245*t/alpha)*t;
  double h2 = (0.222-0.043*t)*t;
  double w = 0;
  while (1) {
    double u1 = dr250();
    double u = dr250();
    double u2 = u1+h1*u-h2;
    if (u2<=0 || u2 >= 1) continue;
    w = u1/u2;
    w = b*w*w*w*w;
    if (c*u2-d+w+1.0/w<=0) break;
    if (c*log(u2)-log(w)+w-1.0>=0) continue;
    break;
  }
  return a*w;
}

double sampler_chi_dist(double dof) {
  return sqrt(2.0*sampler_gamma_dist_gbh(dof/2.0));
}

long double sampler_ergodic_ln_likelihood(long double r) {
  return (-r*r/2.0l);
}
