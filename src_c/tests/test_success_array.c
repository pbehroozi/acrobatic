#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include "../sampler.h"
#include "../mt_rand.h"

int64_t success(double x, double y) {
  if (dr250()<0.5*(1.0-erf(log(x)+y))) return 1;
  return 0;
}

int main(void) {
  struct Sampler *e = new_sampler(10,10,0);
  int64_t i, j;
  for (i=0; i<1000; i++) {
    double x = sampler_draw_stepsize(e, 0.8, 1);
    double s = 0;
    double v = 3+dr250();
    for (j=0; j<e->num_walkers; j++)
      s += success(x, v);
    sampler_update_success(e, x, e->num_walkers, s);
    printf("%g %f\n", x, s/(double)e->num_walkers);
  }
  for (i=0; i<e->num_success; i++) {
    printf("%g %g %g\n", e->step_max*pow(SAMPLER_SUCCESS_ARRAY_SPACING, -0.5-(double)i), e->success[i].tried, e->success[i].success/e->success[i].tried);
  }
  return 0;
}
