#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"

int64_t dims = 50;
double boundary = -1e100;

int valid(const double *params) {
  int64_t i;
  for (i=0; i<dims/10; i++) {
    if (fabs(params[i])>fabs(boundary)) return 0;
  }
  return 1;
}

double ln_prob_gaussian(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  double offset = 0;
  if (!valid(params)) return SAMPLER_INVALID_LN_PROB;
  for (i=0; i<dim; i++)
    ds += (params[i]-offset)*(params[i]-offset);
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t max_steps = 10000;
  double dt = 1.0, tmax_scaling = 1.0;
  double ln_prob_thresh = 5, velocity_scaling = 1.0;
  int64_t full_hamiltonian = 0;
  
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) dt = atof(argv[2]);
  if (argc>3) tmax_scaling = atof(argv[3]);
  if (argc>4) ln_prob_thresh = atof(argv[4]);
  if (argc>5) velocity_scaling = atof(argv[5]);
  if (argc>6) full_hamiltonian = atol(argv[6]);
  if (argc>7) boundary = atof(argv[7]);
  
  struct Sampler *e = new_sampler(10, dims, 0);
  double *params = NULL;
  if (argc>7) e->valid = &valid;
  e->alexander_variant = 1;
  
  //sampler_use_covariance_matrix(e, 0);
  check_calloc_s(params, sizeof(double), dims);
  if (full_hamiltonian==1) sampler_set_basis_dims(e, dims);
  if (full_hamiltonian>1) sampler_set_basis_dims(e, full_hamiltonian+1);
  if (full_hamiltonian==0) sampler_set_basis_dims(e, 2);
  //sampler_initial_exploration(e, params, NULL);
  sampler_estimate_covariance_matrix(e, params, NULL);
  sampler_set_output_file(e, stdout);
  sampler_set_log_file(e, stderr);
  sampler_set_numerical_derivative_step(e, 0.001);
  sampler_set_tmax_scaling(e, tmax_scaling);
  sampler_set_ln_prob_reject_threshold(e, ln_prob_thresh);
  sampler_set_velocity_scaling(e, velocity_scaling);
  sampler_set_dt(e, dt);
  int64_t count = 0;
  while (1) {
    struct QueueSample *q = sampler_get_sample(e);
    double ln_prob = ln_prob_gaussian(q->params, q->dims);
    sampler_assign_ln_prob(e,q,ln_prob);
    if (e->accepted + e->rejected >= max_steps) break;
    if (e->state == SAMPLER_STATE_FULL_EXPLORATION) count++;
  }
  fprintf(stderr, "Acceptance fraction: %f\n", sampler_acceptance_fraction(e));
  fprintf(stderr, "Eval/step: %f\n", (double)count / (double)(e->accepted+e->rejected));
  return 0;
}
