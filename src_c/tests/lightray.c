#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "integrate.h"

#define DLNN_DR ((double)-0.5)

double dlnn_dr(double r) {
  return DLNN_DR;
}

double propagate_lightray(double angle) {
  double p[2] = {1,0};
  double v[2] = {-cos(angle), sin(angle)};
  double r = sqrt(p[0]*p[0] + p[1]*p[1]);
  double theta = atan2(p[1], p[0]);
  double drdt = (v[0]*p[0] + v[1]*p[1])/r;
  double dthetadt = (v[1]*p[0] - v[0]*p[1]/(r*r));
  if (dthetadt==0) return 0;
  double drdtheta = drdt / dthetadt;
  //dr/dtheta = dr/dt / dtheta / dt
  
  double dt = 0.001;
  for (int64_t i=0; i<6280; i++) {
    //printf("%f %f\n", p[0], p[1]);
    r += dt*drdtheta/2.0;

    double dr2_dtheta2 = dlnn_dr(r)*(drdtheta*drdtheta + r*r) +
      2.0/(r)*((drdtheta*drdtheta))+r;
    drdtheta += dr2_dtheta2*dt;

    r += dt*drdtheta/2.0;
    theta += dt;
    if (r>1.0) {
      theta += (1.0-r)/drdtheta;
      break;
    }

    p[0] = r*cos(theta);
    p[1] = r*sin(theta);
  }
  return ((M_PI-theta)/2.0);
  //printf("\n");
}

struct integral_helper {
  double c1;
  double s;
};

double integrand(double x, void *b) {
  struct integral_helper *c = b;
  return c->s/(x*sqrt(c->c1*x*x*exp(2.0*DLNN_DR*x)-1.0));
}

void propagate_lightray_integrate(double angle) {
  double p[2] = {1,0};
  double v[2] = {cos(angle), sin(angle)};
  double r = sqrt(p[0]*p[0] + p[1]*p[1]);
  double theta = atan2(p[1], p[0]);
  double drdt = (v[0]*p[0] + v[1]*p[1])/r;
  double dthetadt = (v[1]*p[0] - v[0]*p[1]/(r*r));
  if (dthetadt==0) return;
  double drdtheta = drdt / dthetadt;

  struct integral_helper ih;
  ih.c1 = (drdtheta*drdtheta/(r*r)+1.0)/(r*r*exp(2.0*DLNN_DR*r));
  ih.s = 1;

  double target_r = r*1.1;
  printf("%f %f\n", ih.c1, ih.s);
  double final_theta = theta + adaptiveSimpsons(&integrand, (void *)(&ih),
						1, target_r, 0.001, 10);
  p[0] = target_r*cos(final_theta);
  p[1] = target_r*sin(final_theta);
  printf("%f %f\n", p[0], p[1]);
}
			  

int main(void) {
  int64_t i;
  for (i=0; i<10; i++) {
    double angle = M_PI/2.0*((double)i+0.5)/10.0;
    double result = propagate_lightray(angle);
    printf("%f %f %f %f\n", angle, result, cos(angle), pow(cos(result), 2));
  }
  //propagate_lightray_integrate(M_PI/2.0);
  return 0;
}
