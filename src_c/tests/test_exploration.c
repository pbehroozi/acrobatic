#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"

double ln_prob_gaussian(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t dims = 50;
  if (argc>1) dims = atol(argv[1]);
  struct Sampler *e = new_sampler(100, dims, 0);
  double *params = NULL;
  check_calloc_s(params, sizeof(double), dims);
  sampler_initial_exploration(e, params, NULL);
  while (e->state == SAMPLER_STATE_INITIAL_EXPLORATION) {
    struct QueueSample *q = sampler_get_sample(e);
    double ln_prob = ln_prob_gaussian(q->params, q->dims);
    double x = e->w[q->walker].x[0];
    if (q->id==0) printf("%"PRId64" %lf %lf\n", q->walker, x, ln_prob + (double)(dims-1)*log(x));
    sampler_assign_ln_prob(e,q,ln_prob);
  }
  return 0;
}
