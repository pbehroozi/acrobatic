#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <math.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"
#include <assert.h>

#define NUM_DRAWS 10000
double angles[NUM_DRAWS];

//Gives cosine of random angle on unit sphere.
double sampler_cos_random_angle2(int64_t basis_dims, double theta_max) {
  //At a given angle theta, volume of remaining sphere will be volume of (n-1) sphere with radius sin(theta).
  //I.e., proportional to the integral of sin(theta)^(n-2)
  if (basis_dims==1) return 1;
  assert(theta_max <= M_PI/2.0);
  double s = sin(theta_max);
  double random_s = s*pow(dr250(), 1.0/(basis_dims-1.0));
  return sqrt(1.0-random_s*random_s);
}

int main(int argc, char **argv) {
  int64_t i, i_max = 4;
  double max = 0.5;
  if (argc>1) i_max = atol(argv[1]);
  if (argc>2) max = atof(argv[2]);
  for (i=0; i<i_max; i++) {
    printf("Int_0^%f sin(x)^%"PRId64"dx = %lg\n", max, i, sampler_integrate_sin_n(i,max));
  }

  r250_init(87L);
  FILE *out = check_fopen("angles_dist.dat", "w");
  double max_a, min_a;
  for (i=0; i<NUM_DRAWS; i++) {
    double c = sampler_cos_random_angle2(i_max, max);
    angles[i] = c;
  }
  max_a = min_a = angles[0];
  for (i=0; i<NUM_DRAWS; i++) {
    if (angles[i] < min_a) min_a = angles[i];
    if (angles[i] > max_a) max_a = angles[i];
  }
  double dist[20] = {0};
  for (i=0; i<NUM_DRAWS; i++) {
    int64_t bin = (angles[i]-min_a)/(max_a-min_a)*20;
    if (bin==20) bin--;
    dist[bin]++;
  }
  for (i=0; i<20; i++) {
    fprintf(out, "%f %e\n", min_a+(max_a-min_a)*(i+0.5)/20.0, dist[i]/(max_a-min_a));
  }
  fclose(out);
  return 0;
}
