#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"

double ln_prob_gaussian(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  for (i=0; i<dim-1; i++)
    ds += params[i]*params[(i+1)%dim];
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t dims = 50;
  int64_t static_dims = 0;
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) static_dims = atol(argv[2]);
  struct Sampler *e = new_sampler(100, dims, 10000);
  double *params = NULL;
  check_calloc_s(params, sizeof(double), dims*2);
  double *tmp_params = params+dims;
  //sampler_use_covariance_matrix(e, 0);
  if (static_dims) {
    for (int64_t i=dims-1; i>=dims-static_dims; i--) {
      sampler_set_static_dim(e, i-1);
      fprintf(stderr, "Dim %"PRId64" is static.\n", i-1);
    }
  }
  fprintf(stderr, "Static dims: %"PRId64"\n", e->static_dims);
  fprintf(stderr, "Static dim 5: %d\n", SAMPLER_STATIC_DIM(e, 5));
  fprintf(stderr, "Static dim 4: %d\n", SAMPLER_STATIC_DIM(e, 4));
  sampler_estimate_covariance_matrix(e, params, NULL);
  while (e->state == SAMPLER_STATE_INITIAL_COV_MATRIX) {
    struct QueueSample *q = sampler_get_sample(e);
    memcpy(params, q->params, sizeof(double)*dims);
    //params[3] = params[5];
    double ln_prob = ln_prob_gaussian(params, dims-static_dims);
    sampler_assign_ln_prob(e,q,ln_prob);
  }
  int64_t i, j;
  

  printf("#Eigenvalues:\n#");
  for (i=0; i<e->dims; i++) {
    printf("%.2e ", e->eigenvalues[i]);
  }
  printf("\n");
  e->dr = 0;
  memset(params, 0, sizeof(double)*e->dims);
  for (i=0; i<10000; i++) {
    for (j=0; j<e->dims-static_dims; j++) params[j] = normal_random(0, 1);
    sampler_translate_basis(e, e->w, params, tmp_params, 0);
    for (j=0; j<e->dims; j++) {
      printf("%f ", tmp_params[j]);
    }    
    printf("\n");
  }
  return 0;
}
