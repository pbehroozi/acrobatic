#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../mt_rand.h"

#define DIMS 50
#define SAMPLES 10000

void random_unit_vector(double *v) {
  double s=0;
  int64_t j;
  for (j=0; j<DIMS; j++) {
    v[j] = normal_random(0, 1);
    s += v[j]*v[j];
  }
  s = sqrt(s);
  for (j=0; j<DIMS; j++) v[j]/=s;  
}

double dot(double *x1, double *x2) {
  double s=0;
  int64_t j;
  for (j=0; j<DIMS; j++)
    s += x1[j]*x2[j];
  return s;
}

void mul(double *v, double a) {
  int64_t j;
  for (j=0; j<DIMS; j++)
    v[j]*=a;
}

void add(double *v1, double *v2, double c) {
  int64_t j;
  for (j=0; j<DIMS; j++)
    v1[j]+=c*v2[j];
}

int main(int argc, char **argv) {
  double x[DIMS]={0}, v[DIMS]={0};
  int64_t i,j;
  double A=0.87;

  if (argc > 1) A = atof(argv[1]);
  x[0] = 1;

  double avg_d = 0;
  double rand_d = 0;
  int64_t theta_bins[100]={0};
  for (i=0; i<SAMPLES; i++) {
    random_unit_vector(v);
    double d = dot(x,v)/sqrt(dot(x,x));
    if (d>0) {
      mul(v, -1);
      d = -d;
    }
    rand_d += fabs(d);

    //d is cos(psi), where psi is angle between r and v
    //hence, asin(d) = phi in EOM
    double phi = asin(d);
    
    //cos((1-A)theta + phi) = cos(phi), so
    //(1-A)theta + phi = -phi, or
    //theta = (-2*phi)/(1-A)
    double theta = -2.0*phi/(1.0-A);

    //Find perpendicular basis vector:
    add(v,x,-d);
    double new_length = dot(v,v);
    mul(v,1.0/sqrt(new_length));

    double x_length = sqrt(dot(x,x));
    
    //rotate x by angle theta
    mul(x,cos(theta));
    add(x,v,x_length*sin(theta));
    avg_d += fabs(cos(theta));

    int64_t bin = 100.0*fabs(theta/M_PI);
    bin=bin%100;
    theta_bins[bin]++;
    /*for (j=0; j<DIMS; j++)
      printf("%f ", x[j]);
      printf("\n");*/
  }
  //fprintf(stderr, "Avg dot: %f (comp: %f for random)\n", avg_d/(double)SAMPLES, rand_d/(double)SAMPLES);
  for (i=0; i<100; i++) {
    printf("%f %"PRId64"\n", (i+0.5)*M_PI/100.0, theta_bins[i]);
  }
  return 0;
}
