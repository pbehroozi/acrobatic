#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../mt_rand.h"

double ln_prob_gaussian(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t dims = 50;
  int64_t max_steps = 100000;
  int64_t i, j, thinning=1;
  
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) max_steps = atol(argv[2]);
  if (argc>3) thinning = atol(argv[3]);
  
  r250_init(83761898273643L);
  double *params = NULL;
  double cur_ln_prob = 0;
  check_calloc_s(params, sizeof(double), dims*2);
  for (j=0; j<dims; j++)
    params[j] = normal_random(0,1);
  cur_ln_prob = ln_prob_gaussian(params, dims);
  
  int64_t accepted = 0;
  for (i=0; i<max_steps; i++) {
    double epsilon = 2.38/(double)sqrt(dims);
    for (j=0; j<dims; j++) params[dims+j] = params[j]+normal_random(0,1)*epsilon;
    double new_ln_prob = ln_prob_gaussian(params+dims, dims);
    if (dr250() < exp(new_ln_prob - cur_ln_prob)) {
      memcpy(params, params+dims, sizeof(double)*dims);
      cur_ln_prob = new_ln_prob;
      accepted++;
    }
    if ((i%thinning)==0) {
      for (j=0; j<dims; j++) printf("%.8e ", params[j]);
      printf("%.8e %.8e\n", cur_ln_prob, -2.0*cur_ln_prob);
    }
  }
  fprintf(stderr, "Acceptance fraction: %lf\n", (double)accepted / (double)max_steps);
  fprintf(stderr, "Eval/step: 1.0\n");
  return 0;
}
