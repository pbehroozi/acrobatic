#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "metropolis_gw_sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"

#define USE_COVARIANCE 0

double offset = 0;
double boundary = -1e100;
int64_t dims = 50;
int64_t invalid = 0;

int valid(const double *params) {
  int64_t i;
  for (i=0; i<dims/10; i++) {
    if (fabs(params[i])>fabs(boundary)) {
      invalid++;
      return 0;
    }
  }
  return 1;
}

double ln_prob_gaussian(double *params, double *variance, int64_t dim) {
  double ds = 0;
  int64_t i;
  if (!valid(params)) return 0.5;
  for (i=0; i<dim; i++)
    ds += (params[i]-offset)*(params[i]-offset)/variance[i];
  if (USE_COVARIANCE) {
    for (i=0; i<dim-1; i++)
      ds += (params[i]-offset)*(params[(i+1)]-offset)/sqrt(variance[i]*variance[(i+1)]);
  }
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t max_steps = 1000000;
  double stretch_step = 2;
  double gw_fraction = 1;
  double variance_sd = 0;
  int64_t thinning = 1, rounds = 0;
  int64_t init_type = 0;
  
  
  if (argc>1) dims = atol(argv[1]);
  max_steps = 100*4*dims*(1000*(double)dims/50.0);

  if (argc>2) stretch_step = atof(argv[2]);
  if (argc>3) gw_fraction = atof(argv[3]);
  if (argc>4) thinning = atof(argv[4]);
  if (argc>5) variance_sd = atof(argv[5]);
  if (argc>6) init_type = atol(argv[6]);
  if (argc>7) max_steps = atol(argv[7]);
  if (argc>8) boundary = atof(argv[8]);
  
  struct EnsembleSampler *e = new_sampler(4*dims, dims, stretch_step);
  e->gw_fraction = gw_fraction;
  e->mode = DJ_SAMPLER;
  r250_init(6962772806745128773L); //Use a large prime
  double *variance = NULL;
  check_calloc_s(variance, sizeof(double), dims);
  for (int64_t i=0; i<dims; i++) variance[i] = exp(normal_random(0,variance_sd));
  //variance[0] = 1;
  
  double *params = NULL;
  check_calloc_s(params, sizeof(double), dims);
  for (int64_t i=0; i<e->num_walkers; i++) {
    for (int64_t j=0; j<dims; j++) {
      double v = 0.0001;
      if (boundary) v=0.1;
      if (!init_type) v = variance[j];
      params[j]=normal_random(0,v);
    }
    init_walker_position(e->w+i, params);
  }

  int64_t count = 0;
  while (1) {
    struct Walker *w = get_next_walker(e);
    update_walker(e, w, -2.0*ln_prob_gaussian(w->params, variance, dims));
    if (ensemble_ready(e)) {
      update_ensemble(e);
      if ((rounds%thinning)==0)
	write_accepted_steps_to_file(e, stdout, 0);
      rounds++;
    }
    count++;
    if (count-invalid > max_steps) break;
  }
  fprintf(stderr, "Acceptance fraction: %f\n", acceptance_fraction(e));
  fprintf(stderr, "Eval/step: %g\n", (double)(count-invalid)/(double)count);
  return 0;
}
