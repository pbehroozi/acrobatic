#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"

double boundary = 1;
int64_t dims = 50;

int valid(const double *params) {
  for (int64_t i=0; i<dims; i++)
    if (fabs(params[i])>fabs(boundary)) return 0;
  return 1;
}

int main(int argc, char **argv) {
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) boundary = atof(argv[2]);
  
  struct Sampler *e = new_sampler(1, dims, 0);
  sampler_set_basis_dims(e, dims);
  double *params = NULL;
  check_calloc_s(params, sizeof(double), dims*2);


  //pick a random direction:
  for (int64_t i=0; i<dims; i++)
    params[i] = normal_random(0,1);
  memset(e->w->params, 0, sizeof(double)*dims);
  e->valid = &valid;
  double max = sampler_find_valid_point(e, e->w->params, params);
  printf("%g\n", max);
  memset(e->w->basis, 0, sizeof(double)*dims*dims);
  for (int64_t i=0; i<dims; i++)
    e->w->basis[i*dims+i] = 1;
    
  sampler_find_boundary_normal(e, e->w, params, params+dims);
  for (int64_t i=0; i<dims; i++) {
    printf("%g %g %g\n", params[i], params[i+dims], fabs(params[i+dims])-1.0);
  }
  memcpy(params+dims, params, sizeof(double)*dims);
  sampler_reflect_off_boundary(e, e->w, params, params+dims);
  for (int64_t i=0; i<dims; i++) {
    printf("%g %g %g\n", params[i], params[i+dims], fabs(params[i+dims])-1.0);
  }
  return 0;
}
