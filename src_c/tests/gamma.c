#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <gsl/gsl_sf_gamma.h>


double a_d(int64_t d) {
  double result = 1.0;
  if (d%2) {
    for (double x=1; x<=(d-1)/2; x++) {
      double num = (d-1)/2;
      result *= (num+x)/(4*x);
    }
    return result*((d-1.0)/2.0);
  }
  for (double x=1; x<=d/2-1; x++) {
    double num = d/2-1;
    result *= (4*x)/(num+x);
  }
  return result/(M_PI);
}

double approx_a_d(int64_t d) {
  return sqrt(((double)d/2.0-1.0)/(M_PI));
}

int main(void) {
  double d;
  for (d=2; d<1000; d++) {
    printf("%f %.7e %.7e %.7e\n", d, 2.0*gsl_sf_poch(d/2.0-0.5, 0.5)/(sqrt(M_PI)*(d-1.0)), 2.0*a_d(d)/(d-1.0), 2.0*approx_a_d(d)/(d-1.0));
  }
}
