#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../mt_rand.h"



//Dot product of x and y
double sampler_dot_product(int64_t dims, const double *x, const double *y) {
  double norm = 0;
  for (int64_t i=0; i<dims; i++) norm += x[i]*y[i];
  return norm;
}

//Divides x by its norm
void sampler_unit_vectorize(int64_t dims, double *x) {
  double norm = sampler_dot_product(dims, x, x);
  if (!norm) return;
  norm = 1.0/sqrt(norm);
  for (int64_t i=0; i<dims; i++) x[i]*=norm;
}

//Generates a random vector on the unit sphere; note that a unit
//multivariate Gaussian is spherically symmetric.
void sampler_random_unit(int64_t dims, char *static_dims, double *basis) {
  for (int64_t i=0; i<dims; i++) {
    basis[i] = normal_random(0, 1);
    if (static_dims && static_dims[i]) basis[i] = 0;
  }
  sampler_unit_vectorize(dims, basis);
}

//Generates a random vector on the unit sphere; note that a unit
//multivariate Gaussian is spherically symmetric.
void sampler_random_unit_perp(int64_t dims, char *static_dims, const double *basis, double *basis2) {
  sampler_random_unit(dims, static_dims, basis2);
  double n = sqrt(sampler_dot_product(dims, basis, basis));
  if (!n) return;
  double d = sampler_dot_product(dims, basis, basis2)/n;
  for (int64_t i=0; i<dims; i++)
    basis2[i] -= d*basis[i]/n;
  sampler_unit_vectorize(dims, basis2);
}

double ln_prob_gaussian(double *params, double *variance, int64_t dim) {
  double ds = 0;
  int64_t i;
  double offset = 0.0;
  for (i=0; i<dim; i++)
    ds += (params[i]-offset)*(params[i]-offset)/variance[i];
  return -ds/2.0;
}

double ln_prob_cos(double *params, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += params[i]*params[i];
  double r = sqrt(ds)*10.0;
  return cos(r);
}

int main(int argc, char **argv) {
  int64_t dims = 50;
  int64_t max_steps = 100000;
  int64_t i, j, thinning=1;
  double theta_step = 1.0;
  
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) theta_step = atof(argv[2]);
  //if (argc>2) max_steps = atol(argv[2]);
  //if (argc>3) thinning = atol(argv[3]);
  
  r250_init(83761898273643L);
  double *params = NULL;
  double cur_ln_prob = 0;
  check_calloc_s(params, sizeof(double), dims*2);
  double *variance = NULL;
  check_calloc_s(variance, sizeof(double), dims);
  for (j=0; j<dims; j++)
    params[j] = normal_random(0,1);
  variance[0] = 0.01;
  for (j=1; j<dims; j++)
    variance[j] = 1;
  cur_ln_prob = ln_prob_gaussian(params, variance, dims);

  double radial_step = 1.0;
  
  int64_t accepted = 0;
  for (i=0; i<max_steps; i++) {
    sampler_random_unit_perp(dims, NULL, params, params+dims);
    double theta = normal_random(0, theta_step*M_PI/2.0);
    double dr = normal_random(0, radial_step);
    double n = sqrt(sampler_dot_product(dims, params, params));
    double r_factor = (n+dr)/n;
    double ln_volume = ((double)dims-1.0)*log(fabs(r_factor));
    double c = cos(theta);
    double s = sin(theta);
    s*=n;
    for (j=0; j<dims; j++)
      params[j+dims] = r_factor*(params[j]*c + params[j+dims]*s);
    
    double new_ln_prob = ln_prob_gaussian(params+dims, variance, dims);
    if (dr250() < exp(new_ln_prob+ln_volume - cur_ln_prob)) {
      memcpy(params, params+dims, sizeof(double)*dims);
      cur_ln_prob = new_ln_prob;
      accepted++;
    }
    if ((i%thinning)==0) {
      for (j=0; j<dims; j++) printf("%.8e ", params[j]);
      printf("%.8e %.8e\n", cur_ln_prob, -2.0*cur_ln_prob);
    }
  }
  fprintf(stderr, "Acceptance fraction: %lf\n", (double)accepted / (double)max_steps);
  fprintf(stderr, "Eval/step: 1.0\n");
  return 0;
}
