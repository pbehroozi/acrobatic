#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"

#define USE_COVARIANCE 0
double offset = 0;
double boundary = -1e100;
int64_t dims = 50;


int valid(const double *params) {
  for (int64_t i=0; i<dims/10; i++)
    if (fabs(params[i])>fabs(boundary)) return 0;
  return 1;
}

double ln_prob_gaussian(double *params, double *variance, int64_t dim) {
  double ds = 0;
  int64_t i;
  if (!valid(params)) return SAMPLER_INVALID_LN_PROB;
  for (i=0; i<dim; i++)
    ds += (params[i]-offset)*(params[i]-offset)/variance[i];
  if (USE_COVARIANCE) {
    for (i=0; i<dim-1; i++)
      ds += (params[i]-offset)*(params[(i+1)%dim]-offset)/sqrt(variance[i]*variance[(i+1)%dim]);
  }
  return -ds/2.0;
}


int main(int argc, char **argv) {
  int64_t max_steps = 100000;
  double dr = SAMPLER_DR;
  int64_t thinning = 1, dim_thinning=1;
  double variance_sd = 0;
  int64_t adaptive_steps = 0;
  
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) dr = atof(argv[2]);
  if (argc>3) thinning = atol(argv[3]);
  if (argc>4) dim_thinning = atol(argv[4]);
  if (argc>5) offset = atof(argv[5]);
  if (argc>6) variance_sd = atof(argv[6]);
  if (argc>7) adaptive_steps = atof(argv[7]);
  
  struct Sampler *e = new_sampler(10, dims, 0);
  e->mode = STANDARD_METROPOLIS_SAMPLER;
  e->success_target = 0.234;
  e->dr = dr;
  e->step_thinning = thinning;
  e->dimension_thinning = dim_thinning;
  max_steps = (10*100*((double)dims/50.0)*180);

  if (argc>8) {
    boundary = atof(argv[8]);
    e->valid = &valid;
  }

  
  double *params = NULL;
  double *variance = NULL;
  check_calloc_s(params, sizeof(double), dims);
  check_calloc_s(variance, sizeof(double), dims);
  for (int64_t i=0; i<dims; i++) params[i] = offset;
  for (int64_t i=0; i<dims; i++)
    variance[i] = exp(normal_random(0,variance_sd));
  variance[0] = 1;

  if (USE_COVARIANCE) sampler_use_covariance_matrix(e, 1);
  sampler_estimate_covariance_matrix(e, params, NULL);
  //sampler_initial_exploration(e, params, NULL);
  sampler_set_output_file(e, stdout);
  sampler_set_log_file(e, stderr);
  int64_t count = 0;
  while (1) {
    struct QueueSample *q = sampler_get_sample(e);
    double ln_prob = ln_prob_gaussian(q->params, variance, q->dims);
    sampler_assign_ln_prob(e,q,ln_prob);
    if (e->accepted + e->rejected >= max_steps) break;
    if (e->state == SAMPLER_STATE_FULL_EXPLORATION) count++;
  }
  fprintf(stderr, "Acceptance fraction: %f\n", sampler_acceptance_fraction(e));
  fprintf(stderr, "Eval/step: %f\n", (double)count / (double)(e->accepted+e->rejected));
  return 0;
}
