#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"

#define USE_COVARIANCE 0

double offset = 0.0;

double ln_prob_gaussian(double *params, double *variance, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++)
    ds += (params[i]-offset)*(params[i]-offset)/variance[i];
  if (USE_COVARIANCE) {
    for (i=0; i<dim-1; i++)
      ds += (params[i]-offset)*(params[(i+1)]-offset)/sqrt(variance[i]*variance[(i+1)]);
  }
  return -ds/2.0;
}

int main(int argc, char **argv) {
  int64_t dims = 50;
  int64_t max_steps = 10000;
  double dtheta = M_PI/2.0;
  double dr = 1.0;
  double mf = 0.5;
  double variance_sd = 0;
  int64_t adaptive_steps = 0;
  
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) dtheta = atof(argv[2]);
  if (argc>3) dr = atof(argv[3]);
  if (argc>4) mf = atof(argv[4]);
  if (argc>5) offset = atof(argv[5]);
  if (argc>6) variance_sd = atof(argv[6]);
  if (argc>7) {
    adaptive_steps = atof(argv[7]);
    max_steps = adaptive_steps/0.3;
  }
  
  struct Sampler *e = new_sampler(1, dims, adaptive_steps/0.3);
  e->mode = SEARCHLIGHT_SAMPLER;
  e->success_target = 0.25;
  e->dtheta = dtheta;
  e->dr = dr;
  e->metropolis_frac = mf;
  if (dims > 500) {
    e->dimension_thinning = dims/500;
    fprintf(stderr, "Thinning dimensionality of outputs by %"PRId64"\n",
	    e->dimension_thinning);
  }
  
  double *params = NULL;
  double *variance = NULL;
  check_calloc_s(params, sizeof(double), dims);
  check_calloc_s(variance, sizeof(double), dims);
  for (int64_t i=0; i<dims; i++) {
    double v = 0;
    do { v = normal_random(0,1); } while (fabs(v)>1);
    variance[i] = exp(variance_sd*v);
  }
  variance[0] = 1;
  
  if (USE_COVARIANCE) sampler_use_covariance_matrix(e, 1);
  //sampler_estimate_covariance_matrix(e, params, NULL);
  sampler_initial_exploration(e, params, NULL);
  sampler_set_output_file(e, stdout);
  sampler_set_log_file(e, stderr);
  int64_t count = 0;
  while (1) {
    struct QueueSample *q = sampler_get_sample(e);
    double ln_prob = ln_prob_gaussian(q->params, variance, q->dims);
    if (e->state == SAMPLER_STATE_FULL_EXPLORATION) count++;
    sampler_assign_ln_prob(e,q,ln_prob);		  
    if (e->accepted + e->rejected >= max_steps) break;
  }
  fprintf(stderr, "Acceptance fraction: %f\n", sampler_acceptance_fraction(e));
  fprintf(stderr, "Eval/step: %f\n", (double)count / (double)(e->accepted+e->rejected));
  return 0;
}
