#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../sampler.h"
#include "../mt_rand.h"
#include "../check_syscalls.h"

int main(int argc, char **argv) {
  int64_t i, j, k, dims = 10, static_dims = 2, max_counts = 10000;
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) static_dims = atol(argv[2]);
  if (argc>3) max_counts = atol(argv[3]);
  
  struct Sampler *e = new_sampler(100, dims, max_counts*10); //Inits RNG
  double *params = NULL, *params2 = NULL, *params3 = NULL;
  check_calloc_s(params, sizeof(double), dims);
  check_calloc_s(params2, sizeof(double), dims);
  check_calloc_s(params3, sizeof(double), dims);
  double *cov_matrix = NULL;
  check_calloc_s(cov_matrix, sizeof(double), dims*dims);
  double *average = NULL;
  check_calloc_s(average, sizeof(double), dims);
  double *cov_matrix2 = NULL;
  check_calloc_s(cov_matrix2, sizeof(double), dims*dims);
  double *average2 = NULL;
  check_calloc_s(average2, sizeof(double), dims);
  
  FILE *output = check_fopen("samples.dat", "w");

  for (i=dims-static_dims; i<dims; i++)
    sampler_set_static_dim(e, i);
  
  for (i=0; i<max_counts; i++) {
    for (j=0; j<dims; j++) {
      double norm = 1.0/sqrt(j+1);
      double rest = sqrt(1.0-norm*norm);
      if (j<(dims-static_dims)) {
	params[j] = (double)(j+1)*norm*normal_random(0,1) + rest*params[j-1];
      } else {
	params[j] = 0;
      }
    }
    for (j=0; j<dims; j++) {
      for (k=0; k<dims; k++)
	cov_matrix[j*dims+k] += params[j]*params[k];
    }
    for (j=0; j<dims; j++) {
      params[j] += (dims-j);
      average[j] += params[j];
      fprintf(output, "%lf ", params[j]);
    }
    fprintf(output, "\n");
    sampler_init_position(e, params);
  }
  fclose(output);

  output = check_fopen("cov.dat", "w");
  for (j=0; j<dims; j++)
    fprintf(output, "%lf ", average[j]/((double)max_counts));
  fprintf(output, "\n");
  for (j=0; j<dims; j++) {
    for (k=0; k<dims; k++) {
      fprintf(output, "%"PRId64" %"PRId64" %lf\n", j, k, cov_matrix[j*dims+k]/(double)max_counts);
    }
  }
  fclose(output);
  
  
  sampler_update_averages(e);
  sampler_set_origin_as_average(e);
  sampler_stats_to_step(e);

  output = check_fopen("samples2.dat", "w");
  FILE *diff = check_fopen("diff.dat", "w");
  for (i=0; i<max_counts; i++) {
    for (j=0; j<dims; j++)
      params[j] = normal_random(0, 1);
    for (j=dims-static_dims; j<dims; j++) params[j] = 0;
    sampler_translate_basis(e, e->w, params, params2);
    sampler_detranslate_basis(e, e->w, params2, params3);

    for (j=0; j<dims; j++) {
      for (k=0; k<dims; k++)
	cov_matrix2[j*dims+k] += (params2[j]-e->avg[j])*(params2[k]-e->avg[k]);
      average2[j] += params2[j];
    }
    
    for (j=0; j<dims; j++) {
      fprintf(output, "%lf ", params2[j]);
      fprintf(diff, "%lf ", params3[j]-params[j]);
    }
    fprintf(output, "\n");
    fprintf(diff, "\n");
  }

  output = check_fopen("cov2.dat", "w");
  for (j=0; j<dims; j++)
    fprintf(output, "%lf ", e->origins[j]);
  fprintf(output, "\n");
  for (j=0; j<dims; j++) {
    for (k=0; k<dims; k++) {
      fprintf(output, "%"PRId64" %"PRId64" %lf\n", j, k, e->cov_matrix[j*e->dims+k]/(double)e->avg_steps);
    }
  }
  fclose(output);
    
  output = check_fopen("cov_diff.dat", "w");
  for (j=0; j<dims; j++)
    fprintf(output, "%lf ", (average[j] - average2[j])/(double)max_counts);
  fprintf(output, "\n");
  for (j=0; j<dims; j++)
    fprintf(output, "%lf ", (average[j])/(double)max_counts - e->origins[j]);
  fprintf(output, "\n");
  for (j=0; j<dims; j++) {
    for (k=0; k<dims; k++) {
      fprintf(output, "%"PRId64" %"PRId64" %lf\n", j, k, (cov_matrix[j*e->dims+k]-cov_matrix2[j*e->dims+k])/(double)e->avg_steps);
    }
  }
  fclose(output);
  return 0;
}
