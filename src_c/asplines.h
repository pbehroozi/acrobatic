#ifndef _ASPLINE_H_
#define _ASPLINE_H_
#include <math.h>
#include <inttypes.h>

#ifndef M_PIl
#define M_PIl       0xc.90fdaa22168c235p-2L
#endif /*ndef M_PIl */

#ifndef ASPLINE_TYPE
#define ASPLINE_TYPE long double
#endif /*ndef ASPLINE_TYPE */

#define ASPLINE_SEGMENT_IGNORE_ROOTS   1
#define ASPLINE_UNCERTAINTIES_CALCULATED 1
#define ASPLINE_DEFAULT_UNCERTAINTY_SCALING 2.0

struct aspline_segment {
  ASPLINE_TYPE x,y,s,m,a,b,c,d,uncertainty;
  int64_t flags;
};

struct aspline {
  struct aspline_segment *v;
  struct aspline_segment extrapolate[2];
  ASPLINE_TYPE uncertainty_scaling;
  int64_t flags;
  int64_t n;
};

struct aspline *aspline_new(void);
void aspline_free(struct aspline **a);
void aspline_reset(struct aspline *a);

//Solves ax^2 + bx + c = 0; stores solutions in x1 and x2, with *x1 <= *x2
int aspline_quadratic_solution(ASPLINE_TYPE a, ASPLINE_TYPE b, ASPLINE_TYPE c, ASPLINE_TYPE *x1, ASPLINE_TYPE *x2);

//Follows the wikipedia algorithm for ax^3 + bx^2 + cx + d = 0
int aspline_cubic_solution(ASPLINE_TYPE d, ASPLINE_TYPE c, ASPLINE_TYPE b, ASPLINE_TYPE a, ASPLINE_TYPE *roots);

//Find quadratic curve that passes through (x0, y0), (x1, y1), (x2, y2)
void aspline_quad_solve(struct aspline_segment *v, struct aspline_segment *res);

void _aspline_psolve(struct aspline *a);
void _aspline_refactor(struct aspline *a);
int aspline_search(const void *a, const void *b);
int64_t aspline_lookup_interval(struct aspline *a, ASPLINE_TYPE x);
struct aspline_segment *aspline_lookup_segment(struct aspline *a, int64_t interval);
void aspline_add_points(struct aspline *a, ASPLINE_TYPE *x, ASPLINE_TYPE *y, int64_t n);
ASPLINE_TYPE aspline_eval(struct aspline *a, ASPLINE_TYPE x);
ASPLINE_TYPE aspline_eval_derivative(struct aspline *a, ASPLINE_TYPE x);
ASPLINE_TYPE _aspline_eval_derivative(struct aspline_segment *v, ASPLINE_TYPE x);
int64_t aspline_count_roots(struct aspline *a, ASPLINE_TYPE xmin);
ASPLINE_TYPE aspline_search_nth_root(struct aspline *a, int64_t n, ASPLINE_TYPE xmin);
ASPLINE_TYPE aspline_estimate_uncertainty(struct aspline *a, ASPLINE_TYPE x);

#endif /*_ASPLINE_H_ defined*/
