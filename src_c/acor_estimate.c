#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "sampler.h"
#include "check_syscalls.h"

double *params = NULL;
int64_t total_steps = 0;

int64_t read_params(char *buffer, double *params, int max_n) {
  int num_entries = 0;
  char *cur_pos = buffer, *end_pos;
  double val = strtod(cur_pos, &end_pos);
  while (cur_pos != end_pos && num_entries < max_n) {
    params[num_entries] = val;
    num_entries++;
    cur_pos=end_pos;
    while (*cur_pos==' ' || *cur_pos=='\t' || *cur_pos=='\n') cur_pos++;
    val = strtod(cur_pos, &end_pos);
  }
  return num_entries;
}

int main(int argc, char **argv) {
  if (argc < 4) {
    fprintf(stderr, "Usage: %s <mcmc_output> <ndims> <nwalkers>\n", argv[0]);
    fprintf(stderr, "Assumes one space-separated column per dimension, followed by chi^2 or ln(P).\n");
    exit(EXIT_FAILURE);
  }

  char *buffer = NULL;
  int64_t dims = atol(argv[2]);
  int64_t nwalkers = atol(argv[3]);
  FILE *in = check_fopen(argv[1], "r");

  int64_t max_size = (1 + /*sign*/
		      (1+1+16) + /*number+decimal places*/
		      (2) + /* e+/- */
		      (3) + /*exponent*/
		      1)    /*space*/
    * (dims+3) + 2; //Final "\n" and "\0";
  check_realloc_s(buffer, sizeof(char), max_size);
  while (fgets(buffer, max_size, in)) {
    if (buffer[0]=='#') continue;
    check_realloc_every(params, sizeof(double)*(dims+1), total_steps, nwalkers);
    if (read_params(buffer, params+((dims+1)*total_steps), dims+1) < dims+1) continue;
    total_steps++;
  }

  int64_t i, j;
  double avg = 0, avg2 = 0;
  double chi2_corr = 0;
  int64_t static_dims = 0;
  free(buffer);
  double *dim_params = NULL;
  check_realloc_s(dim_params, sizeof(double), total_steps);
  int64_t converged = 1;

  for (i=0; i<dims+1; i++) {
    double avg_dim = 0;
    for (j=0; j<total_steps; j++) {
      dim_params[j] = params[j*(dims+1)+i];
      avg_dim += dim_params[j];
    }
    double acor = autocorrelation_time_estimate(dim_params, nwalkers, total_steps,0);
    double acor2 = autocorrelation_time_estimate(dim_params, nwalkers, total_steps,1);
    printf("Dimension %"PRId64": %lf %lf (avg: %e)\n", i, acor, acor2, avg_dim/(double)total_steps);
    if (acor*5 > (total_steps/nwalkers) || acor2*5 > (total_steps/nwalkers))
      converged = 0;
    if (i<dims) {
      if (acor > 1 && acor2>1) {
	avg += acor;
	avg2 += acor2;
      } else {
	static_dims++;
      }
    } else {
      chi2_corr = acor;
    }
  }
  avg /= (double)(dims-static_dims);
  avg2 /= (double)(dims-static_dims);
  printf("Chi2: %lf\n", chi2_corr);
  printf("Avg: %lf\n", avg);
  printf("Avg Abs: %lf\n", avg2);
  if (avg < chi2_corr) avg = chi2_corr;
  if (avg < avg2) avg = avg2;
  printf("Max of Chi2, Average: %lf\n", avg);
  if (!converged)
    printf("***Warning: Autocorrelation times are large relative to chain lengths, and are likely underestimates.\n");
  return 0;
}
