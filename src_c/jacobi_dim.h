#ifndef JACOBI_DIM_H
#define JACOBI_DIM_H
#include <inttypes.h>

void jacobi_decompose_dim(double *cov_matrix, double *eigenvalues, double *orth_matrix, int64_t dim);
void vector_madd_dim(double *a, double b, double *c, int64_t dim); //Performs a += b*c

#endif /*JACOBI_DIM_H*/
