#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "sampler.h"
#include "check_syscalls.h"
#include "mt_rand.h"

double ln_prob_gaussian(double *params, double *variance, int64_t dim) {
  double ds = 0;
  int64_t i;
  for (i=0; i<dim; i++) {
    ds += (params[i]*params[i])/variance[i];
  }
  return -ds/2.0;
}

int main(int argc, char **argv) {
  int64_t dims = 50; //Number of dimensions to sample
  int64_t num_walkers = 10; //Number of parallel samplers to use
  int64_t max_steps = 100000; //Max steps planned for sampling
  double variance_sd = 0; //Variance of widths of P(x) along different axes

  //Allow user to change the number of dimensions/variance
  if (argc>1) dims = atol(argv[1]);
  if (argc>2) variance_sd = atof(argv[2]);
  
  //Create a new sampler
  struct Sampler *e = new_sampler(num_walkers, dims, max_steps);
  //Set the type.  Options are:
  // STANDARD_METROPOLIS_SAMPLER (Adaptive Metropolis-Hastings)
  // ANGULAR_METROPOLIS_SAMPLER  (Adaptive Angular Metropolis-Hastings)
  // ALEXANDER_SAMPLER (Alexander Hamiltonian)
  // SEARCHLIGHT_SAMPLER (Adaptive Searchlight)
  sampler_set_type(e, ANGULAR_METROPOLIS_SAMPLER);

  //If some dimensions are *static*, i.e., they should not be varied
  //by the sampler, then you should declare that here:
  //sampler_set_static_dim(e, DIM); //Call once for each static dimension
  
  //Set the maximum likelihood position
  double *params = NULL;
  check_calloc_s(params, sizeof(double), dims);
  
  //Initialize the variances along each axis
  double *variance = NULL;
  check_calloc_s(variance, sizeof(double), dims);
  for (int64_t i=0; i<dims; i++) variance[i] = exp(normal_random(0,variance_sd));
  variance[0] = 1;

  //If a fast bounds check routine is available, let the sampler know here:
  //valid function should have prototype: int valid(const double *params);
  //sampler_set_valid_function(e, &valid);
  
  //Tell the sampler to estimate the covariance matrix from the
  //second derivatives (Hessian matrix) near the maximum likelihood point.
  //If estimates of standard deviations are already available, you can
  //pass those in the third argument.
  sampler_estimate_covariance_matrix(e, params, NULL);

  /*Alternative options: If using, comment out the estimate_covariance_matrix line above!*/
  /*Automatic searching for typical set along random directions.*/
  //sampler_initial_exploration(e, params, standard_deviation_estimates);

  /*User-supplied initial positions:*/
  /*Call at least once for each walker.  Calling additional times will help build
    up the sample covariance matrix.  user_params should be: double[num_user_points][dims] */
  //for (int64_t i=0; i<num_user_points; i++)
  //  sampler_init_position(e, user_params[i]);
  
  sampler_set_output_file(e, stdout); //Accepted samples output here
  sampler_set_log_file(e, stderr);  //Step size info output here

  while (1) {
    struct QueueSample *q = sampler_get_sample(e);
    double ln_prob = ln_prob_gaussian(q->params, variance, q->dims);
    sampler_assign_ln_prob(e,q,ln_prob); //or sampler_assign_chi2(e,q,chi2) if chi^2 is preferred
    if (sampler_total_steps(e) >= max_steps) break;
  }
  fprintf(stderr, "Acceptance fraction: %f\n", sampler_acceptance_fraction(e));
  fprintf(stderr, "Eval/step: %f\n", (double)sampler_total_likelihoods(e) / (double)sampler_total_steps(e));
  fprintf(stderr, "Total steps: %g\n", (double)(sampler_total_steps(e)));
  return 0;
}
