#ifndef _SAMPLER_H_
#define _SAMPLER_H_

#include <math.h>
#include <stdio.h>
#include "asplines.h"

#define ALEXANDER_VARIANT 1

#define STANDARD_METROPOLIS_SAMPLER 0
#define ANGULAR_METROPOLIS_SAMPLER 1
#define ALEXANDER_SAMPLER 2
#define SEARCHLIGHT_SAMPLER 3

#define DERIVATIVE_NUMERICAL_BASIC 0
#define DERIVATIVE_NUMERICAL_TRIANGLE 1
#define DERIVATIVE_NUMERICAL_SECANT 2
#define DERIVATIVE_USER 3

#define EVALUATION_TYPE 0
#define DERIVATIVE_TYPE 1

#define SAMPLER_TMAX_SCATTER 0.01
#define SAMPLER_F_ADAPT 0.3
#define SAMPLER_DELTA_PROB 5.0
#define SAMPLER_THETA_ROTATE (M_PI/5.0)
#define SAMPLER_COV_MATRIX_DIM_THRESH 5001
#define SAMPLER_SUCCESS_TARGET 0.25
#define SAMPLER_DEFAULT_BASIS_DIMS 3
#define SAMPLER_MIN_COVARIANCE_STEPS 300
#define SAMPLER_STEPS_PER_ADAPTATION 300

#define SAMPLER_DT_MAX 5.0
#define SAMPLER_DT 1.0
#define SAMPLER_DV 0.01
#define SAMPLER_EXPLORATION_DX 0.01
#define SAMPLER_DTHETA 1.0
#define SAMPLER_DTHETA_MAX (M_PI/2.0)
#define SAMPLER_DR 1.0
#define SAMPLER_DR_MAX 1.0
#define SAMPLER_DEFAULT_BOUNDARY_RETRIES 100

#define SAMPLER_DEFAULT_METROPOLIS_FRAC 0.5
#define SAMPLER_DEFAULT_LNP_TOL 0.001
#define SAMPLER_DEFAULT_DX_TOL 0.01

#define SAMPLER_METROPOLIS_SCALING 2.38
#define SAMPLER_METROPOLIS_SUCCESS_TARGET 0.234

#define SAMPLER_ADAPT_SUCCESS_RATIO 0
#define SAMPLER_ADAPT_SUCCESS_ARRAY 1
#define SAMPLER_ADAPT_NONE 2
#define SAMPLER_SUCCESS_ARRAY_SPACING 1.15

#define SAMPLER_SEARCHLIGHT_TUNNELING_THRESHOLD 1e4
#define SAMPLER_SEARCHLIGHT_MAX_STEPS 50
#define SAMPLER_MAX_REJECTION_TYPES 100

#define SAMPLER_STATE_INVALID 0
#define SAMPLER_STATE_INITIAL_EVALUATION 1
#define SAMPLER_STATE_INITIAL_STDDEV 2
#define SAMPLER_STATE_INITIAL_COV_MATRIX 3
#define SAMPLER_STATE_INITIAL_EXPLORATION 4
#define SAMPLER_STATE_FULL_EXPLORATION 5

#define SAMPLER_ORIGIN_AVERAGE 0
#define SAMPLER_ORIGIN_BESTFIT 1
#define SAMPLER_ORIGIN_MANUAL  2

#define QUEUESAMPLE_NEW      0
#define QUEUESAMPLE_PENDING  1
#define QUEUESAMPLE_COMPLETE 2
#define QUEUESAMPLE_SKIPPED  3

#define WALKER_HIT_LEFT_MAX_FLAG 1
#define WALKER_HIT_RIGHT_MAX_FLAG 2
#define WALKER_HIT_LEFT_MIN_FLAG 4
#define WALKER_HIT_RIGHT_MIN_FLAG 8

#define SAMPLER_MIN_QUEUE_SIZE 3

#define SAMPLER_INVALID_LN_PROB 1000000
#define SAMPLER_INVALID_CHI2    -1

#define SAMPLER_RUNNING_AVERAGE_LENGTH 1000

#define SAMPLER_STACKSIZE 5

#define SAMPLER_STATIC_DIM_FLAG 1
#define SAMPLER_METROPOLIS_DIM_FLAG 2

#define SAMPLER_STATIC_DIM(e,dim) ((e)->dim_flags[dim] & SAMPLER_STATIC_DIM_FLAG)
#define SAMPLER_METROPOLIS_DIM(e,dim) ((e)->dim_flags[dim] & SAMPLER_METROPOLIS_DIM_FLAG)

struct Walker {
  int64_t dims, basis_dims, static_dims, count;
  double inv_temp, *params;
  int64_t derivative_mode, samples_present;
  double dim_correction;
  double *derivatives;
  double initial_ln_prob, final_ln_prob;
  double initial_ln_volume, final_ln_volume;
  double initial_dL_du;
  double initial_ke, final_ke;
  double initial_net_prob, final_net_prob;
  long double ergodic_length, ergodic_unit_perp, ergodic_unit_parallel, ergodic_dims;
  double *origin;
  double *basis;
  double *x, *v;
  double initial_norm;
  double min_x, max_x, min_x_net_prob, max_x_net_prob;
  double min_x2, max_x2;
  int64_t adventuring, root_count;
  double tunneling_threshold;
  long double exclude_min, exclude_max, final_x;
  double refraction_gradient, r_0, phi;
  void (*translate_function)(struct Walker *, double, double *, long double *);
  int64_t step, max_steps, direction, mode, flip_u;
  double dt, dt_factor;
  char *dim_flags;
  double t_max;
  struct QueueSample *queue;
  int64_t num_queue, queue_expected;
  int64_t success_streak, longest_streak;
  double stopping_criterion;
  int64_t flags;
  struct aspline *aspline;
};

struct QueueSample {
  int64_t dims, walker, id, type;
  double *params;
  double *direction;
  double stepsize, x;
  long double v;
  double ln_prob;
  double ln_volume;
  double net_prob;
  int64_t state;
};

struct SamplerSuccess {
  double success, tried; 
};

//Todo: add updates every N samples distinct from # of walkers
struct Sampler {
  int64_t state;
  int64_t num_walkers, dims, basis_dims, walkers_done, cur_walker;
  int64_t accepted, rejected, ignore_steps, new_ignore_steps;
  int64_t prev_accepted, running_avg_length;
  int64_t total_steps, total_likelihoods;
  int64_t resampling_dims;
  double running_success_fraction;
  double inv_temp;  //The sampler inverse temperature
  int64_t step_thinning; //Number of steps between outputs
  int64_t dimension_thinning; //Number of dimensions to skip between outputs  
  int64_t no_eigen_rescaling;
  double ergodic_dims;
  
  int64_t num_origins, origin_type;
  double *origins;
  int64_t adaptive_samples, boundary_retries, mix_metropolis;
  double ln_prob_reject_threshold, theta_rotate, tmax_scatter, success_target;
  
  struct Walker *w;
  int64_t mode, avg_steps;
  int64_t static_dims;
  char *dim_flags;

  int64_t adaptation_mode;
  struct SamplerSuccess *success;
  int64_t num_success;
  double step_min, step_max;
  int64_t rejection_stats[SAMPLER_MAX_REJECTION_TYPES];
  
  int64_t alexander_variant;
  double radial_scaling;
  int64_t use_covariance_matrix, min_covariance_steps;
  int64_t freeze_covariance_matrix;
  int64_t last_adaptation, steps_per_adaptation;
  int64_t cov_element, cov_elements_done;
  double avg_ln_prob;
  double *avg, *new_avg, best_ln_prob;
  double *cov_matrix, *orth_matrix, *variance, *eigenvalues;
  double dt, dv, exploration_dx, dr, dtheta;
  double velocity_scaling, tmax_scaling;
  int64_t exploration_max_steps, searchlight_max_steps;
  int64_t save_adaptive_steps;
  double lnp_tol, dx_tol, metropolis_frac;
  double adventuring_prob, tunneling_prob, tunneling_threshold;

  
  int (*valid)(const double *);
  void (*enforce_parameterization)(double *);
  void (*step_completed)(int64_t, const double *, double);
  double (*ln_prob_callback)(double *);

  struct QueueSample *queue;
  double *queue_params;
  int64_t queue_allocated, cur_queue;
  double *stack;
  int64_t stack_counter;
  
  FILE *output_file, *log_file, *error_file;
};


struct Sampler *new_sampler(int64_t num_walkers, int64_t dims, int64_t total_samples);
void free_sampler(struct Sampler *e);
void sampler_set_type(struct Sampler *e, int64_t mode);
void sampler_setup_walker(struct Sampler *e, struct Walker *w);
void sampler_set_basis_dims(struct Sampler *e, int64_t basis_dims);
void sampler_set_walker_basis_dims(struct Sampler *e, struct Walker *w, int64_t basis_dims);
void sampler_set_t_max(struct Walker *w);
void sampler_use_covariance_matrix(struct Sampler *e, int64_t use_cov_matrix);
void sampler_reset_min_covariance_steps(struct Sampler *e);
void sampler_set_identity_covariance(struct Sampler *e);
void sampler_set_temp(struct Sampler *e, double temp);
void sampler_set_adaptive_samples(struct Sampler *e, int64_t adaptive_samples);
void sampler_set_ln_prob_reject_threshold(struct Sampler *e, double threshold);
void sampler_set_tmax_scatter(struct Sampler *e, double scatter);
void sampler_set_theta_rotate(struct Sampler *e, double theta);
void sampler_set_target_success_ratio(struct Sampler *e, double ratio);
void sampler_set_dt(struct Sampler *e, double dt);
void sampler_set_numerical_derivative_step(struct Sampler *e, double dv);
void sampler_set_exploration_derivative_step(struct Sampler *e, double dx);
void sampler_set_step_scaling(struct Sampler *e, double step_scale);
void sampler_set_radial_scaling(struct Sampler *e, double radial_scale);
void sampler_set_theta_scaling(struct Sampler *e, double theta_scale);
void sampler_set_output_file(struct Sampler *e, FILE *output);
void sampler_set_log_file(struct Sampler *e, FILE *output);
void sampler_set_valid_function(struct Sampler *e, int (*valid)(const double *));
void sampler_set_parameterization_function(struct Sampler *e, void (*param)(double *));
void sampler_set_step_completed_function(struct Sampler *e, void (*step_completed)(int64_t, const double *, double));
void sampler_check_state(struct Sampler *e, int64_t state, char *name);
void sampler_change_origin_type(struct Sampler *e, int64_t type);
void sampler_set_velocity_scaling(struct Sampler *e, double velocity_scaling);
void sampler_set_tmax_scaling(struct Sampler *e, double tmax_scaling);
void sampler_set_static_dim(struct Sampler *e, int64_t dim);
void sampler_clear_static_dims(struct Sampler *e);
void sampler_set_metropolis_dim(struct Sampler *e, int64_t dim);
void sampler_set_origin_as_average(struct Sampler *e);
void sampler_check_bestfit_origin(struct Sampler *e, double *params, double ln_prob);
void sampler_add_to_origins(struct Sampler *e, double *params);
void sampler_clear_origins(struct Sampler *e);
void sampler_init_walker_position(struct Walker *w, double *params);
void sampler_init_position(struct Sampler *e, double *params);
void sampler_init_position_lnprob(struct Sampler *e, double *params, double ln_prob);
void sampler_init_position_chi2(struct Sampler *e, double *params, double chi2);
void sampler_initial_exploration(struct Sampler *e, double *params, double *stddev);
void sampler_exploration_queue_derivatives(struct Sampler *e, struct Walker *w);
void sampler_exploration_derivatives(struct Sampler *e, struct Walker *w);
void sampler_finalize_exploration(struct Sampler *e, struct Walker *w, double ln_prob);
void sampler_explore(struct Sampler *e, struct Walker *w);
void sampler_clear_averages(struct Sampler *e);
void sampler_update_averages(struct Sampler *e);
void sampler_reset_averages(struct Sampler *e);
void sampler_stats_to_step(struct Sampler *e);
void sampler_prep_covariance_matrix(struct Sampler *e, int64_t avg_steps);
void sampler_remap_orthogonal_bases(struct Sampler *e);
double sampler_find_valid_limit(struct Sampler *e, const double *cur, const double *basis, double max);
double sampler_find_valid_point(struct Sampler *e, const double *cur, double *new);
double sampler_find_valid_point_bidirectional(struct Sampler *e, const double *cur, double *new);
void sampler_find_boundary_normal(struct Sampler *e, struct Walker *w, const double *params, double *normal);
void sampler_reflect_off_boundary(struct Sampler *e, struct Walker *w, const double *params, double *vec);
void sampler_translate_origin(struct Walker *w, double *params);
void sampler_translate_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params, int64_t translate_origin);
void sampler_detranslate_basis(struct Sampler *e, struct Walker *w, const double *in_params, double *out_params);
struct QueueSample *sampler_reset_queue_sample(struct Sampler *e, struct Walker *w, int64_t id);
struct QueueSample *sampler_get_sample(struct Sampler *e);
void _sampler_assign_ln_prob(struct Sampler *e, struct QueueSample *q, double ln_prob);
void sampler_assign_ln_prob(struct Sampler *e, struct QueueSample *q, double ln_prob);
void sampler_assign_chi2(struct Sampler *e, struct QueueSample *q, double chi2);
double sampler_calc_ln_volume(struct Walker *w, double *x, double basis_dims);
void sampler_calc_net_prob(struct Sampler *e, struct Walker *w);
void sampler_queue_derivatives(struct Sampler *e, struct Walker *w);
void sampler_derivatives(struct Sampler *e, struct Walker *w);
void sampler_leapfrog(struct Sampler *e, struct Walker *w);
double sampler_dot_product(int64_t dims, const double *x, const double *y);
void sampler_unit_vectorize(int64_t dims, double *x);
void sampler_random_unit(int64_t dims, char *static_dims, double *basis);
void sampler_random_unit_walker(struct Walker *w, double *basis);
void sampler_random_unit_perp(int64_t dims, char *static_dims, const double *basis, double *basis2);
double sampler_lbeta(double a, double b);
double sampler_lincbeta(double x, double a, double b);
double sampler_rand_incbeta(double max, double a, double b);
double sampler_cos_random_angle(double dims, double max_theta);
void sampler_random_rotate(int64_t dims, int64_t static_dim_count, char *static_dims, const double *in, double *out, double theta);
void sampler_init_alexander_step(struct Sampler *e, struct Walker *w);

void sampler_init_success_array(struct Sampler *e, double stepsize);
double sampler_draw_stepsize(struct Sampler *e, double target, double max_step);
void sampler_update_success(struct Sampler *e, double stepsize, double tried, double success);

void sampler_update_state(struct Sampler *e);
void sampler_decide_acceptance(struct Sampler *e, struct Walker *w);
void sampler_reject(struct Sampler *e, struct Walker *w);
void sampler_angular_step(struct Sampler *e, struct Walker *w, double dtheta);
void sampler_metropolis_step(struct Sampler *e, struct Walker *w);

void sampler_estimate_stddev_and_covariance(struct Sampler *e, double *avg, double *stddev_guess);
void sampler_evaluate_stddev_results(struct Sampler *e, struct Walker *w);
void sampler_output_covariance_samples(struct Sampler *e, int64_t num_samples, char *filename);
void sampler_set_covariance_matrix(struct Sampler *e, double *avg, double *cov_matrix);
void sampler_estimate_covariance_matrix(struct Sampler *e, double *avg, double *stddev);
void sampler_increment_cov_element(struct Sampler *e, int64_t use_covariance_matrix);
void sampler_queue_covariance_derivatives(struct Sampler *e, struct Walker *w, int64_t use_covariance_matrix);
void sampler_estimate_bounded_covariance(struct Sampler *e);

void sampler_searchlight(struct Sampler *e, struct Walker *w);
void sampler_init_searchlight_step(struct Sampler *e, struct Walker *w);
void sampler_set_refraction_gradient(struct Sampler *e, double refraction_gradient);

int sampler_quadratic_solution(long double a, long double b, long double c, long double *x1, long double *x2);
void sampler_searchlight_queue_derivatives(struct Sampler *e, struct Walker *w);

int64_t sampler_total_steps(struct Sampler *e);
int64_t sampler_total_likelihoods(struct Sampler *e);
double sampler_acceptance_fraction(struct Sampler *e);
double autocorrelation(double *params, int64_t nwalkers, int64_t total_steps, int64_t ss, int64_t absolute);
void print_autocorrelation(double *params, int64_t nwalkers, int64_t total_steps, int64_t absolute, FILE *out);
double autocorrelation_time_estimate(double *params, int64_t nwalkers, int64_t total_steps, int64_t absolute);
double sampler_optimal_refraction_gradient(int64_t dims);
double sampler_chi_dist(double dof);
long double sampler_ergodic_ln_likelihood(long double r);
void sampler_searchlight_set_ergodic_dims(struct Sampler *e, double dims);
void sampler_searchlight_rejection_stats(struct Sampler *e, FILE *output);

#endif /* _SAMPLER_H_ */
